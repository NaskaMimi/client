import {Component} from '@angular/core';
import {AuthService} from "../authorization/authservice/auth.service";

@Component({
    selector: 'planview-header',
    templateUrl: './header.component.html',
    styleUrls: ['header.component.css']
})
export class HeaderComponent {
    constructor(public authService: AuthService) {
    }

    logout() {
        this.authService.logout()
    }

    sign() {
        this.authService.moveToSign()
    }
}