import { NgModule }      from '@angular/core';
import { HeaderComponent }   from './header.component';
import {HttpClientModule} from "@angular/common/http";
import {AuthorizationServiceModule} from "../authorization/authservice/auth.service.module";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule, HttpClientModule, AuthorizationServiceModule],
    declarations: [HeaderComponent],
    exports: [
        HeaderComponent
    ]
})
export class HeaderModule {}