import {Component} from '@angular/core';

@Component({
    selector: 'mc-arch',
    template: `
        <div class="main-block">
            <router-outlet></router-outlet>
        </div>
    `,
    styleUrls: ['./app.component.css']


})
export class AppComponent {}
