import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {RouterModule, Routes} from "@angular/router";
import {APP_BASE_HREF, CommonModule} from '@angular/common';
import {AuthInterceptor} from "./authorization/api.interceptor";
import {AuthorizationComponent} from "./authorization/authorization.component";
import {AuthorizationModule} from "./authorization/authorization.module";
import {AuthGuard} from "./authorization/auth.guard";
import {PlanviewComponent} from "./planview/planview.component";
import {PlanviewModule} from "./planview/planview.module";
import {BuildingComponent} from "./planview/rightpanel/building/building.component";
import {MaininfoComponent} from "./planview/rightpanel/maininfo/maininfo.component";
import {ObjectinfoComponent} from "./planview/rightpanel/objectinfo/objectinfo.component";
import {ObjectsbarComponent} from "./planview/rightpanel/objectsbar/objectsbar.component";
import {RoominfoComponent} from "./planview/rightpanel/roominfo/roominfo.component";
import {WallinfoComponent} from "./planview/rightpanel/wallinfo/wallinfo.component";
import {BalconiesComponent} from "./planview/rightpanel/balconies/balconies.component";
import {ColumnsComponent} from "./planview/rightpanel/columns/columns.component";
import {DoorsComponent} from "./planview/rightpanel/doors/doors.component";
import {FoundationComponent} from "./planview/rightpanel/foundation/foundation.component";
import {PorchComponent} from "./planview/rightpanel/porch/porch.component";
import {RailingsComponent} from "./planview/rightpanel/railings/railings.component";
import {RoofComponent} from "./planview/rightpanel/roof/roof.component";
import {StairsComponent} from "./planview/rightpanel/stairs/stairs.component";
import {WindowsComponent} from "./planview/rightpanel/windows/windows.component";
import {BalconyinfoComponent} from "./planview/rightpanel/balconyinfo/balconyinfo.component";
import {PorchinfoComponent} from "./planview/rightpanel/porchinfo/porchinfo.component";
import {RoofinfoComponent} from "./planview/rightpanel/roofinfo/roofinfo.component";
import {HelloComponent} from "./authorization/hello/hello.component";
import {SignComponent} from "./authorization/sign/sign.component";
import {TexturesComponent} from "./planview/rightpanel/textures/textures.component";
import {FloorTexturesComponent} from "./planview/rightpanel/floortextures/floor.textures.component";
import {ChangePasswordComponent} from "./authorization/change-password/change-password.component";
import {RegistrationComponent} from "./authorization/register/registration.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CUSTOM_ERROR_MESSAGES, NgBootstrapFormValidationModule} from "ng-bootstrap-form-validation";
import {CUSTOM_ERRORS} from "./custom.errors";

//Todo change to lazy routing
const appRoutes: Routes = [
    {path: '', redirectTo: '/auth/hello', pathMatch: 'full'},
    {
        path: 'planview', component: PlanviewComponent, canActivate: [AuthGuard],
        children: [
            {path: 'balconyinfo', component: BalconyinfoComponent},
            {path: 'balconies', component: BalconiesComponent},
            {path: 'building', component: BuildingComponent},
            {path: 'columns', component: ColumnsComponent},
            {path: 'doors', component: DoorsComponent},
            {path: 'foundation', component: FoundationComponent},
            {path: 'maininfo', component: MaininfoComponent},
            {path: 'objectinfo', component: ObjectinfoComponent},
            {path: 'objectsbar', component: ObjectsbarComponent},
            {path: 'porch', component: PorchComponent},
            {path: 'porchinfo', component: PorchinfoComponent},
            {path: 'roofinfo', component: RoofinfoComponent},
            {path: 'railings', component: RailingsComponent},
            {path: 'roof', component: RoofComponent},
            {path: 'roominfo', component: RoominfoComponent},
            {path: 'stairs', component: StairsComponent},
            {path: 'wallinfo', component: WallinfoComponent},
            {path: 'textures', component: TexturesComponent},
            {path: 'floor-textures', component: FloorTexturesComponent},
            {path: 'windows', component: WindowsComponent}
        ]
    },
    {
        path: 'auth', component: AuthorizationComponent,
        children: [
            {path: 'hello', component: HelloComponent},
            {path: 'sign', component: SignComponent},
            {path: 'recovery-email', component: ChangePasswordComponent},
            {path: 'registration', component: RegistrationComponent}
        ]
    }
];

@NgModule({
    imports: [CommonModule, BrowserModule, PlanviewModule, RouterModule.forRoot(
        appRoutes,
        {enableTracing: true, useHash: true} // <-- debugging purposes only
    ), NgBootstrapFormValidationModule.forRoot(),
        AuthorizationModule, FormsModule, ReactiveFormsModule],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    providers: [
        AuthGuard,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        {provide: APP_BASE_HREF, useValue: '/'},
        {
            provide: CUSTOM_ERROR_MESSAGES,
            useValue: CUSTOM_ERRORS,
            multi: true
        }
    ]
})
export class AppModule {}