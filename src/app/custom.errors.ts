import {ErrorMessage} from "ng-bootstrap-form-validation";

export const CUSTOM_ERRORS: ErrorMessage[] = [
    {
        error: "required",
        format: requiredFormat
    }, {
        error: "email",
        format: emailFormat
    }, {
        error: "minlength",
        format: minLengthFormat
    }
];

export function requiredFormat(label: string, error: any): string {
    return `${label} должен быть заполнен`;
}

export function emailFormat(label: string, error: any): string {
    return `${label} не является электронной почтой`;
}

export function minLengthFormat(label: string, error: any): string {
    let end = endNumberFormat(error.actualLength);
    return `${label} должно быть минимум ${error.actualLength} ${end}`;
}

function endNumberFormat(count: number): string {
    if (count === 1) {
        return "символ";
    }
    return count < 6 ? 'символа' : 'символов'
}