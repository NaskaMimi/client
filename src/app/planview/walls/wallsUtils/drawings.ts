import {WallsPoints} from "./wallsPoints";
import {Room} from "../elements/room";
import * as THREE from 'three';
import {Wall} from "../elements/wall";
import {PlanViewContext} from "../../context/planViewContext";
import {PlanViewSettings} from "../../configuration/planViewSettings";
import {PlanViewEnvironment} from "../../context/planViewEnvironment";
import {Materials} from "../materials";

export class Drawings {

    static drawWall(name, walls, intersect: THREE.Vector2, cubeMaterial, axis, orientation: string) {
        var wallGeometry = new THREE.BoxGeometry(PlanViewSettings.WALL_INITIAL_SIZE, PlanViewSettings.WALL_HEIGHT, PlanViewSettings.WALL_INITIAL_SIZE);
        if (axis == "x") {
            //wallGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.WALL_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);
            wallGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.WALL_HEIGHT / 2, 0);
        } else if (axis == "z") {
            //wallGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.WALL_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);
            wallGeometry.translate(0, PlanViewSettings.WALL_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);
        } else {
            wallGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.WALL_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);
        }

        var wall = new THREE.Mesh(wallGeometry, cubeMaterial);
        wall.name = name;
        wall.castShadow = true;
        wall.receiveShadow = true;
        wall.position.copy(intersect);
        wall.position.divideScalar(50).floor().multiplyScalar(50).addScalar(0);
        wall.position.y = PlanViewContext.getInstance().yPosition;

        return wall;
    }

    static drawWallFromJSON(name, walls, vertex1: THREE.Vector3, vertex2: THREE.Vector3, cubeMaterial, axis, orientation: string) {
        var wallGeometry = new THREE.BoxGeometry(PlanViewSettings.WALL_INITIAL_SIZE, PlanViewSettings.WALL_HEIGHT, PlanViewSettings.WALL_INITIAL_SIZE);
        if (axis == "x") {
            //wallGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.WALL_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);
            wallGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.WALL_HEIGHT / 2, 0);
        } else if (axis == "z") {
            //wallGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.WALL_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);
            wallGeometry.translate(0, PlanViewSettings.WALL_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);
        } else {
            wallGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.WALL_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);
        }

        var wall = new THREE.Mesh(wallGeometry, cubeMaterial);
        wall.name = name;
        wall.castShadow = true;
        wall.receiveShadow = true;
        wall.position.copy(vertex1);
        wall.position.divideScalar(50).floor().multiplyScalar(50).addScalar(0);
        wall.position.y = vertex1.y;
        // wall.position.y = PlanViewContext.getInstance().yPosition;

      if (axis == "x") {
        wall.scale.x = vertex2.x - vertex1.x;
        wall.scale.z = PlanViewSettings.WALL_SIZE;
      } else {
        wall.scale.x = PlanViewSettings.WALL_SIZE;
        wall.scale.z = vertex2.z - vertex1.z;
      }

        return wall;
    }

    static drawFloor(wallArray, floorTexture: string) {

        var orderedVertices = WallsPoints.collectUnicPoints(wallArray);

        const floorShape = new THREE.Shape();
        var firstPoint = orderedVertices[0];

        floorShape.moveTo(firstPoint.x, firstPoint.y);
        for (var d = 0; d < orderedVertices.length; d++) {
            var vertex = orderedVertices[d];
            floorShape.lineTo(vertex.x, vertex.y);
        }

        const floorGeometry = new THREE.ShapeBufferGeometry(floorShape);
        const floorPlane = new THREE.Mesh(floorGeometry, Materials.createDoubleSideMeshMaterial(new THREE.TextureLoader(), 0xF2EEE8, floorTexture));

        this.normalizeVertixesForTextures(floorPlane, orderedVertices);

        floorPlane.rotateX(Math.PI / 2);
        floorPlane.position.y = PlanViewContext.getInstance().yPosition;
        PlanViewEnvironment.getInstance().scene.add(floorPlane);
        PlanViewContext.getInstance().addFloor(floorPlane);

        return floorPlane;
    }

    private static normalizeVertixesForTextures(floorPlane: THREE.Mesh, orderedVertices: any[]) {
        var box = new THREE.Box3().setFromObject(floorPlane);
        var size = new THREE.Vector3();
        box.getSize(size);
        var vec3 = new THREE.Vector3();
        var attPos = floorPlane.geometry.attributes.position;
        var attUv = floorPlane.geometry.attributes.uv;
        for (let i = 0; i < attPos.count; i++) {
            vec3.fromBufferAttribute(attPos, i);
            attUv.setXY(i,
                (vec3.x - box.min.x) / size.x,
                (vec3.y - box.min.y) / size.y
            );
        }

        var points = [];
        orderedVertices.forEach(r => {
            points.push(new THREE.Vector3(r.x, r.y, r.z));
        });

        var bufferPoints = [];
        points.slice().forEach(p => {
            bufferPoints.push(p.x, p.y, p.z);
        });
        var F32A = new Float32Array(bufferPoints);
        attPos.set(F32A, 0);
    }

    static redrawSingleWall(voxel) {

        let rollOverMesh = PlanViewEnvironment.getInstance().rollOverMesh;
        var voxelClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(voxel);

        if (WallsPoints.isChangeHorizontal(voxel.position, rollOverMesh.position)) {
            voxel.scale.x = rollOverMesh.position.x - voxel.position.x;
            voxel.scale.z = PlanViewSettings.WALL_SIZE;

            voxelClass.vertex1 = new THREE.Vector2(voxel.position.x, voxel.position.z);
            voxelClass.vertex2 = new THREE.Vector2(voxel.position.x + voxel.scale.x, voxel.position.z);
            voxelClass.wallAxis = "x";

        } else {
            voxel.scale.z = rollOverMesh.position.z - voxel.position.z;
            voxel.scale.x = PlanViewSettings.WALL_SIZE;

            voxelClass.vertex1 = new THREE.Vector2(voxel.position.x, voxel.position.z);
            voxelClass.vertex2 = new THREE.Vector2(voxel.position.x, voxel.position.z + voxel.scale.z);
            voxelClass.wallAxis = "z";
        }
    }

    static redrawRoom(wall1, wall2, wall3, wall4) {
        let rollOverMesh = PlanViewEnvironment.getInstance().rollOverMesh;

        wall3.position.z = rollOverMesh.position.z;
        wall4.position.x = rollOverMesh.position.x;
        wall4.position.z = rollOverMesh.position.z;

        wall1.scale.x = wall4.position.x - wall2.position.x;
        wall1.scale.z = PlanViewSettings.WALL_SIZE;

        wall2.scale.z = wall3.position.z - wall1.position.z;
        wall2.scale.x = PlanViewSettings.WALL_SIZE;

        wall3.scale.x = wall4.position.x - wall2.position.x;
        wall3.scale.z = PlanViewSettings.WALL_SIZE;

        wall4.scale.z = wall1.position.z - wall3.position.z;
        wall4.scale.x = PlanViewSettings.WALL_SIZE;

        var wall1Class = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall1);
        var wall2Class = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall2);
        var wall3Class = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall3);
        var wall4Class = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall4);

        wall1Class.vertex1 = new THREE.Vector2(wall1.position.x, wall1.position.z);
        wall1Class.vertex2 = new THREE.Vector2(wall1.position.x + wall1.scale.x, wall1.position.z);

        wall2Class.vertex1 = new THREE.Vector2(wall2.position.x, wall2.position.z);
        wall2Class.vertex2 = new THREE.Vector2(wall2.position.x, wall2.position.z + wall2.scale.z);

        wall3Class.vertex1 = new THREE.Vector2(wall2.position.x, wall2.position.z + wall2.scale.z);
        wall3Class.vertex2 = new THREE.Vector2(wall1.position.x + wall1.scale.x, wall2.position.z + wall2.scale.z);

        wall4Class.vertex1 = new THREE.Vector2(wall1.position.x + wall1.scale.x, wall1.position.z);
        wall4Class.vertex2 = new THREE.Vector2(wall1.position.x + wall1.scale.x, wall2.position.z + wall2.scale.z);
    }

    static highlightWall(walls, raycaster) {

        var outlinePass = PlanViewEnvironment.getInstance().outlinePass;
        var selectedObjects = [];

        if(walls) {
          const intersectsWalls = raycaster.intersectObjects(walls);

          if (intersectsWalls.length > 0) {
            selectedObjects.push(intersectsWalls[0].object);
            outlinePass.selectedObjects = selectedObjects;
            return intersectsWalls[0].object;
          }
          outlinePass.selectedObjects = [];
        }
        return null;
    }

    static highlightRoom(walls, raycaster) {

        let rooms: Room[] = PlanViewContext.getInstance().getRooms();

        var outlinePass = PlanViewEnvironment.getInstance().outlinePass;
        var selectedObjects = [];

        const intersectsFloors = raycaster.intersectObjects(PlanViewContext.getInstance().getFloors());

        if (intersectsFloors.length > 0) {
            var intersectedFloor = intersectsFloors[0].object;

            for (var y = 0; y < rooms.length; y++) {
                var room: Room = rooms[y];
                if (room.floor == intersectedFloor) {
                    for (var wallIndex = 0; wallIndex < room.walls.length; wallIndex++) {
                        selectedObjects.push(room.walls[wallIndex]);
                    }
                    outlinePass.selectedObjects = selectedObjects;
                    return room;
                }
            }
        }
        outlinePass.selectedObjects = [];
        return null;
    }

    static highlightObject(models3D, raycaster, type = "mesh") {

        var outlinePass = PlanViewEnvironment.getInstance().outlinePass;
        var selectedObjects = [];

        const intersectObjects = raycaster.intersectObjects(models3D, true);

        if (intersectObjects.length > 0) {
            var obj = intersectObjects[0].object;

            if(type != "mesh") {
                if (obj.parent) {
                    obj = obj.parent;
                }
            }
            selectedObjects.push(obj);
            outlinePass.selectedObjects = selectedObjects;
            return obj;
        }
        outlinePass.selectedObjects = [];
        return null;
    }

    static markObjectsAsValid(objects) {

        var disableOutlinePass = PlanViewEnvironment.getInstance().disableOutlinePass;
        disableOutlinePass.selectedObjects = [];

        var enableOutlinePass = PlanViewEnvironment.getInstance().enableOutlinePass;
        enableOutlinePass.selectedObjects = objects;
    }

    static markObjectsAsInvalid(objects) {

        var enableOutlinePass = PlanViewEnvironment.getInstance().enableOutlinePass;
        enableOutlinePass.selectedObjects = [];

        var disableOutlinePass = PlanViewEnvironment.getInstance().disableOutlinePass;
        disableOutlinePass.selectedObjects = objects;
    }

    static selectWall(wall) {

        var outlinePass = PlanViewEnvironment.getInstance().selectionOutlinePass;
        var selectedObjects = [];
        selectedObjects.push(wall);
        outlinePass.selectedObjects = selectedObjects;
    }

    static unselectObjects() {

        var outlinePass = PlanViewEnvironment.getInstance().selectionOutlinePass;
        outlinePass.selectedObjects = [];
    }

    static unmarkObjects() {

        var disableOutlinePass = PlanViewEnvironment.getInstance().disableOutlinePass;
        disableOutlinePass.selectedObjects = [];

        var enableOutlinePass = PlanViewEnvironment.getInstance().enableOutlinePass;
        enableOutlinePass.selectedObjects = [];
    }

    static selectRoom(room: Room) {
        var outlinePass = PlanViewEnvironment.getInstance().selectionOutlinePass;
        var selectedObjects = [];

        for (var wallIndex = 0; wallIndex < room.walls.length; wallIndex++) {
            selectedObjects.push(room.walls[wallIndex]);
        }
        outlinePass.selectedObjects = selectedObjects;

    }

    static selectObject(object: Object) {
        var outlinePass = PlanViewEnvironment.getInstance().selectionOutlinePass;
        var selectedObjects = [];
        selectedObjects.push(object);
        outlinePass.selectedObjects = selectedObjects;
    }

    static colorObject(object: THREE.Mesh, material) {
        object.material = material;
    }

    static paintWalls(wallArray: THREE.Mesh[], paintedWallsMap: Map<Wall, number>, wallpaperTexure: string, outsideTexure: string) {
        const loader = new THREE.TextureLoader();
        // var wallpaper_material: THREE.MeshLambertMaterial = Drawings.createMeshMaterial(loader, 0xF2EEE8, wallpaperTexure, 0, this.currentPorch.scale.x / 500, this.currentPorch.scale.z / 500);
        // const undressed_material: THREE.MeshLambertMaterial = Drawings.createMeshMaterial(loader, 0xffffff, outsideTexure, 0);

        //каждому классу стены добавить свойство - массив, в котором будут содержаться цифры 0, 4 или 0 и 4
        //в зависимости от того, какие цифры содержаться в этом массиве, нужно создавать mixedMaterials

        var room: Room = PlanViewContext.getInstance().wallsManager.findRoomByWallArray(wallArray);

        for (var i = 0; i < wallArray.length; i++) {

            let mesh: THREE.Mesh = wallArray[i];

            var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(mesh);

            let repeatX;
            if (wallClass.wallAxis == "x") {
                repeatX = WallsPoints.getNumberByModule(mesh.scale.x / 500);
            } else {
                repeatX = WallsPoints.getNumberByModule(mesh.scale.z / 500);
            }
            var wallpaper_material: THREE.MeshLambertMaterial = Materials.createMeshMaterial(loader, 0xF2EEE8, wallpaperTexure, 0, repeatX, 1);
            var undressed_material: THREE.MeshLambertMaterial = Materials.createMeshMaterial(loader, 0xffffff, outsideTexure, 0, repeatX, 1);
            if (room != null) {
                wallpaper_material = Materials.createMeshMaterial(loader, 0xF2EEE8, room.wallpapersTexture, 0, repeatX, 1);
            }

            var mixedMaterials: THREE.MeshLambertMaterial[] = wallClass.mixedMaterials;
            if (mixedMaterials == null) {
                mixedMaterials = [
                    undressed_material,
                    undressed_material,
                    undressed_material,
                    undressed_material,
                    undressed_material,
                    undressed_material
                ];
            } else {
                for (var p = 0; p < mixedMaterials.length; p++) {
                    if (!wallClass.paintedWallsNumbers.includes(p)) {
                        mixedMaterials[p] = undressed_material;
                    }
                }
            }

            var paintedWallNumber: number = paintedWallsMap.get(wallClass);
            mixedMaterials[paintedWallNumber] = wallpaper_material;

            wallClass.mixedMaterials = mixedMaterials;

            Drawings.colorObject(mesh, mixedMaterials);
        }
    }
}
