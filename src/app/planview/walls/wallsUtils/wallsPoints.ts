import * as THREE from 'three';
import {Wall} from "../elements/wall";
import {PlanViewContext} from "../../context/planViewContext";

export class WallsPoints {
    static isChangeHorizontal(initialPosition, currentPosition) {
        var initialXModule = initialPosition.x < 0 ? -1 * initialPosition.x : initialPosition.x;
        var initialYModule = initialPosition.z < 0 ? -1 * initialPosition.z : initialPosition.z;

        var currentXModule = currentPosition.x < 0 ? -1 * currentPosition.x : currentPosition.x;
        var currentYModule = currentPosition.z < 0 ? -1 * currentPosition.z : currentPosition.z;

        var changeX = currentXModule - initialXModule;
        var changeY = currentYModule - initialYModule;

        var changeXModule = changeX < 0 ? -1 * changeX : changeX;
        var changeYModule = changeY < 0 ? -1 * changeY : changeY;

        return changeXModule >= changeYModule;
    }

    static getNumberByModule(number) {
        return number < 0 ? -1 * number : number;
    }

    static collectUnicPoints(wallArray):THREE.Vector2[] {
        var pointsArray:THREE.Vector2[] = [];
        for (var i = 0; i < wallArray.length; i++) {
            var wall = wallArray[i];
            var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall);

          var crossPoint:THREE.Vector2;
            if (i + 1 < wallArray.length) {
                var nextWall = wallArray[i + 1];
                var nextWallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(nextWall);

                crossPoint = WallsPoints.findCrossPoint(wallClass, nextWallClass);
                pointsArray.push(crossPoint);
            } else {
                var firstWall = wallArray[0];
                var firstWallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(firstWall);

                crossPoint = WallsPoints.findCrossPoint(wallClass, firstWallClass);
                pointsArray.push(crossPoint);
            }
        }
        return pointsArray;
    }

    private static findCrossPoint(wallClass: Wall, nextWallClass: Wall):THREE.Vector2 {
        if (wallClass.vertex1.equals(nextWallClass.vertex1)) {
            return wallClass.vertex1;
        }
        if (wallClass.vertex1.equals(nextWallClass.vertex2)) {
            return wallClass.vertex1;
        }
        if (wallClass.vertex2.equals(nextWallClass.vertex1)) {
            return wallClass.vertex2;
        }
        if (wallClass.vertex2.equals(nextWallClass.vertex2)) {
            return wallClass.vertex2;
        }
    }

    static EPS = 1E-9;

    static det(a, b, c, d) {
        return a * d - b * c;
    }

    static between(a, b, c) {
        return Math.min(a, b) <= c + WallsPoints.EPS && c <= Math.max(a, b) + WallsPoints.EPS;
    }

    static intersect_1(a, b, c, d) {
        if (a > b) {
            b = [a, a = b][0];
        }
        if (c > d) {
            d = [c, c = d][0];
        }
        return Math.max(a, c) <= Math.min(b, d);
    }

    static intersect(wall1Vertex1, wall1Vertex2, wall2Vertex1, wall2Vertex2) {
        var A1 = wall1Vertex1.y - wall1Vertex2.y, B1 = wall1Vertex2.x - wall1Vertex1.x,
            C1 = -A1 * wall1Vertex1.x - B1 * wall1Vertex1.y;
        var A2 = wall2Vertex1.y - wall2Vertex2.y, B2 = wall2Vertex2.x - wall2Vertex1.x,
            C2 = -A2 * wall2Vertex1.x - B2 * wall2Vertex1.y;
        var zn = WallsPoints.det(A1, B1, A2, B2);
        if (zn != 0) {
            var x = -WallsPoints.det(C1, B1, C2, B2) / zn;
            var y = -WallsPoints.det(A1, C1, A2, C2) / zn;
            if (WallsPoints.between(wall1Vertex1.x, wall1Vertex2.x, x) && WallsPoints.between(wall1Vertex1.y, wall1Vertex2.y, y)
                && WallsPoints.between(wall2Vertex1.x, wall2Vertex2.x, x) && WallsPoints.between(wall2Vertex1.y, wall2Vertex2.y, y)) {
                return new THREE.Vector2(x, y);
            } else {
                return false;
            }
        } else
            return WallsPoints.det(A1, C1, A2, C2) == 0 && WallsPoints.det(B1, C1, B2, C2) == 0
                && WallsPoints.intersect_1(wall1Vertex1.x, wall1Vertex2.x, wall2Vertex1.x, wall2Vertex2.x)
                && WallsPoints.intersect_1(wall1Vertex1.y, wall1Vertex2.y, wall2Vertex1.y, wall2Vertex2.y);
    }

    static bothVericesAreEqual(currentWallVertex1: THREE.Vector2, wallVertex1: THREE.Vector2, currentWallVertex2: THREE.Vector2, wallVertex2: THREE.Vector2) {
        return (currentWallVertex1.equals(wallVertex1) && currentWallVertex2.equals(wallVertex2)) ||
            (currentWallVertex2.equals(wallVertex1) && currentWallVertex1.equals(wallVertex2));
    }

    static atLeastOneVertexIsEqual(currentWallVertex1: THREE.Vector2, wallVertex1: THREE.Vector2, wallVertex2: THREE.Vector2, currentWallVertex2: THREE.Vector2) {
        return currentWallVertex1.equals(wallVertex1) ||
            currentWallVertex1.equals(wallVertex2) ||
            currentWallVertex2.equals(wallVertex1) ||
            currentWallVertex2.equals(wallVertex2);
    }

    static angleIs90(wallVertex1, wallVertex2, currentWallVertex1, currentWallVertex2) {
        if (this.allNumbersAreEqual(wallVertex1.x, wallVertex2.x, currentWallVertex1.x, currentWallVertex2.x) ||
            this.allNumbersAreEqual(wallVertex1.y, wallVertex2.y, currentWallVertex1.y, currentWallVertex2.y)) {
            return false;
        } else {
            return true;
        }
    }

    private static allNumbersAreEqual(wallVertex1, wallVertex2, currentWallVertex1, currentWallVertex2) {
        return wallVertex1 == wallVertex2 &&
            wallVertex1 == currentWallVertex1 &&
            wallVertex1 == currentWallVertex2 &&
            currentWallVertex1 == currentWallVertex2;
    }

    static isWallsOpposite(wall1Class: Wall, wall2Class: Wall) {
        if (wall1Class.wallAxis != wall2Class.wallAxis) {
            return false;
        }
        if(WallsPoints.atLeastOneVertexIsEqual(wall1Class.vertex1, wall2Class.vertex1, wall2Class.vertex2, wall1Class.vertex2)){
            return false;
        }
        if (wall1Class.wallAxis == "x") {
            var l1 = null;
            var r1 = null;
            var l2 = null;
            var r2 = null;

            if (wall1Class.vertex1.x > wall1Class.vertex2.x) {
                l1 = wall1Class.vertex2.x;
                r1 = wall1Class.vertex1.x;
            } else {
                l1 = wall1Class.vertex1.x;
                r1 = wall1Class.vertex2.x;
            }

            if (wall2Class.vertex1.x > wall2Class.vertex2.x) {
                l2 = wall2Class.vertex2.x;
                r2 = wall2Class.vertex1.x;
            } else {
                l2 = wall2Class.vertex1.x;
                r2 = wall2Class.vertex2.x;
            }

            if (l1 < l2) {
                return r1 >= l2;
            } else {
                return r2 >= l1;
            }
        } else {
            var t1 = null;
            var b1 = null;
            var t2 = null;
            var b2 = null;

            if (wall1Class.vertex1.y > wall1Class.vertex2.y) {
                t1 = wall1Class.vertex2.y;
                b1 = wall1Class.vertex1.y;
            } else {
                t1 = wall1Class.vertex1.y;
                b1 = wall1Class.vertex2.y;
            }

            if (wall2Class.vertex1.y > wall2Class.vertex2.y) {
                t2 = wall2Class.vertex2.y;
                b2 = wall2Class.vertex1.y;
            } else {
                t2 = wall2Class.vertex1.y;
                b2 = wall2Class.vertex2.y;
            }

            if (t1 < t2) {
                return b1 >= t2;
            } else {
                return b2 >= t1;
            }
        }
    }
}
