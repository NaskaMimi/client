import * as THREE from 'three';
import {PlanViewContext} from "../../context/planViewContext";
import {WallsPoints} from "./wallsPoints";
import {Room} from "../elements/room";

export class ClosedSpaceService {

    finalWalls: THREE.Mesh[];

    public calculateClosedSpaceVertecesForLevel(level: number) {
        var wallsArray: THREE.Mesh[] = [];

        ClosedSpaceService.getVirtualOutsideWalls(wallsArray, PlanViewContext.getInstance().getRoomsByLevel(level));
        this.findClosedSpace(wallsArray[0], [], wallsArray[0], wallsArray);

        console.log("walls = " + this.finalWalls);
        return WallsPoints.collectUnicPoints(this.finalWalls);
    }

    private static getVirtualOutsideWalls(wallsArray: THREE.Mesh[], rooms: Room[]) {
        for (var room of rooms) {
            var roomWallsClone: THREE.Mesh[] = [];

            ClosedSpaceService.addAllElementsToArray(roomWallsClone, room.walls);

            for (var wall of roomWallsClone) {
                if (wallsArray.includes(wall)) {
                    wallsArray.splice(wallsArray.indexOf(wall), 1);
                    roomWallsClone.splice(roomWallsClone.indexOf(wall), 1);
                }
            }
            ClosedSpaceService.addAllElementsToArray(wallsArray, roomWallsClone);
        }
    }

    private findClosedSpace(wall: THREE.Mesh, wallArray: THREE.Mesh[], firstWall, walls) {

        if (wallArray.includes(wall)) {
            if (wall == firstWall) {
                if (wallArray.length > 3 && PlanViewContext.getInstance().wallsManager.allIntersectionsMoreOne(wallArray, walls)) {
                    this.finalWalls = wallArray;
                }
            }
        } else {
            var newWallArray: THREE.Mesh[] = [...wallArray];
            newWallArray.push(wall);
            var intersectedWalls = PlanViewContext.getInstance().wallsManager.findIntersectedWalls(wall, walls);
            for (var i = 0; i < intersectedWalls.length; i++) {
                var intersectedWall = intersectedWalls[i];
                this.findClosedSpace(intersectedWall, newWallArray, firstWall, walls);
            }
        }
    }

    private static addAllElementsToArray(targetArray: THREE.Mesh[], elementsArray: THREE.Mesh[]) {
        for (var element of elementsArray) {
            targetArray.push(element);
        }
    }

}