import * as THREE from 'three';
import {PlanViewContext} from "../context/planViewContext";
import {PlanViewSettings} from "../configuration/planViewSettings";
import {PlanViewEnvironment} from "../context/planViewEnvironment";

let textureLoader = new THREE.TextureLoader();

export class Materials{
    public static cubeMaterial = new THREE.MeshPhongMaterial({color: 0xB76357});
    public static rollOverMaterial = new THREE.MeshBasicMaterial({color: 0x000000});
    public static blue_glass_material = new THREE.MeshPhongMaterial({
        color: 0x7AFAE8,
        transparent: true,
        emissive: 0x001b3d,
        specular: 0xffffff,
        shininess: 100,
        opacity: 0.5
    });

    public static glass_material = new THREE.MeshPhongMaterial({
        color: 0xffffff,
        transparent: true,
        emissive: 0x001b3d,
        specular: 0xffffff,
        shininess: 100,
        opacity: 0.5
    });

    public static dark_glass_material = new THREE.MeshPhongMaterial({
        color: 0x3E3232,
        transparent: true,
        emissive: 0x001b3d,
        specular: 0xffffff,
        shininess: 100,
        opacity: 0.5
    });

    public static tulle_material = new THREE.MeshLambertMaterial({
        color: 0xffffff,
        side: THREE.DoubleSide,
        transparent: true,
        opacity: 0.5
    });

    public static gold_material = new THREE.MeshPhongMaterial({
        color: 0xD9D302,
        transparent: false,
        specular: 0xffffff,
        shininess: 100
    });

    public static silver_material = new THREE.MeshPhongMaterial({
        color: 0xABABAB,
        transparent: false,
        specular: 0xffffff,
        shininess: 100
    });

    public static black_metal_material = new THREE.MeshPhongMaterial({
        color: 0x000000,
        transparent: false,
        specular: 0xffffff,
        shininess: 100
    });

    public static dark_metal_material = new THREE.MeshPhongMaterial({
        color: 0x261D1D,
        side: THREE.DoubleSide,
        transparent: false,
        specular: 0xffffff,
        shininess: 100
    });

    public static createMixedMaterial(texture: string, repeatX = 2, repeatZ = 1) {
        var material = Materials.createMeshMaterial(textureLoader, 0xffffff, PlanViewContext.getInstance().foundationTexture, 0, 2, 0.3);
        var floorMaterial = Materials.createMeshMaterial(textureLoader, 0xffffff, texture, 0, repeatX, repeatZ);

        return [
            material,
            material,
            floorMaterial,
            floorMaterial,
            material,
            material
        ];
    }

  public static createMeshMaterial(loader: THREE.TextureLoader, defaultColor, defaultTexture: string, rotation: number = 0, repeatX = 2, repeatZ = 1) {
    return new THREE.MeshPhongMaterial({
      color: defaultColor,
      map: loader.load(defaultTexture, function (texture) {
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.offset.set(0, 0);
        texture.repeat.set(repeatX, repeatZ);
        texture.needsUpdate = true;
        texture.rotation = rotation;
        PlanViewEnvironment.getInstance().render();

      })
    });
  }

  public static createDoubleSideMeshMaterial(loader: THREE.TextureLoader, defaultColor, defaultTexture: string, rotation: number = 0, repeatX = 2, repeatZ = 2) {
    return new THREE.MeshPhongMaterial({
      color: defaultColor,
      side: THREE.DoubleSide,
      map: loader.load(defaultTexture, function (texture) {

        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.offset.set(0.1, 0.1);
        texture.repeat.set(repeatX, repeatZ);
        texture.rotation = rotation;
        PlanViewEnvironment.getInstance().render();

      })
    });
  }

    private static velvet1 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet1.jpg", -Math.PI / 2, 6, 3);
    private static  velvet2 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet2.jpg", -Math.PI / 2, 6, 3);
    private static  velvet3 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet3.jpg", -Math.PI / 2, 6, 3);
    private static  velvet4 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet4.jpg", -Math.PI / 2, 6, 3);
    private static  velvet5 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet5.jpg", -Math.PI / 2, 6, 3);
    private static  velvet6 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet6.jpg", -Math.PI / 2, 6, 3);
    private static  velvet7 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet7.jpg", -Math.PI / 2, 3, 3);
    private static  velvet8 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet8.jpg", -Math.PI / 2, 6, 3);
    private static velvet9 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet9.jpg", -Math.PI / 2, 6, 3);
    private static  velvet10 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet10.jpg", -Math.PI / 2, 6, 3);
    private static  velvet11 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet11.jpg", -Math.PI / 2, 6, 3);
    private static  velvet12 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet12.jpg", -Math.PI / 2, 6, 3);
    private static  velvet13 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velvet13.jpg", -Math.PI / 2, 6, 3);

    private static  velours1 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velours1.jpg", -Math.PI / 2, 6, 3);
    private static  velours2 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velours2.jpg", -Math.PI / 2, 6, 3);
    private static  velours3 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velours3.jpg", -Math.PI / 2, 6, 3);
    private static  velours4 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velours4.jpg", -Math.PI / 2, 6, 3);
    private static  velours5 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velours5.jpg", -Math.PI / 2, 6, 3);
    private static  velours6 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velours6.jpg", -Math.PI / 2, 6, 3);
    private static  velours7 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velours7.jpg", -Math.PI / 2, 6, 3);
    private static  velours8 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velours8.jpg", -Math.PI / 2, 6, 3);
    private static  velours9 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/velours9.jpg", -Math.PI / 2, 6, 3);

    private static  painted1 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted1.jpg", -Math.PI / 2, 6, 3);
    private static  painted4 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted4.jpg", -Math.PI / 2, 6, 3);
    private static  painted6 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted6.jpg", -Math.PI / 2, 6, 3);
    private static  painted8 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted8.jpg", -Math.PI / 2, 6, 3);
    private static  painted9 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted9.jpg", -Math.PI / 2, 6, 3);
    private static  painted10 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted10.jpg", -Math.PI / 2, 6, 3);
    private static  painted11 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted11.jpg", -Math.PI / 2, 6, 3);
    private static  painted12 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted12.jpg", -Math.PI / 2, 6, 3);
    private static  painted16 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted16.jpg", -Math.PI / 2, 6, 3);
    private static  painted17 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted17.jpg", -Math.PI / 2, 6, 3);
    private static  painted18 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted18.jpg", -Math.PI / 2, 6, 3);
    private static  painted19 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted19.jpg", -Math.PI / 2, 6, 3);
    private static  painted20 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/painted20.jpg", -Math.PI / 2, 6, 3);

    private static bricks1 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/bricks1.jpg", 0, 4, 2);
    private static bricks3 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/bricks3.jpg", 0, 1, 1);
    private static small_bricks3 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/bricks3.jpg", 0, 4, 4);
    private static rotated_bricks1 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/bricks2.jpg", -Math.PI / 2, 6, 3);

    private static plaster1 = Materials.createDoubleSideMeshMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/plaster1.jpg", -Math.PI / 2, 6, 3);

    private static  wood1 = Materials.createDoubleSideWoodMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/wood1.jpg", -Math.PI / 2, 2, 1);
    private static  wood2 = Materials.createDoubleSideWoodMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/wood2.jpg", -Math.PI / 2, 2, 1);
    private static  wood3 = Materials.createDoubleSideWoodMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/wood3.jpg", -Math.PI / 2, 2, 1);
    private static  wood4 = Materials.createDoubleSideWoodMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/wood4.jpg", -Math.PI / 2, 2, 1);
    private static  wood5 = Materials.createDoubleSideWoodMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/wood5.jpg", -Math.PI / 2, 2, 1);
    private static  rotated_wood1 = Materials.createDoubleSideWoodMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/wood1.jpg", 0, 2, 1);
    private static  rotated_wood2 = Materials.createDoubleSideWoodMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/wood2.jpg", 0, 2, 1);
    private static  rotated_wood3 = Materials.createDoubleSideWoodMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/wood3.jpg", 0, 2, 1);
    private static  rotated_wood4 = Materials.createDoubleSideWoodMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/wood4.jpg", 0, 2, 1);
    private static  rotated_wood5 = Materials.createDoubleSideWoodMaterial(textureLoader, 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/wood5.jpg", 0, 2, 1);

    public static sofas_solid_textures = [
        {color: "black", textures: [Materials.velvet7]},
        {color: "coffe", textures: [Materials.velours8]},
        {color: "dark-brown", textures: [Materials.velours4]},
        {color: "dark-green", textures: [Materials.velvet3]},
        {color: "dark-pink", textures: [Materials.velvet5]},
        {color: "dark-red", textures: [Materials.velours2]},
        {color: "dark-yellow", textures: [Materials.velvet8]},
        {color: "deep-blue", textures: [Materials.velvet9]},
        {color: "lavande", textures: [Materials.velvet12]},
        {color: "light-blue", textures: [Materials.velours6]},
        {color: "lime", textures: [Materials.velvet11]},
        {color: "pink", textures: [Materials.velvet10]},
        {color: "red", textures: [Materials.velours7]},
        {color: "sea", textures: [Materials.velours9]},
        {color: "soft-yellow", textures: [Materials.velvet6]},
        {color: "white", textures: [Materials.velours5]},
        {color: "yellow", textures: [Materials.velvet13]},
        {color: "dark-blue", textures: [Materials.velvet1]},
        {color: "brown", textures: [Materials.velours3]}
    ];

    public static  sofas_triple_textures = [
        {color: "beige-white-darkbrown", textures: [Materials.velours1, Materials.velours5, Materials.velours5, Materials.velours4]},
        {color: "blue-green-yellow", textures: [Materials.velvet1, Materials.painted9, Materials.velours9, Materials.painted11]},
        {color: "brown-white-beige", textures: [Materials.velours3, Materials.painted8, Materials.painted12, Materials.velours1]},
        {color: "darkbrown-white-brown", textures: [Materials.velours4, Materials.painted16, Materials.painted8, Materials.painted8]},
        {color: "darkgreen-white-green", textures: [Materials.velvet3, Materials.velours5, Materials.painted1, Materials.painted11]},
        {color: "darkgrey-white-beige", textures: [Materials.velvet4, Materials.velvet2, Materials.velours3, Materials.velours3]},
        {color: "darkpink-white-pink", textures: [Materials.velvet5, Materials.velvet10, Materials.velvet10, Materials.velours5]},
        {color: "grey-darkgrey-beige", textures: [Materials.velvet2, Materials.velours1, Materials.velvet4, Materials.velours3]},
        {color: "grey-green-yellow", textures: [Materials.velvet2, Materials.painted9, Materials.painted4, Materials.painted11]},
        {color: "red-green-pink", textures: [Materials.velours2, Materials.painted9, Materials.painted6, Materials.painted10]},
        {color: "white-black-darkbrown", textures: [Materials.velvet7, Materials.velours1, Materials.velours8, Materials.velours4]},
        {color: "white-black-darkgreen", textures: [Materials.velours5, Materials.velvet3, Materials.velvet3, Materials.painted17]},
        {color: "white-blue-pink", textures: [Materials.velours5, Materials.velvet1, Materials.velvet1, Materials.painted18]},
        {color: "yellow-white-cyan", textures: [Materials.velvet6, Materials.painted19, Materials.velours6, Materials.painted20]}
    ];

    public static  bed_triple_textures = [
        {color: "blue-green-yellow", textures: [Materials.wood1, Materials.painted9, Materials.velours9, Materials.painted11]},
        {color: "beige-white-darkbrown", textures: [Materials.wood1, Materials.velvet9, Materials.velours5, Materials.velvet1]},
        {color: "brown-white-beige", textures: [Materials.wood1, Materials.painted8, Materials.painted12, Materials.velours1]},
        {color: "darkbrown-white-brown", textures: [Materials.wood1, Materials.painted16, Materials.painted8, Materials.painted8]},
        {color: "darkgreen-white-green", textures: [Materials.wood1, Materials.velours5, Materials.painted1, Materials.painted11]},
        {color: "darkgrey-white-beige", textures: [Materials.wood1, Materials.velvet2, Materials.velours3, Materials.velours3]},
        {color: "darkpink-white-pink", textures: [Materials.wood1, Materials.velvet10, Materials.velvet10, Materials.velours5]},
        {color: "grey-darkgrey-beige", textures: [Materials.wood1, Materials.velours1, Materials.velvet4, Materials.velours3]},
        {color: "grey-green-yellow", textures: [Materials.wood1, Materials.painted9, Materials.painted4, Materials.painted11]},
        {color: "red-green-pink", textures: [Materials.wood1, Materials.painted9, Materials.painted6, Materials.painted10]},
        {color: "white-black-darkbrown", textures: [Materials.wood1, Materials.velours1, Materials.velours8, Materials.velours4]},
        {color: "white-black-darkgreen", textures: [Materials.wood1, Materials.velvet3, Materials.velvet3, Materials.painted17]},
        {color: "white-blue-pink", textures: [Materials.wood1, Materials.velvet1, Materials.velvet1, Materials.painted18]},
        {color: "yellow-white-cyan", textures: [Materials.wood1, Materials.painted19, Materials.velours6, Materials.painted20]}
    ];

    public static  metall_glass_textures = [
        {color: "gold-white", textures: [Materials.gold_material, Materials.glass_material]},
        {color: "gold-brown", textures: [Materials.gold_material, Materials.dark_glass_material]},
        {color: "silver-white", textures: [Materials.silver_material, Materials.glass_material]},
        {color: "silver-brown", textures: [Materials.silver_material, Materials.dark_glass_material]}
    ];

    public static  metall_textures = [
        {color: "yellow", textures: [Materials.gold_material]},
        {color: "white", textures: [Materials.silver_material]}
    ];

    public static  wood_textures = [
        {color: "brown", textures: [Materials.rotated_wood1]},
        {color: "light-brown", textures: [Materials.rotated_wood2]},
        {color: "coffe", textures: [Materials.rotated_wood3]},
        {color: "white", textures: [Materials.rotated_wood4]},
        {color: "dark-brown", textures: [Materials.rotated_wood5]}
    ];

    public static  non_rotated_wood_textures = [
        {color: "brown", textures: [Materials.wood1]},
        {color: "light-brown", textures: [Materials.wood2]},
        {color: "coffe", textures: [Materials.wood3]},
        {color: "white", textures: [Materials.wood4]},
        {color: "dark-brown", textures: [Materials.wood5]}
    ];

    public static outside_stair1_textures = [
        {color: "brown", textures: [Materials.velvet2, Materials.bricks1, Materials.wood1]},
        {color: "light-brown", textures: [Materials.velvet2, Materials.bricks1, Materials.wood2]},
        {color: "coffe", textures: [Materials.velvet2, Materials.bricks1, Materials.wood3]},
        {color: "white", textures: [Materials.velvet2, Materials.bricks1, Materials.wood4]},
        {color: "dark-brown", textures: [Materials.velvet2, Materials.bricks1, Materials.wood5]}
    ];

    public static  curtains_double_textures = [
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velours1]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velours2]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velours3]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velours4]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velours5]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velours6]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velours7]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velours8]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velours9]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet1]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet1]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet2]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet3]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet4]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet5]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet6]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet7]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet8]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet9]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet10]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet11]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet12]},
        {color: "gold-white", textures: [Materials.tulle_material, Materials.velvet13]}
    ];

    public static fireplace_textures = [
        {color: "silver-brown", textures: [Materials.wood1, Materials.dark_metal_material, Materials.black_metal_material, Materials.bricks1, Materials.black_metal_material]},
        {color: "silver-brown", textures: [Materials.wood5, Materials.dark_metal_material, Materials.black_metal_material, Materials.small_bricks3, Materials.black_metal_material]},
        {color: "silver-brown", textures: [Materials.wood2, Materials.dark_metal_material, Materials.black_metal_material, Materials.plaster1, Materials.black_metal_material]},
        {color: "silver-brown", textures: [Materials.wood4, Materials.dark_metal_material, Materials.black_metal_material, Materials.plaster1, Materials.black_metal_material]}
    ];

    public static fireplace2_textures = [
        {color: "silver-brown", textures: [Materials.black_metal_material, Materials.bricks1, Materials.rotated_wood1, Materials.dark_metal_material]},
        {color: "silver-brown", textures: [Materials.black_metal_material, Materials.plaster1, Materials.rotated_wood2, Materials.dark_metal_material]},
        {color: "silver-brown", textures: [Materials.black_metal_material, Materials.plaster1, Materials.rotated_wood4, Materials.dark_metal_material]}
    ];

    public static fireplace3_textures = [
        {color: "silver-brown", textures: [Materials.rotated_wood1, Materials.black_metal_material, Materials.black_metal_material, Materials.glass_material]}
    ];

    public static fireplace4_textures = [
        {color: "white", textures: [Materials.bricks3, Materials.rotated_wood5, Materials.dark_metal_material]},
        {color: "white", textures: [Materials.plaster1, Materials.rotated_wood2, Materials.dark_metal_material]}
    ];

    public static bottom_kitchen_textures = [
        {color: "black-dark-brown", textures: [Materials.silver_material, Materials.wood1, Materials.dark_metal_material, Materials.black_metal_material]},
        {color: "white-wood", textures: [Materials.silver_material, Materials.wood2, Materials.dark_metal_material, Materials.velours5]},
        {color: "white-coffe", textures: [Materials.silver_material, Materials.wood3, Materials.velours8, Materials.velours5]},
        {color: "white-light-brown", textures: [Materials.silver_material, Materials.wood4, Materials.velours8, Materials.velours5]},
        {color: "white-brown", textures: [Materials.silver_material, Materials.velours5, Materials.velvet2, Materials.rotated_wood2]}
    ];

    public static top_kitchen_textures = [
        {color: "black-dark-brown", textures: [Materials.silver_material, Materials.wood1, Materials.dark_metal_material]},
        {color: "white-wood", textures: [Materials.silver_material, Materials.wood2, Materials.dark_metal_material]},
        {color: "white-coffe", textures: [Materials.silver_material, Materials.wood3, Materials.velours8]},
        {color: "white-light-brown", textures: [Materials.silver_material, Materials.wood4, Materials.velours8]},
        {color: "white-brown", textures: [Materials.silver_material, Materials.velours5, Materials.velvet2]}
    ];

    public static mirror1_textures = [
        {color: "silver-brown", textures: [Materials.velours5, Materials.rotated_wood1, Materials.silver_material]},
        {color: "silver-brown", textures: [Materials.velours5, Materials.rotated_wood4, Materials.silver_material]},
        {color: "silver-brown", textures: [Materials.velvet1, Materials.rotated_wood1, Materials.dark_metal_material]},
        {color: "silver-brown", textures: [Materials.velvet3, Materials.rotated_wood1, Materials.dark_metal_material]},
        {color: "silver-brown", textures: [Materials.velours2, Materials.rotated_wood1, Materials.dark_metal_material]},
        {color: "silver-brown", textures: [Materials.velours9, Materials.rotated_wood2, Materials.gold_material]}
    ];

    public static mirror2_textures = [
        {color: "silver-brown", textures: [Materials.wood1, Materials.silver_material]},
        {color: "silver-brown", textures: [Materials.wood2, Materials.silver_material]},
        {color: "silver-brown", textures: [Materials.wood3, Materials.silver_material]},
        {color: "silver-brown", textures: [Materials.wood4, Materials.silver_material]},
        {color: "silver-brown", textures: [Materials.wood5, Materials.silver_material]}
    ];

    static createDoubleSideWoodMaterial(loader: THREE.TextureLoader, defaultColor, defaultTexture: string, rotation: number = 0, repeatX = 2, repeatZ = 2) {
        return new THREE.MeshPhongMaterial({
            color: defaultColor,
            specular: 0xffffff,
            shininess: 100,
            side: THREE.DoubleSide,
            map: loader.load(defaultTexture, function (texture) {

                texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                texture.offset.set(0.1, 0.1);
                texture.repeat.set(repeatX, repeatZ);
                texture.rotation = rotation;
                // PlanViewEnvironment.getInstance().render();

            })
        });
    }
}
