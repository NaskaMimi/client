import {WallsPoints} from "./wallsUtils/wallsPoints";
import {Drawings} from "./wallsUtils/drawings";
import {Wall} from "./elements/wall";
import * as THREE from 'three';
import {Room} from "./elements/room";
import {PlanViewContext} from "../context/planViewContext";
import {PlanViewSettings} from "../configuration/planViewSettings";

export class WallsManager {
    wallsClasses = [];

    addWall(wall: Wall) {
        this.wallsClasses.push(wall);
    }

    deleteWall(wall) {
        this.wallsClasses.splice(this.wallsClasses.indexOf(wall), 1);
    }

    findWallClassByWall(wall) {
        for (var i = 0; i < this.wallsClasses.length; i++) {
            var wallClass = (this.wallsClasses)[i];
            if (wallClass.wall == wall) {
                return wallClass;
            }
        }
        return null;
    }

    findRoomByFloor(floor){
        let rooms: Room[] = PlanViewContext.getInstance().getRooms();
        for (var y = 0; y < rooms.length; y++) {
            var room: Room = rooms[y];
            if (room.floor == floor) {
                return room;
            }
        }
        return null;
    }

    findIntersectedWalls(currentWall: THREE.Mesh, walls) {
        var currentWallClass: Wall = this.findWallClassByWall(currentWall);

        var currentWallVertex1: THREE.Vector2 = currentWallClass.vertex1;
        var currentWallVertex2: THREE.Vector2 = currentWallClass.vertex2;

        var intersections: THREE.Mesh[] = [];
        for (var i = 0; i < walls.length; i++) {
            var wall: THREE.Mesh = walls[i];
            var wallClass: Wall = this.findWallClassByWall(wall);

            var wallVertex1: THREE.Vector2 = wallClass.vertex1;
            var wallVertex2: THREE.Vector2 = wallClass.vertex2;

            if (WallsPoints.atLeastOneVertexIsEqual(currentWallVertex1, wallVertex1, wallVertex2, currentWallVertex2)) {
                if (WallsPoints.bothVericesAreEqual(currentWallVertex1, wallVertex1, currentWallVertex2, wallVertex2)) {
                    continue; //если отрезки полностью совпадают - пропускаем данную стену, т.к. это и есть currentWall
                }

                intersections.push(wall);
            }
        }
        return intersections;
    }

    findClosedSpace(wall: THREE.Mesh, wallArray: THREE.Mesh[], firstWall, walls, deletedRoomWallTexturesMap: Map<THREE.Vector2[], string>, deletedRoomFloorTexturesMap: Map<THREE.Vector2[], string>) {
        var outsideTexture = PlanViewContext.getInstance().outsideTexture;
        if (wallArray.includes(wall)) {
            if (wall == firstWall) {
                if (wallArray.length > 3 && this.allIntersectionsMoreOne(wallArray, walls)) {
                    if (WallsManager.isNoRoomsWithSameWalls(wallArray)) {
                        var paintedWallsMap: Map<Wall, number> = this.assignWallsInnerSide(wallArray);
                        var wallpapersTexture = this.defineWallpapers(wallArray, deletedRoomWallTexturesMap);
                        var floorTexture = this.defineFloor(wallArray, deletedRoomFloorTexturesMap);
                        Drawings.paintWalls(wallArray, paintedWallsMap, wallpapersTexture, outsideTexture);
                        var floorPlane = Drawings.drawFloor(wallArray, floorTexture);
                        PlanViewContext.getInstance().addRoom(new Room("New Room", wallArray, floorPlane, outsideTexture, wallpapersTexture, floorTexture, paintedWallsMap));
                    }
                }
            }
        } else {
            if (this.wallIsValid(wall, wallArray, walls)) {
                var newWallArray: THREE.Mesh[] = [...wallArray];
                newWallArray.push(wall);
                var intersectedWalls = this.findIntersectedWalls(wall, walls);
                for (var i = 0; i < intersectedWalls.length; i++) {
                    var intersectedWall = intersectedWalls[i];
                    this.findClosedSpace(intersectedWall, newWallArray, firstWall, walls, deletedRoomWallTexturesMap, deletedRoomFloorTexturesMap);
                }
            } else {
            }
        }
    }

    private defineWallpapers(wallArray: THREE.Mesh[], deletedRoomWallTexturesMap: Map<THREE.Vector2[], string>) {
        for (var deletedWallArray of deletedRoomWallTexturesMap.keys()) {
            let intersection = wallArray.filter(x => deletedWallArray.includes(x));
            if (intersection.length >= 3) {
                return deletedRoomWallTexturesMap.get(deletedWallArray);
            }
        }
        return PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper1.jpg";
    }

    private defineFloor(wallArray: THREE.Mesh[], deletedRoomWallTexturesMap: Map<THREE.Vector2[], string>) {
        for (var deletedWallArray of deletedRoomWallTexturesMap.keys()) {
            let intersection = wallArray.filter(x => deletedWallArray.includes(x));
            if (intersection.length >= 3) {
                return deletedRoomWallTexturesMap.get(deletedWallArray);
            }
        }
        return PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg";
    }

    private defineOutsideMaterial(wallArray: THREE.Mesh[], deletedRoomWallTexturesMap: Map<THREE.Vector2[], string>) {
        for (var deletedWallArray of deletedRoomWallTexturesMap.keys()) {
            let intersection = wallArray.filter(x => deletedWallArray.includes(x));
            if (intersection.length >= 3) {
                return deletedRoomWallTexturesMap.get(deletedWallArray);
            }
        }
        return PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper4.jpg";
    }

    private assignWallsInnerSide(wallArray: THREE.Mesh[]) {
        var paintedWallsMap: Map<Wall, number> = new Map();

        for (let wall of wallArray) {
            var wallClass: Wall = this.findWallClassByWall(wall);
            for (let currentWall of wallArray) {
                var currentWallClass: Wall = this.findWallClassByWall(currentWall);
                if (wall == currentWall) {
                    continue;
                }
                if (WallsPoints.isWallsOpposite(wallClass, currentWallClass)) {
                    if (wallClass.wallAxis == "x") {
                        if (wall.position.z > currentWall.position.z) {
                            wallClass.addPaintedWallsNumber(5);
                            paintedWallsMap.set(wallClass, 5);
                        } else {
                            wallClass.addPaintedWallsNumber(4);
                            paintedWallsMap.set(wallClass, 4);
                        }
                    } else {
                        if (wall.position.x > currentWall.position.x) {
                            wallClass.addPaintedWallsNumber(1);
                            paintedWallsMap.set(wallClass, 1);
                        } else {
                            wallClass.addPaintedWallsNumber(0);
                            paintedWallsMap.set(wallClass, 0);
                        }
                    }
                }
            }
        }
        return paintedWallsMap;
    }

    logWalls(wallArray: THREE.Mesh[]) {
        for (var i = 0; i < wallArray.length; i++) {
            var wallClass: Wall = this.findWallClassByWall(wallArray[i]);
            console.log(wallArray[i].name + " axis = " + "(" + wallClass.vertex1.x + ", " + wallClass.vertex1.y + "), " + "(" + wallClass.vertex2.x + ", " + wallClass.vertex2.y + ") " + wallClass.wallAxis);
        }
    }

    logWallClasses(wallArray: Wall[]) {
        for (var i = 0; i < wallArray.length; i++) {
            var wallClass: Wall = wallArray[i];
            console.log(" name = " + wallClass.wall.name + " axis = " + "(" + wallClass.vertex1.x + ", " + wallClass.vertex1.y + "), " + "(" + wallClass.vertex2.x + ", " + wallClass.vertex2.y + ") " + wallClass.wallAxis);
        }
    }

    private wallIsValid(wall: THREE.Mesh, wallArray: THREE.Mesh[], walls: THREE.Mesh[]) {

        if (wallArray.length == 0) {
            return true;
        }
        var intersectedWalls = this.findIntersectedWalls(wall, walls);

        if (intersectedWalls.length > 2) {
            var previousWall: THREE.Mesh = wallArray[wallArray.length - 1];
            if (previousWall == null) {
                return false;
            }
            var wallClass: Wall = this.findWallClassByWall(wall);
            if (this.hasHalfCrossroads(previousWall, wall, walls)) {
                if (this.wallsArePerpendicular(wall, previousWall)) {
                    return this.perpendicularWallCase(wall, previousWall, wallArray);
                } else {
                    return this.defineInDependOnDirection(wallClass, wallArray, wall, walls, previousWall);
                }
            } else
                return true;
        } else return true;
    }

    perpendicularWallCase(wall, previousWall, wallArray) {
        var wallClass: Wall = this.findWallClassByWall(wall);
        var previousWallWithSameAxis = this.findPreviousWallWithSameAxis(wallClass, wallArray);

        if (previousWallWithSameAxis != null) {
            var currentWallDirection: String = this.defineDirection(wall, previousWall);
            var previousWallWithSameAxisDirection: String = this.defineDirection(previousWallWithSameAxis, previousWall);

            return currentWallDirection == previousWallWithSameAxisDirection;
        } else return false;
    }

    wallsArePerpendicular(wall1, wall2) {
        var wall1Class: Wall = this.findWallClassByWall(wall1);
        var wall2Class: Wall = this.findWallClassByWall(wall2);
        return WallsPoints.angleIs90(wall1Class.vertex1, wall1Class.vertex2, wall2Class.vertex1, wall2Class.vertex2);
    }

    // @ts-ignore
    defineInDependOnDirection(wallClass, wallArray, wall, walls, previousWall): boolean {
        // /*
        // * найти такую предыдущую стену, чтобы ее аксис отличался от текущего
        // * определить ее дирекшен
        // * у обеих смежных стен найти пересекаемые стены
        // * у полученных массивов пересекаемых стен найти пересечение - найдем "входящую стену"
        // * у входящей стены определить дирекшен
        // * если дирекшены разные, то можно перепрыгнуть - стена валидна, иначе нет
        // * */
        var previousWallWithDifferentAxis = this.findPreviousWallWithDifferentAxis(wallClass, wallArray);
        if (previousWallWithDifferentAxis != null) {

            var intersectedWalls = this.findIntersectedWalls(wall, walls);
            var previousIntersectedWalls = this.findIntersectedWalls(previousWall, walls);

            let intersection = intersectedWalls.filter(x => previousIntersectedWalls.includes(x));
            if (intersection.length > 0) {

                if (intersection.length > 1) {
                    return false;
                }
                var enterWall = intersection[0];

                var enterWallsDirection: String = this.defineDirection(enterWall, wall);
                var previousWallWithDifferentAxisDirection: String = this.defineDirection(previousWallWithDifferentAxis, wall);

                return previousWallWithDifferentAxisDirection != enterWallsDirection;
            }
        } else
            return false;
    }

    private findPreviousWallWithDifferentAxis(wallClass: Wall, wallArray: THREE.Mesh[]) {
        for (var i = wallArray.length - 1; i >= 0; i--) {
            var wallIClass: Wall = this.findWallClassByWall(wallArray[i]);
            if (wallClass.wallAxis != wallIClass.wallAxis) {
                return wallArray[i];
            }
        }
    }

    private findPreviousWallWithSameAxis(wallClass: Wall, wallArray: THREE.Mesh[]) {
        for (var i = wallArray.length - 1; i >= 0; i--) {
            var wallIClass: Wall = this.findWallClassByWall(wallArray[i]);
            if (wallClass.wallAxis == wallIClass.wallAxis) {
                return wallArray[i];
            }
        }
    }

    private hasHalfCrossroads(previousWall: THREE.Mesh, wall: THREE.Mesh, walls: THREE.Mesh[]) {
        var intersectedWalls = this.findIntersectedWalls(wall, walls);
        var previousIntersectedWalls = this.findIntersectedWalls(previousWall, walls);
        let intersection = intersectedWalls.filter(x => previousIntersectedWalls.includes(x));
        let boolean = intersection.length > 0;
        return boolean;

    }

    private defineDirection(definableWall: THREE.Mesh, abuttingWall: THREE.Mesh) {
        var definableWallClass: Wall = this.findWallClassByWall(definableWall);
        var abuttingWallClass: Wall = this.findWallClassByWall(abuttingWall);
        if (definableWallClass.wallAxis == "x") {
            var abuttingWallPositionXModule = abuttingWallClass.vertex1.x;
            var definableWallPositionXModule;
            if (abuttingWallPositionXModule != definableWallClass.vertex1.x) {
                definableWallPositionXModule = definableWallClass.vertex1.x;
            } else {
                definableWallPositionXModule = definableWallClass.vertex2.x;
            }
            if (abuttingWallPositionXModule <= definableWallPositionXModule) {
                return "left";
            } else return "right";
        } else if (definableWallClass.wallAxis == "z") {
            var abuttingWallPositionYModule = abuttingWallClass.vertex1.y;
            var definableWallPositionYModule;
            if (abuttingWallPositionYModule != definableWallClass.vertex1.y) {
                definableWallPositionYModule = definableWallClass.vertex1.y;
            } else {
                definableWallPositionYModule = definableWallClass.vertex2.y;
            }
            if (abuttingWallPositionYModule <= definableWallPositionYModule) {
                return "top";
            } else return "bottom";
        } else {
            return "";
        }
    }

    public allIntersectionsMoreOne(wallArray: THREE.Mesh[], walls) {
        for (var i = 0; i < wallArray.length; i++) {
            var wallIntersections = this.findIntersectedWalls(wallArray[i], walls);
            if (wallIntersections.length < 2) {
                return false;
            }
        }
        return true;
    }

    private static isNoRoomsWithSameWalls(wallArray: THREE.Mesh[]) {
        let rooms: Room[] = PlanViewContext.getInstance().getRooms();
        for (var i = 0; i < rooms.length; i++) {
            var room: Room = rooms[i];
            let intersection = room.walls.filter(x => wallArray.includes(x));
            if (intersection.length == room.walls.length) {
                return false;
            }
        }
        return true;
    }

    splitWall(crossingPoint: THREE.Vector2, wall1: THREE.Mesh, wall2: THREE.Mesh, wall1Class: Wall, wall2Class: Wall, walls, cubeMaterial, wallsToCreate, wallsToDelete) {
        if (this.crossingPointIsNotVertex(crossingPoint, wall1Class)) {

            var wall1Split1 = Drawings.drawWall("wall1Split1" + Math.floor(Math.random() * 100), walls, wall1Class.vertex1, cubeMaterial, wall1Class.wallAxis, wall1Class.orientation);
            var wall1Split2 = Drawings.drawWall("wall1Split2" + Math.floor(Math.random() * 100), walls, crossingPoint, cubeMaterial, wall1Class.wallAxis, wall1Class.orientation);

            wall1Split1.position.x = wall1Class.vertex1.x;
            wall1Split1.position.z = wall1Class.vertex1.y;

            wall1Split2.position.x = crossingPoint.x;
            wall1Split2.position.z = crossingPoint.y;

            if (wall1Class.wallAxis == "x") {
                wall1Split1.scale.x = crossingPoint.x - wall1Class.vertex1.x;
                wall1Split2.scale.x = wall1Class.vertex2.x - crossingPoint.x;

                wall1Split1.scale.z = PlanViewSettings.WALL_SIZE;
                wall1Split2.scale.z = PlanViewSettings.WALL_SIZE;
            } else {
                wall1Split1.scale.z = crossingPoint.y - wall1Class.vertex1.y;
                wall1Split2.scale.z = wall1Class.vertex2.y - crossingPoint.y;

                wall1Split1.scale.x = PlanViewSettings.WALL_SIZE;
                wall1Split2.scale.x = PlanViewSettings.WALL_SIZE;
            }

            var wall1ToDeleteClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall1);
            if (!walls.includes(wall1Split1)) {
                wallsToCreate.push(wall1Split1);
                this.addWall(new Wall(wall1Split1, wall1Class.vertex1, crossingPoint, wall1Class.wallAxis, wall1Class.orientation, [], wall1ToDeleteClass.mixedMaterials));
            }
            if (!walls.includes(wall1Split2)) {
                wallsToCreate.push(wall1Split2);
                this.addWall(new Wall(wall1Split2, crossingPoint, wall1Class.vertex2, wall1Class.wallAxis, wall1Class.orientation, [], wall1ToDeleteClass.mixedMaterials));
            }

            wallsToDelete.push(wall1);
        }

        if (this.crossingPointIsNotVertex(crossingPoint, wall2Class)) {

            var wall2Split1 = Drawings.drawWall("wall2Split1" + Math.floor(Math.random() * 100), walls, wall2Class.vertex1, cubeMaterial, wall2Class.wallAxis, wall2Class.orientation);
            var wall2Split2 = Drawings.drawWall("wall2Split2" + Math.floor(Math.random() * 100), walls, crossingPoint, cubeMaterial, wall2Class.wallAxis, wall2Class.orientation);

            wall2Split1.position.x = wall2Class.vertex1.x;
            wall2Split1.position.z = wall2Class.vertex1.y;

            wall2Split2.position.x = crossingPoint.x;
            wall2Split2.position.z = crossingPoint.y;

            if (wall2Class.wallAxis == "x") {
                wall2Split1.scale.x = crossingPoint.x - wall2Class.vertex1.x;
                wall2Split2.scale.x = wall2Class.vertex2.x - crossingPoint.x;

                wall2Split1.scale.z = PlanViewSettings.WALL_SIZE;
                wall2Split2.scale.z = PlanViewSettings.WALL_SIZE;
            } else {
                wall2Split1.scale.z = crossingPoint.y - wall2Class.vertex1.y;
                wall2Split2.scale.z = wall2Class.vertex2.y - crossingPoint.y;

                wall2Split1.scale.x = PlanViewSettings.WALL_SIZE;
                wall2Split2.scale.x = PlanViewSettings.WALL_SIZE;
            }

            wallsToCreate.push(wall2Split1);
            wallsToCreate.push(wall2Split2);
            wallsToDelete.push(wall2);

            var wall2ToDeleteClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall2);

            this.addWall(new Wall(wall2Split1, wall2Class.vertex1, crossingPoint, wall2Class.wallAxis, wall2Class.orientation, [], wall2ToDeleteClass.mixedMaterials));
            this.addWall(new Wall(wall2Split2, crossingPoint, wall2Class.vertex2, wall2Class.wallAxis, wall2Class.orientation, [], wall2ToDeleteClass.mixedMaterials));
        }
    }

    isWallsCross(wall1: THREE.Mesh, wall2: THREE.Mesh, walls, cubeMaterial, wallsToCreate, wallsToDelete) {
        var wall1Class = this.findWallClassByWall(wall1);
        var wall2Class = this.findWallClassByWall(wall2);


        var isCross = WallsPoints.intersect(wall1Class.vertex1, wall1Class.vertex2, wall2Class.vertex1, wall2Class.vertex2);
        if (isCross != false && isCross != true) {
            if (this.crossingPointIsNotVertex(isCross, wall1Class) ||
                this.crossingPointIsNotVertex(isCross, wall2Class)) {
                this.splitWall(isCross, wall1, wall2, wall1Class, wall2Class, walls, cubeMaterial, wallsToCreate, wallsToDelete);
            } else {

            }
        } else {
        }
    }

    isWallsOverlap(wall1: THREE.Mesh, wall2: THREE.Mesh, walls, cubeMaterial, wallsToCreate, wallsToDelete) {
        var wall1Class = this.findWallClassByWall(wall1);
        var wall2Class = this.findWallClassByWall(wall2);

        let isOverlap = this.isOverlap(wall1, wall2, wall1Class, wall2Class, walls, cubeMaterial, wallsToCreate, wallsToDelete);

    }

    isOverlap(wall1: THREE.Mesh, wall2: THREE.Mesh, wall1Class: Wall, wall2Class: Wall, walls, cubeMaterial, wallsToCreate, wallsToDelete) {

        var wallToDeleteClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall1);

        if (wall1Class.wallAxis != wall2Class.wallAxis) {
            return false;
        }
        var wallOverlap;

        if (WallsPoints.bothVericesAreEqual(wall2Class.vertex1, wall1Class.vertex1, wall2Class.vertex2, wall1Class.vertex2)) {
            wallOverlap = Drawings.drawWall("wallOverlap" + Math.floor(Math.random() * 100), walls, wall1Class.vertex1, cubeMaterial, wall1Class.wallAxis, wall1Class.orientation);

            wallOverlap.position.x = wall1.position.x;
            wallOverlap.position.z = wall1.position.z;

            wallOverlap.scale.x = wall1.scale.x;
            wallOverlap.scale.z = wall1.scale.z;

            wallsToCreate.push(wallOverlap);
            wallsToDelete.push(wall1);
            wallsToDelete.push(wall2);

            this.addWall(new Wall(wallOverlap, wall1Class.vertex1, wall1Class.vertex2, wall1Class.wallAxis, wall1Class.orientation, [], wallToDeleteClass.mixedMaterials));
            return true;
        }

        if (wall1Class.wallAxis == "x") {
            if (wall1Class.vertex1.y != wall2Class.vertex1.y) {
                return false;
            }

            var l1 = null;
            var r1 = null;
            var l2 = null;
            var r2 = null;

            if (wall1Class.vertex1.x > wall1Class.vertex2.x) {
                l1 = wall1Class.vertex2.x;
                r1 = wall1Class.vertex1.x;
            } else {
                l1 = wall1Class.vertex1.x;
                r1 = wall1Class.vertex2.x;
            }

            if (wall2Class.vertex1.x > wall2Class.vertex2.x) {
                l2 = wall2Class.vertex2.x;
                r2 = wall2Class.vertex1.x;
            } else {
                l2 = wall2Class.vertex1.x;
                r2 = wall2Class.vertex2.x;
            }

            if (l1 < l2) {
                if (r1 < l2) {
                    return false;
                } else {
                    wallOverlap = Drawings.drawWall("wallOverlap" + Math.floor(Math.random() * 100), walls, wall1Class.vertex1, cubeMaterial, "x", wall1Class.orientation);

                    wallOverlap.position.z = wall1.position.z;
                    wallOverlap.scale.z = PlanViewSettings.WALL_SIZE;

                    if (r1 < r2) {
                        wallOverlap.position.x = l1;
                        wallOverlap.scale.x = r2 - l1;
                    } else {
                        wallOverlap.position.x = l1;
                        wallOverlap.scale.x = r1 - l1;
                    }

                    wallsToCreate.push(wallOverlap);
                    wallsToDelete.push(wall1);
                    wallsToDelete.push(wall2);

                    this.addWall(new Wall(wallOverlap, new THREE.Vector2(wallOverlap.position.x, wallOverlap.position.z), new THREE.Vector2(wallOverlap.position.x + wallOverlap.scale.x, wallOverlap.position.z), "x", wall1Class.orientation, [], wallToDeleteClass.mixedMaterials));
                    return true;
                }
            } else {
                if (r2 < l1) {
                    return false;
                } else {

                    wallOverlap = Drawings.drawWall("wallOverlap" + Math.floor(Math.random() * 100), walls, wall1Class.vertex1, cubeMaterial, "x", wall1Class.orientation);

                    wallOverlap.position.z = wall1.position.z;
                    wallOverlap.scale.z = PlanViewSettings.WALL_SIZE;

                    if (r2 < r1) {
                        wallOverlap.position.x = l2;
                        wallOverlap.scale.x = r1 - l2;
                    } else {
                        wallOverlap.position.x = l2;
                        wallOverlap.scale.x = r2 - l2;
                    }

                    wallsToCreate.push(wallOverlap);
                    wallsToDelete.push(wall1);
                    wallsToDelete.push(wall2);

                    this.addWall(new Wall(wallOverlap, new THREE.Vector2(wallOverlap.position.x, wallOverlap.position.z), new THREE.Vector2(wallOverlap.position.x + wallOverlap.scale.x, wallOverlap.position.z), "x", wall1Class.orientation, [], wallToDeleteClass.mixedMaterials));
                    return true;

                }
            }
        }

        if (wall1Class.wallAxis == "z") {
            if (wall1Class.vertex1.x != wall2Class.vertex1.x) {
                return false;
            }

            var t1 = null;
            var b1 = null;
            var t2 = null;
            var b2 = null;

            if (wall1Class.vertex1.y > wall1Class.vertex2.y) {
                t1 = wall1Class.vertex2.y;
                b1 = wall1Class.vertex1.y;
            } else {
                t1 = wall1Class.vertex1.y;
                b1 = wall1Class.vertex2.y;
            }

            if (wall2Class.vertex1.y > wall2Class.vertex2.y) {
                t2 = wall2Class.vertex2.y;
                b2 = wall2Class.vertex1.y;
            } else {
                t2 = wall2Class.vertex1.y;
                b2 = wall2Class.vertex2.y;
            }

            if (t1 < t2) {
                if (b1 < t2) {
                    return false;
                } else {
                    wallOverlap = Drawings.drawWall("wallOverlap" + Math.floor(Math.random() * 100), walls, wall1Class.vertex1, cubeMaterial, "z", wall1Class.orientation);

                    wallOverlap.position.x = wall1.position.x;
                    wallOverlap.scale.x = PlanViewSettings.WALL_SIZE;

                    if (b2 < b1) {
                        wallOverlap.position.z = t1;
                        wallOverlap.scale.z = b1 - t1;
                    } else {
                        wallOverlap.position.z = t1;
                        wallOverlap.scale.z = b2 - t1;
                    }

                    wallsToCreate.push(wallOverlap);
                    wallsToDelete.push(wall1);
                    wallsToDelete.push(wall2);

                    this.addWall(new Wall(wallOverlap, new THREE.Vector2(wallOverlap.position.x, wallOverlap.position.z), new THREE.Vector2(wallOverlap.position.x, wallOverlap.position.z + wallOverlap.scale.z), "z", wall1Class.orientation, [], wallToDeleteClass.mixedMaterials));

                    return true;
                }
            } else {
                if (b2 < t1) {
                    return false;
                } else {
                    wallOverlap = Drawings.drawWall("wallOverlap" + Math.floor(Math.random() * 100), walls, wall1Class.vertex1, cubeMaterial, "z", wall1Class.orientation);

                    wallOverlap.position.x = wall1.position.x;
                    wallOverlap.scale.x = PlanViewSettings.WALL_SIZE;

                    if (b1 < b2) {
                        wallOverlap.position.z = t2;
                        wallOverlap.scale.z = b2 - t2;
                    } else {
                        wallOverlap.position.z = t2;
                        wallOverlap.scale.z = b1 - t2;
                    }

                    wallsToCreate.push(wallOverlap);
                    wallsToDelete.push(wall1);
                    wallsToDelete.push(wall2);

                    this.addWall(new Wall(wallOverlap, new THREE.Vector2(wallOverlap.position.x, wallOverlap.position.z), new THREE.Vector2(wallOverlap.position.x, wallOverlap.position.z + wallOverlap.scale.z), "z", wall1Class.orientation, [], wallToDeleteClass.mixedMaterials));

                    return true;
                }
            }
        }

        return true;
    }

    crossingPointIsNotVertex(crossingPoint, wallClass: Wall) {
        return !wallClass.vertex1.equals(crossingPoint) && !wallClass.vertex2.equals(crossingPoint);
    }

    findRoomByWallArray(wallArray: THREE.Mesh[]): Room {

        var rooms: Room[] = PlanViewContext.getInstance().getRooms();

        for (var i = 0; i < rooms.length; i++) {
            var room = rooms[i];
            if (WallsManager.roomContainsWallsInAnyOrder(room, wallArray)) {
                return room;
            }
        }
        return null;

    }

    private static roomContainsWallsInAnyOrder(room: Room, wallArray: THREE.Mesh[]) {
        var roomWalls: THREE.Mesh[] = room.walls;
        let intersection = roomWalls.filter(x => wallArray.includes(x));
        return intersection.length == roomWalls.length;

    }
}