import * as THREE from 'three';
import {WallsPoints} from "../wallsUtils/wallsPoints";

export class Wall{
    constructor(public wall:THREE.Mesh,
                public vertex1:THREE.Vector2,
                public vertex2:THREE.Vector2,
                public wallAxis:String,
                public orientation:string,
                public paintedWallsNumbers:number[],
                public mixedMaterials:THREE.MeshLambertMaterial[]) {

    }

    public addPaintedWallsNumber(number){
        if(!this.paintedWallsNumbers.includes(number)){
            this.paintedWallsNumbers.push(number);
        }
    }

    public wallLength():number{
        if(this.wallAxis == "x"){
            return WallsPoints.getNumberByModule(this.vertex1.x - this.vertex2.x) / 100;
        } else return WallsPoints.getNumberByModule(this.vertex1.y - this.vertex2.y) / 100;
    }
}