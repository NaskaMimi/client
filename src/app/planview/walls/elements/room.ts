import * as THREE from 'three';
import {Wall} from "./wall";
import {PlanViewContext} from "../../context/planViewContext";
import {WallsPoints} from "../wallsUtils/wallsPoints";

export class Room{
    constructor(public roomName:string,
                public walls:THREE.Mesh[],
                public floor:THREE.Mesh,
                public outsideTexture:string,
                public wallpapersTexture:string,
                public floorTexture:string,
                public paintedWallsMap:Map<Wall, number>) {

    }

    calculatePerimeter():number{
        var perimeter:number = 0;
        for(var i=0; i<this.walls.length; i++){
            var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(this.walls[i]);
            perimeter += wallClass.wallLength();
        }
        return perimeter;
    }

    calculateArea():number{
        var unicPoints = WallsPoints.collectUnicPoints(this.walls);
        return WallsPoints.getNumberByModule(THREE.ShapeUtils.area(unicPoints)) / 10000;
    }
}