import * as THREE from 'three';
import {Drawings} from "./wallsUtils/drawings";
import {Wall} from "./elements/wall";
import {Room} from "./elements/room";
import {PlanViewContext} from "../context/planViewContext";
import {PlanViewSettings} from "../configuration/planViewSettings";
import {PlanViewEnvironment} from "../context/planViewEnvironment";
import {ShowRoomInfo, ShowWallInfo} from "../store/actions/rightpanel.actions";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../store/state/app.state";
import {RoomDto} from "../dtos/room.dto";
import {WallDto} from "../dtos/wall.dto";
import {Observable} from "rxjs";
import {
    changeFloor,
    changeOutsideMaterial,
    changeRoomName,
    changeWallpapers
} from "../store/selectors/rightpanel.selectors";
import {Calculation} from "../utils/calculation";
import {WallsPoints} from "./wallsUtils/wallsPoints";
import {HintController} from "../context/hintController";
import {AbstractFacade} from "../abstractFacade";
import {Materials} from "./materials";

export class WallFacade extends AbstractFacade{

    raycaster: THREE.Raycaster;
    // walls;
    pointer;

    voxel;
    wall1;
    wall2;
    wall3;
    wall4;

    firstPoint;

    highlightedWall: THREE.Mesh = null;
    highlightedRoom: Room = null;
    selectedWall: THREE.Mesh = null;
    selectedRoom: Room = null;

    _store: Store<IAppState>;

    deletedRoomOutsideTexturesMap: Map<THREE.Vector2[], string>
    deletedRoomWallTexturesMap: Map<THREE.Vector2[], string>
    deletedRoomFloorTexturesMap: Map<THREE.Vector2[], string>
    isValid = false;

    constructor(raycaster2: THREE.Raycaster,
                pointer2,
                store: Store<IAppState>) {

        super();
        this._store = store;

        this.raycaster = raycaster2;
        // this.walls = PlanViewContext.getInstance().getWalls();
        this.pointer = pointer2;

        this.deletedRoomOutsideTexturesMap = new Map();
        this.deletedRoomWallTexturesMap = new Map();
        this.deletedRoomFloorTexturesMap = new Map();

        var wallpapersObserver: Observable<string> = this._store.pipe(select(changeWallpapers));
        wallpapersObserver.subscribe((wallpaper => {
            if (this.selectedRoom != null) {
                this.selectedRoom.wallpapersTexture = wallpaper;
                Drawings.paintWalls(this.selectedRoom.walls, this.selectedRoom.paintedWallsMap, wallpaper, this.selectedRoom.outsideTexture);
            }
        }));

        var outsideMaterialObserver: Observable<string> = this._store.pipe(select(changeOutsideMaterial));
        outsideMaterialObserver.subscribe((outsideMaterial => {
            if (outsideMaterial != null && outsideMaterial != "") {
                PlanViewContext.getInstance().outsideTexture = outsideMaterial;
                var rooms = PlanViewContext.getInstance().getRooms();
                for (var i = 0; i < rooms.length; i++) {
                    var room = rooms[i];
                    room.outsideTexture = outsideMaterial;
                    Drawings.paintWalls(room.walls, room.paintedWallsMap, room.wallpapersTexture, outsideMaterial);
                }
            }
        }));

        var floorObserver: Observable<string> = this._store.pipe(select(changeFloor));
        floorObserver.subscribe((floor => {
            if (this.selectedRoom != null) {
                this.selectedRoom.floorTexture = floor;
                this.selectedRoom.floor.material = Materials.createDoubleSideMeshMaterial(new THREE.TextureLoader(), 0xF2EEE8, floor);
            }
        }));

        var roomNameObserver: Observable<string> = this._store.pipe(select(changeRoomName));
        roomNameObserver.subscribe((roomName => {
            if (this.selectedRoom != null) {
                this.selectedRoom.roomName = roomName;
            }
        }));

    }

    public createSingleWall(intersect) {
        var material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().outsideTexture);
        this.voxel = Drawings.drawWall("wall" + Math.floor(Math.random() * 100), PlanViewContext.getInstance().getWalls(), intersect.point, material, "xz", "n");
        this.addWallToArrays(this.voxel, "xz");
        PlanViewContext.getInstance().wallsManager.addWall(new Wall(this.voxel, this.voxel.position, this.voxel.position, "xz", "n", [], null));
    }

    public createRoom(intersect) {
        this.firstPoint = new THREE.Vector2();
        this.firstPoint.x = this.pointer.x;
        this.firstPoint.y = this.pointer.y;

        var material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().outsideTexture);

        this.wall1 = Drawings.drawWall("wall1", PlanViewContext.getInstance().getWalls(), intersect.point, material, "x", "l");
        this.wall2 = Drawings.drawWall("wall2", PlanViewContext.getInstance().getWalls(), intersect.point, material, "z", "t");
        this.wall3 = Drawings.drawWall("wall3", PlanViewContext.getInstance().getWalls(), intersect.point, material, "x", "r");
        this.wall4 = Drawings.drawWall("wall4", PlanViewContext.getInstance().getWalls(), intersect.point, material, "z", "b");

        this.addWallToArrays(this.wall1, "x");
        this.addWallToArrays(this.wall2, "z");
        this.addWallToArrays(this.wall3, "x");
        this.addWallToArrays(this.wall4, "z");

        PlanViewContext.getInstance().wallsManager.addWall(new Wall(this.wall1, this.wall1.position, this.wall1.position, "x", "l", [], null));
        PlanViewContext.getInstance().wallsManager.addWall(new Wall(this.wall2, this.wall2.position, this.wall2.position, "z", "t", [], null));
        PlanViewContext.getInstance().wallsManager.addWall(new Wall(this.wall3, this.wall3.position, this.wall3.position, "x", "r", [], null));
        PlanViewContext.getInstance().wallsManager.addWall(new Wall(this.wall4, this.wall4.position, this.wall4.position, "z", "b", [], null));
    }

    public deleteSelectedWall() {
        var wallsToDelete: THREE.Mesh[] = [];
        wallsToDelete.push(this.selectedWall);
        this.deleteWallsIfNeed(wallsToDelete);
    }

    public deleteSelectedRoom() {
        this.deleteWallsIfNeed(this.selectedRoom.walls);
    }

    public endWallsDraw() {
        HintController.hideHint();
        if (this.voxel != null) {

            if (this.wallIsSuperSmall(this.voxel) || !this.isValid) {
                this.deleteWallFromAllArrays(this.voxel);
            } else {

                this.voxel = this.joinWalls(this.voxel);
                this.splitWalls(this.voxel);
                var repeatX:number;
                var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(this.voxel);
                if(wallClass.wallAxis == "x"){
                    repeatX = this.voxel.scale.x / 500;
                } else {
                    repeatX = this.voxel.scale.z / 500;
                }
                this.voxel.material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().outsideTexture, 0, repeatX, 1);
            }
        }
        if (this.wall1 != null) {

            if (this.wallIsSuperSmall(this.wall1) || !this.isValid) {
                this.deleteWallFromAllArrays(this.wall1);
                this.deleteWallFromAllArrays(this.wall2);
                this.deleteWallFromAllArrays(this.wall3);
                this.deleteWallFromAllArrays(this.wall4);
            } else {
                this.wall1 = this.joinWalls(this.wall1);
                this.wall2 = this.joinWalls(this.wall2);
                this.wall3 = this.joinWalls(this.wall3);
                this.wall4 = this.joinWalls(this.wall4);

                for (var w = 0; w < PlanViewContext.getInstance().getWalls().length; w++) {
                    this.splitWalls(PlanViewContext.getInstance().getWalls()[w]);
                }
            }
        }

        if (this.isValid) {
            for (var f = 0; f < PlanViewContext.getInstance().getWalls().length; f++) {
                var wallArray = [];
                PlanViewContext.getInstance().wallsManager.findClosedSpace(PlanViewContext.getInstance().getWalls()[f], wallArray, PlanViewContext.getInstance().getWalls()[f], PlanViewContext.getInstance().getWalls(), this.deletedRoomWallTexturesMap, this.deletedRoomFloorTexturesMap);
            }
        }

        this.voxel = null;
        this.wall1 = null;
        this.wall2 = null;
        this.wall3 = null;
        this.wall4 = null;

    }

    protected isObjectValid():boolean{
        if(this.voxel != null){
            var voxelClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(this.voxel);
            return this.isWallValid(voxelClass.wallAxis, this.voxel);
        } else {
            var wall1Class: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(this.wall1);
            var wall2Class: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(this.wall2);
            var wall3Class: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(this.wall3);
            var wall4Class: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(this.wall4);
            return this.isWallValid(wall1Class.wallAxis, this.wall1) && this.isWallValid(wall2Class.wallAxis, this.wall2) && this.isWallValid(wall3Class.wallAxis, this.wall3) && this.isWallValid(wall4Class.wallAxis, this.wall4);
        }
    }

    protected getObjects(){
        if(this.voxel != null){
            return [this.voxel];
        } else {
            return [this.wall1, this.wall2, this.wall3, this.wall4];
        }
    }


    private deleteWallFromAllArrays(wall) {
        PlanViewEnvironment.getInstance().scene.remove(wall);
        PlanViewContext.getInstance().deleteObject(wall);
        // this.walls.splice(this.walls.indexOf(wall), 1);
      PlanViewContext.getInstance().deleteWall(wall);
        PlanViewContext.getInstance().wallsManager.deleteWall(wall);
    }

    private wallIsSuperSmall(wall) {
        return wall.scale.x == PlanViewSettings.WALL_SIZE && wall.scale.z == PlanViewSettings.WALL_SIZE;
    }

    public highlightWall() {
        this.highlightedWall = Drawings.highlightWall(PlanViewContext.getInstance().getWalls(), this.raycaster);
    }

    public selectWall() {
        this.selectedWall = this.highlightedWall;

        if (this.highlightedWall != null) {
            Drawings.selectWall(this.highlightedWall);
            var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(this.highlightedWall);
            this._store.dispatch(new ShowWallInfo(new WallDto(wallClass.wallLength(), PlanViewSettings.WALL_HEIGHT)));
        } else {
            Drawings.unselectObjects();
            this._store.dispatch(new ShowWallInfo(null));
        }

    }

    public highlightRoom() {
        this.highlightedRoom = Drawings.highlightRoom(PlanViewContext.getInstance().getWalls(), this.raycaster);
    }

    public selectRoom() {
        this.selectedRoom = this.highlightedRoom;

        if (this.highlightedRoom != null) {
            Drawings.selectRoom(this.highlightedRoom);
            this._store.dispatch(new ShowRoomInfo(new RoomDto(
                this.highlightedRoom.roomName,
                this.highlightedRoom.calculateArea(), this.highlightedRoom.calculatePerimeter(),
                this.highlightedRoom.wallpapersTexture,
                this.highlightedRoom.floorTexture)));
        } else {
            Drawings.unselectObjects();
            this._store.dispatch(new ShowRoomInfo(null));
        }
    }

    public drawSingleWall() {
        this.validateObject();
        Drawings.redrawSingleWall(this.voxel);
    }

    public drawRoom() {
        this.validateObject();
        Drawings.redrawRoom(this.wall1, this.wall2, this.wall3, this.wall4);
    }

    protected getHintText(): string{
        if(this.voxel != null){
            return "Длина стены = " + this.voxel.scale.x / 100 + " м";
        } else {
            return "Площадь комнаты = " + (this.wall1.scale.x * this.wall2.scale.z) / 10000 + " м";
        }
    }

    private addWallToArrays(wall, axis) {
        PlanViewEnvironment.getInstance().scene.add(wall);
        PlanViewContext.getInstance().addObject(wall);
        // this.walls.push(wall);
      PlanViewContext.getInstance().addWall(wall);
    }

    private joinWalls(wall) {
        var wallsToCreate = [];
        var wallsToDelete = [];
        var material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().outsideTexture);

        //определяем, перекрещиваются ли стены
        for (var i = 0; i < PlanViewContext.getInstance().getWalls().length; i++) {
            if (PlanViewContext.getInstance().getWalls()[i] != wall) {
                PlanViewContext.getInstance().wallsManager.isWallsOverlap(wall, PlanViewContext.getInstance().getWalls()[i], PlanViewContext.getInstance().getWalls(), material, wallsToCreate, wallsToDelete,);

                if (wallsToCreate.length > 0 || wallsToDelete.length > 0) {
                    this.addWallsIfNeed(wallsToCreate);
                    this.deleteWallsIfNeed(wallsToDelete);

                    var joinWall = wallsToCreate[0];

                    return this.joinWalls(joinWall);
                }
            }
        }

        return wall;
    }

    private addWallsIfNeed(wallsToCreate: THREE.Mesh[]) {
        for (var g = 0; g < wallsToCreate.length; g++) {

            var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wallsToCreate[g]);
            this.addWallToArrays(wallsToCreate[g], wallClass.wallAxis);
        }
    }

    private deleteWallsIfNeed(wallsToDelete: THREE.Mesh[]) {
        var roomsToDelete: Room[] = [];

        for (var h = 0; h < wallsToDelete.length; h++) {

            let wallToDelete = wallsToDelete[h];

            //TODO: use deleteWallFromAllArrays
            PlanViewEnvironment.getInstance().scene.remove(wallToDelete);
            PlanViewContext.getInstance().deleteObject(wallToDelete);
          PlanViewContext.getInstance().getWalls().splice(PlanViewContext.getInstance().getWalls().indexOf(wallToDelete), 1);

            let rooms: Room[] = PlanViewContext.getInstance().getRooms();
            for (var i = 0; i < rooms.length; i++) {
                var room: Room = rooms[i];
                if (room.walls.includes(wallToDelete)) {
                    PlanViewEnvironment.getInstance().scene.remove(room.floor);
                    PlanViewContext.getInstance().deleteFloor(room.floor);
                    roomsToDelete.push(room);
                }
            }
        }

        for (var k = 0; k < roomsToDelete.length; k++) {

            let roomToDelete: Room = roomsToDelete[k];
            this.deleteRoom(roomToDelete);
        }
    }

    private deleteRoom(roomToDelete: Room) {

        for (var i = 0; i < roomToDelete.walls.length; i++) {
            var wall = roomToDelete.walls[i];
            var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall);
            wallClass.paintedWallsNumbers = [];
        }

        this.deletedRoomOutsideTexturesMap.set(roomToDelete.walls, roomToDelete.outsideTexture);
        this.deletedRoomWallTexturesMap.set(roomToDelete.walls, roomToDelete.wallpapersTexture);
        this.deletedRoomFloorTexturesMap.set(roomToDelete.walls, roomToDelete.floorTexture);
        PlanViewContext.getInstance().deleteRoom(roomToDelete);
    }

    private splitWalls(wall) {
        var wallsToCreate: THREE.Mesh[] = [];
        var wallsToDelete: THREE.Mesh[] = [];
        var material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().outsideTexture);

        //определяем, перекрещиваются ли стены
        for (var i = 0; i < PlanViewContext.getInstance().getWalls().length; i++) {
            if (PlanViewContext.getInstance().getWalls()[i] != wall) {
                PlanViewContext.getInstance().wallsManager.isWallsCross(wall, PlanViewContext.getInstance().getWalls()[i], PlanViewContext.getInstance().getWalls(), material, wallsToCreate, wallsToDelete);

                if (wallsToCreate.length > 0 && wallsToDelete.length > 0) {
                    this.addWallsIfNeed(wallsToCreate);
                    this.deleteWallsIfNeed(wallsToDelete);

                    if (wallsToDelete[0] == wall) {
                        var split1 = wallsToCreate[0];
                        var split2 = wallsToCreate[1];

                        this.splitWalls(split1);
                        this.splitWalls(split2);
                    } else {
                        this.splitWalls(wall);
                    }

                    break;
                }
            }
        }
    }

    showAndHideWallsAccordingToLevel() {
        this.showAllWallsOnAllLevels();
        this.hideWallsUpperCurrentLevel();
    }

    private showAllWallsOnAllLevels() {
        for (var level = 1; level < 4; level++) {
            var walls: THREE.Mesh[] = PlanViewContext.getInstance().getWallsByLevel(level);
            for (let wall of walls) {
                wall.visible = true;
            }

            var floors: THREE.Mesh[] = PlanViewContext.getInstance().getFloorsByLevel(level);
            for (let floor of floors) {
                floor.visible = true;
            }
        }
    }

    private hideWallsUpperCurrentLevel() {
        for (var level = PlanViewContext.getInstance().currentLevel + 1; level < 4; level++) {
            var walls: THREE.Mesh[] = PlanViewContext.getInstance().getWallsByLevel(level);
            for (let wall of walls) {
                wall.visible = false;
            }

            var floors: THREE.Mesh[] = PlanViewContext.getInstance().getFloorsByLevel(level);
            for (let floor of floors) {
                floor.visible = false;
            }
        }
    }

    private isWallValid(axis: String, wall: THREE.Mesh): boolean {
      var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall);
        let isNotIntersectWalls = !Calculation.isWallIntersectOtherWalls(wallClass.vertex1, wallClass.vertex2);
        HintController.defineWarnings(!isNotIntersectWalls, HintController.WALL_INTERSECT_OTHER_WALLS);

        let isNotIntersectObjects = !Calculation.isWallIntersectOtherObjects(wall, PlanViewContext.getInstance().getModels3D());
        HintController.defineWarnings(!isNotIntersectObjects, HintController.WALL_INTERSECT_OBJECT);

        if(PlanViewContext.getInstance().currentLevel == 1){
            let isOnPorches = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, PlanViewContext.getInstance().getPorches());
            HintController.defineWarnings(!isOnPorches, HintController.WALL_IS_OUT_OF_PORCH);
            return isOnPorches && isNotIntersectWalls && isNotIntersectObjects;
        } else {
            let isNotIntersectBalconies = !Calculation.isWallIntersectOtherObjects(wall, PlanViewContext.getInstance().getBalconies());
            HintController.defineWarnings(!isNotIntersectBalconies, HintController.WALL_INTERSECT_BALCONY);
            return isNotIntersectBalconies && isNotIntersectWalls && isNotIntersectObjects;
        }
    }
}
