import * as THREE from 'three';
import {PlanViewEnvironment} from "../context/planViewEnvironment";
import {PlanViewContext} from "../context/planViewContext";
import {Object3D} from "../objects3D/object3D";
import {Loader3DObjects} from "../objects3D/loader3DObjects";
import {WallsPoints} from "../walls/wallsUtils/wallsPoints";
import {Drawings} from "../walls/wallsUtils/drawings";
import {Calculation} from "../utils/calculation";
import {Wall} from "../walls/elements/wall";
import {AbstractFacade} from "../abstractFacade";
import {HintController} from "../context/hintController";

var loadingInProcess: boolean = false;
var railingsArray: THREE.Mesh[] = [];

export class RailingFacade extends AbstractFacade {

  drawRailing: boolean = false;
  railingToDraw: Object3D = null;
  startPoint:THREE.Vector2 = null;
  previousPoint = null;

  railingsArray: THREE.Mesh[];
  currentRailingsArray: THREE.Mesh[];
  previousRailingsCount = 0;
  isValid = false;
  objectPosition;
  axis;

  constructor() {
    super();
    this.railingsArray = [];
    this.currentRailingsArray = [];
  }

  public startDrawRailing(point) {
    this.drawRailing = true;
    this.startPoint = point;
  }

  public stopDrawRailing() {
    if (this.drawRailing) {
      this.drawRailing = false;
      Drawings.unmarkObjects();
      if (this.isValid) {
        railingsArray = [];
      } else {
        this.deleteAllCurrentRailings();
      }
      this.currentRailingsArray = [];
    }
  }

  public setRailingForDraw(railing: Object3D) {
    this.railingToDraw = railing;
  }

  public createRailing(railing: Object3D, point, rotation) {
    if (railing) {
      new Loader3DObjects(PlanViewEnvironment.getInstance().scene, railing).model3D.subscribe({
        next(model) {
          if (model) {
            model.position.x = point.x;
            model.position.z = point.y;
            model.position.y = PlanViewContext.getInstance().yPosition;
            model.rotation.y = rotation;

            railingsArray.push(model);
          }
        },
        complete() {
        }
      });
    } else {
      loadingInProcess = false;
    }
  }

  public dragRailing(intersect) {

    if (this.drawRailing) {
      this.axis = WallsPoints.isChangeHorizontal(this.startPoint, PlanViewEnvironment.getInstance().rollOverMesh.position) ? "x" : "z";
      var pointsArray = this.defineRailingPositions(this.axis);
      let currentRailingsCount = pointsArray.length;

      if (this.previousRailingsCount != currentRailingsCount) {

        this.validateObject();
        this.deleteAllCurrentRailings();
        this.currentRailingsArray = [];

        this.previousRailingsCount = currentRailingsCount;
        for (var point of pointsArray) {
          this.createRailing(this.railingToDraw, point, this.axis == "x" ? Math.PI / 2 : 0);
        }
      }
    }
  }

  protected getObjects() {
    return railingsArray;
  }

  protected isObjectValid(): boolean {
    return this.isRailingsValid(this.objectPosition, this.axis);
  }

  protected getHintText(): string {
    return "Длина перил = " + (railingsArray.length * 50) / 100 + " м";
  }

  private isRailingsValid(objectPosition, axis) {
    var pointsArray = this.defineRailingPositions(axis);

    if (axis == "x") {
      if (this.startPoint.x < PlanViewEnvironment.getInstance().rollOverMesh.position.x) {
        pointsArray.push(new THREE.Vector2(PlanViewEnvironment.getInstance().rollOverMesh.position.x, this.startPoint.z));
      }
    } else {
      if (this.startPoint.z < PlanViewEnvironment.getInstance().rollOverMesh.position.z) {
        pointsArray.push(new THREE.Vector2(this.startPoint.x, PlanViewEnvironment.getInstance().rollOverMesh.position.z));
      }
    }

    for (var railingPoint of pointsArray) {
      var isRailingValid;
      var objectStartPoint: THREE.Vector2 = new THREE.Vector2(this.startPoint.x, this.startPoint.z);
      var objectEndPoint: THREE.Vector2 = new THREE.Vector2(PlanViewEnvironment.getInstance().rollOverMesh.position.x, PlanViewEnvironment.getInstance().rollOverMesh.position.z);
      let isRailingIntersectWalls = Calculation.isWallIntersectOtherWalls(objectStartPoint, objectEndPoint);
      HintController.defineWarnings(isRailingIntersectWalls, HintController.RAILING_INTERSECT_WALL);

      if (PlanViewContext.getInstance().currentLevel == 1) {
        let isRailingIntersectPorches = Calculation.isPointIntersectBox(railingPoint, PlanViewContext.getInstance().getPorches());
        HintController.defineWarnings(!isRailingIntersectPorches, HintController.RAILING_IS_OUT_OF_PORCH);
        isRailingValid = isRailingIntersectPorches && !isRailingIntersectWalls;
      } else {
        let isRailingIntersectBalcony = Calculation.isPointIntersectBox(railingPoint, PlanViewContext.getInstance().getBalconies());
        HintController.defineWarnings(!isRailingIntersectBalcony, HintController.RAILING_IS_OUT_OF_BALCONY);
        isRailingValid = isRailingIntersectBalcony && !isRailingIntersectWalls;
      }
      if (!isRailingValid) {
        return false;
      }
    }
    return true;
  }

  private defineRailingPositions(axis) {
    var pointsArray: THREE.Vector2[] = [];

    var stepsCount: number;
    if (axis == "x") {
      stepsCount = (PlanViewEnvironment.getInstance().rollOverMesh.position.x - this.startPoint.x) / 50;
    } else {
      stepsCount = (PlanViewEnvironment.getInstance().rollOverMesh.position.z - this.startPoint.z) / 50;
    }

    for (var i = 0; i < WallsPoints.getNumberByModule(stepsCount); i++) {
      var point: THREE.Vector2;
      if (axis == "x") {
        if (this.startPoint.x < PlanViewEnvironment.getInstance().rollOverMesh.position.x) {
          point = new THREE.Vector2(this.startPoint.x + (i) * 50, this.startPoint.z);
        } else {
          point = new THREE.Vector2(this.startPoint.x - (i + 1) * 50, this.startPoint.z);
        }
      } else {
        if (this.startPoint.z < PlanViewEnvironment.getInstance().rollOverMesh.position.z) {
          point = new THREE.Vector2(this.startPoint.x, this.startPoint.z + (i) * 50);
        } else {
          point = new THREE.Vector2(this.startPoint.x, this.startPoint.z - (i + 1) * 50);
        }
      }
      pointsArray.push(point);
    }
    return pointsArray;
  }

  private deleteAllCurrentRailings() {
    for (var railing of railingsArray) {
      PlanViewEnvironment.getInstance().scene.remove(railing);
      PlanViewContext.getInstance().deleteModel3D(railing);
    }
  }


}
