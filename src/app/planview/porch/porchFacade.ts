import {Drawings} from "../walls/wallsUtils/drawings";
import {PlanViewContext} from "../context/planViewContext";
import * as THREE from 'three';
import {PlanViewSettings} from "../configuration/planViewSettings";
import {PlanViewEnvironment} from "../context/planViewEnvironment";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../store/state/app.state";
import {ShowPorchInfo} from "../store/actions/rightpanel.actions";
import {PorchDto} from "../dtos/porch.dto";
import {Observable} from "rxjs";
import {changeFoundationMaterial, changePorchFloor} from "../store/selectors/rightpanel.selectors";
import {Calculation} from "../utils/calculation";
import {AbstractFacade} from "../abstractFacade";
import {HintController} from "../context/hintController";
import {Materials} from "../walls/materials";

export class PorchFacade extends AbstractFacade {

    currentPorch: THREE.Mesh;
    highlightedPorch: THREE.Mesh;
    selectedPorch: THREE.Mesh;
    raycaster: THREE.Raycaster;
    selectedPorchTexture: string;
    porchTextures: Map<THREE.Mesh, string>;
    startPoint: THREE.Vector3;
    isValid: boolean = false;

    _store: Store<IAppState>;

    constructor(raycaster2: THREE.Raycaster, store: Store<IAppState>) {
        super();
        this.raycaster = raycaster2;
        this._store = store;
        this.porchTextures = new Map();

        var porchObserver: Observable<string> = this._store.pipe(select(changePorchFloor));
        porchObserver.subscribe((floor => {
            if (this.selectedPorch != null) {
                this.selectedPorchTexture = floor;
                this.selectedPorch.material = Materials.createMixedMaterial(floor);

                this.porchTextures.delete(this.selectedPorch);
                this.porchTextures.set(this.selectedPorch, this.selectedPorchTexture);
            }
        }));

        var foundationMaterialObserver: Observable<string> = this._store.pipe(select(changeFoundationMaterial));
        foundationMaterialObserver.subscribe((foundationMaterial => {
            if (foundationMaterial != null && foundationMaterial != "") {
                PlanViewContext.getInstance().foundationTexture = foundationMaterial;
                for (var porch of PlanViewContext.getInstance().getPorches()) {
                    porch.material = Materials.createMixedMaterial(this.porchTextures.get(porch));
                }
            }
        }));
    }

    public createPorch(intersect) {
        this.startPoint = PlanViewEnvironment.getInstance().rollOverMesh.position.clone();

        var mixedMaterials = Materials.createMixedMaterial(PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg");

        var porchGeometry = new THREE.BoxGeometry(PlanViewSettings.WALL_INITIAL_SIZE, PlanViewSettings.FOUNDATION_HEIGHT, PlanViewSettings.WALL_INITIAL_SIZE);
        porchGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.FOUNDATION_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);

        var porch = new THREE.Mesh(porchGeometry, mixedMaterials);
        porch.castShadow = true;
        porch.receiveShadow = true;
        porch.position.copy(intersect.point);
        porch.position.divideScalar(50).floor().multiplyScalar(50).addScalar(0);
        porch.position.y = -0.5;

        this.currentPorch = porch;
        PlanViewEnvironment.getInstance().scene.add(porch);
        PlanViewContext.getInstance().addPorch(porch);
        this.porchTextures.set(porch, PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg");

    }

    public drawPorch() {

        let rollOverMesh = PlanViewEnvironment.getInstance().rollOverMesh;
        this.currentPorch.scale.x = rollOverMesh.position.x - this.currentPorch.position.x;
        this.currentPorch.scale.z = rollOverMesh.position.z - this.currentPorch.position.z;

        this.validateObject();
    }

    public stopDrawPorch() {
        if (this.currentPorch != null) {
            if (this.isValid) {
                this.currentPorch.material = Materials.createMixedMaterial(PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg", this.currentPorch.scale.x / 500, this.currentPorch.scale.z / 500);
                Drawings.unmarkObjects();
            } else {
                this.selectedPorch = this.currentPorch;
                this.deleteSelectedPorch();
            }
            this.currentPorch = null;
        }
    }

    protected isObjectValid(): boolean {
        return this.isPorchValid();
    }

    protected getObjects() {
        return [this.currentPorch];
    }

    protected getHintText() {
        return "Площадь крыльца = " + (this.currentPorch.scale.x * this.currentPorch.scale.z) / 10000 + " м";
    }

    public highlightPorch() {
        this.highlightedPorch = Drawings.highlightObject(PlanViewContext.getInstance().getPorches(), this.raycaster);
    }

    public selectPorch() {
        this.selectedPorch = this.highlightedPorch;

        if (this.selectedPorch != null) {
            Drawings.selectObject(this.selectedPorch);
            this._store.dispatch(new ShowPorchInfo(new PorchDto(Calculation.calculateSquare(this.selectedPorch), Calculation.calculatePerimeter(this.selectedPorch), this.porchTextures.get(this.selectedPorch))));
        } else {
            Drawings.unselectObjects();
            this._store.dispatch(new ShowPorchInfo(null));
        }

    }

    public deleteSelectedPorch() {
        PlanViewEnvironment.getInstance().scene.remove(this.selectedPorch);
        PlanViewContext.getInstance().deletePorch(this.selectedPorch);

        this.highlightedPorch = null;
        this.selectedPorch = null;
        Drawings.unselectObjects();
        this._store.dispatch(new ShowPorchInfo(null));
    }

    private isPorchValid(): boolean {
        var porchesArray = PlanViewContext.getInstance().getPorches().map((x) => x);
        porchesArray.splice(porchesArray.indexOf(this.currentPorch), 1);
        let isPorchIntersectOtherPorches = !Calculation.isAtLeastOneVertexOfBoxIntersectOtherBox(this.currentPorch, porchesArray);
        HintController.defineWarnings(!isPorchIntersectOtherPorches, HintController.PORCH_INTERSECT_OTHER_PORCH);
        return isPorchIntersectOtherPorches;
    }
}
