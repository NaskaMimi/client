export class Model3DDto {
    private _rotation:number;

    constructor(public isDraggable:boolean,
                rotation:number) {
        this._rotation = rotation;
    }

    get rotation(): number {
        return this._rotation;
    }

    set rotation(value: number) {
        this._rotation = value;
    }
}