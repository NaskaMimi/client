export class RoofDto {

    private _rotation:number;

    constructor(rotation:number) {
        this._rotation = rotation;
    }

    get rotation(): number {
        return this._rotation;
    }

    set rotation(value: number) {
        this._rotation = value;
    }
}