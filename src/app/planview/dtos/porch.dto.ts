export class PorchDto {
    constructor(public commonSquare:number,
                public perimeter:number,
                public floorTexture:string) {
    }
}