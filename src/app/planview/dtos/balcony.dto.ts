export class BalconyDto {
    constructor(public commonSquare:number,
                public perimeter:number,
                public floorTexture:string) {
    }
}