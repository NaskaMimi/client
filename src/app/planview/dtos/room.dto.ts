export class RoomDto {
    constructor(public name:string,
                public commonSquare:number,
                public perimeter:number,
                public wallpapersTexture:string,
                public floorTexture:string) {
    }
}