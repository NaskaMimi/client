import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {PlanViewFacade} from "./planViewFacade";
import {PlanViewEnvironment} from "./context/planViewEnvironment";
import {PlanViewUIElements} from "./context/planViewUIElements";
import {IAppState} from "./store/state/app.state";
import {select, Store} from "@ngrx/store";
import {
    selectBalcony,
    selectObject,
    selectPorch,
    selectRoof,
    selectRoom,
    selectWall
} from "./store/selectors/rightpanel.selectors";
import {Observable} from "rxjs";
import {RoomDto} from "./dtos/room.dto";
import {WallDto} from "./dtos/wall.dto";
import {Model3DDto} from "./dtos/model3D.dto";
import {Router} from "@angular/router";
import {Objects} from "./objects3D/objects";
import {RoofDto} from "./dtos/roof.dto";
import {PlanViewRouter} from "./context/planViewRouter";
import {JsonParser} from "./saveload/json/jsonParser";
import {SaveloadComponent} from "./saveload/saveload.component";


@Component({
    selector: 'plan-view',
    templateUrl: './planview.component.html',
    styleUrls: ['./planview.component.css'],
    // directives: [SaveloadComponent],
    changeDetection: ChangeDetectionStrategy.Default
})
export class PlanviewComponent implements OnInit {

    @ViewChild(SaveloadComponent) saveloadComponent:SaveloadComponent;

    wallpapers = Objects.wallpapers;
    selectedWallpaper = this.wallpapers[0];

    selectedOutsideMaterial = this.wallpapers[0];

    floors = Objects.floors;
    selectedFloor = this.floors[0];

    highlightedRoom: RoomDto = null;
    highlightedWall: WallDto = null;
    highlightedObject: Model3DDto = null;
    highlightedBalcony: Object = null;
    highlightedPorch: Object = null;
    highlightedRoof: RoofDto = null;

    constructor(private _store: Store<IAppState>, private router: Router, private _planViewFacade: PlanViewFacade) {

        PlanViewRouter.getInstance().router = router;

        var highlightedRoomObserver: Observable<RoomDto> = this._store.pipe(select(selectRoom));
        highlightedRoomObserver.subscribe((room => {
            this.highlightedRoom = room;
            if (room != null) {
                this.selectedWallpaper = this.defineSelectedWallpapers(room.wallpapersTexture);
                this.selectedFloor = this.defineSelectedFloor(room.floorTexture);
            }
        }));

        var highlightedWallObserver: Observable<WallDto> = this._store.pipe(select(selectWall));
        highlightedWallObserver.subscribe((wall => {
            this.highlightedWall = wall;
        }));

        var highlightedObjectObserver: Observable<Model3DDto> = this._store.pipe(select(selectObject));
        highlightedObjectObserver.subscribe((object => {
            this.highlightedObject = object;
        }));

        var highlightedBalconyObserver: Observable<Object> = this._store.pipe(select(selectBalcony));
        highlightedBalconyObserver.subscribe((object => {
            this.highlightedBalcony = object;
        }));

        var highlightedPorchObserver: Observable<Object> = this._store.pipe(select(selectPorch));
        highlightedPorchObserver.subscribe((object => {
            this.highlightedPorch = object;
        }));

        var highlightedRoofObserver: Observable<RoofDto> = this._store.pipe(select(selectRoof));
        highlightedRoofObserver.subscribe((object => {
            this.highlightedRoof = object;
        }));
    }

    ngOnInit(): void {

        PlanViewUIElements.getInstance().view = document.getElementById("view");
        PlanViewUIElements.getInstance().content = document.getElementById("content");

        // document.addEventListener( 'keydown', this.onDocumentKeyDown );
        // document.addEventListener( 'keyup', this.onDocumentKeyUp );
        // document.addEventListener( 'keypress', this.onDocumentKeyPress );

        PlanViewEnvironment.getInstance().render();

        window.addEventListener('resize', onWindowResize);
    }

    onDocumentKeyDown( event ) {
        console.log("888");
        switch ( event.keyCode ) {

            case 16: {
                console.log("37");
                PlanViewEnvironment.getInstance().controls.enablePan = true;
                PlanViewEnvironment.getInstance().controls.enableRotate = false;
                PlanViewEnvironment.getInstance().render();
                break;
            }

            case 37: {
                console.log(this._planViewFacade);
                this._planViewFacade.rotateClockwise();
                break;
            }
            case 39: {
                console.log(this._planViewFacade);
                this._planViewFacade.rotateCounterClockwise();
                break;
            }

        }

    }

    onDocumentKeyUp( event ) {

        switch ( event.keyCode ) {

            case 16: {
                console.log("37");
                PlanViewEnvironment.getInstance().controls.enablePan = false;
                PlanViewEnvironment.getInstance().controls.enableRotate = true;
                PlanViewEnvironment.getInstance().render();
                break;
            }

        }

    }

    onPointerMove(event) {
        this._planViewFacade.onPointerMove(event);
        PlanViewUIElements.getInstance().content.focus();
    }

    onPointerDown(event) {
        // PlanViewUIElements.getInstance().content.focus();
        var callback = () => {
            if (this.highlightedWall != null) {
                PlanViewRouter.getInstance().navigateToWallInfo();
            } else if (this.highlightedRoom != null) {
                PlanViewRouter.getInstance().navigateToRoomInfo();
            } else if (this.highlightedObject != null) {
                PlanViewRouter.getInstance().navigateToObjectInfo();
            } else if (this.highlightedBalcony != null) {
                PlanViewRouter.getInstance().navigateToBalconyInfo();
            } else if (this.highlightedPorch != null) {
                PlanViewRouter.getInstance().navigateToPorchInfo();
            } else if (this.highlightedRoof != null) {
                PlanViewRouter.getInstance().navigateToRoofInfo();
            } else {
                PlanViewRouter.getInstance().navigateToMainInfo();
            }
        }
        if(this.detectLeftButton(event)) {
            this._planViewFacade.onPointerDown(event, callback);
        } else {
            this._planViewFacade.cancelAllDraggableObjects();
        }
    }

    onPointerUp() {

        this._planViewFacade.onPointerUp();
        // PlanViewUIElements.getInstance().content.focus();
    }

    detectLeftButton(evt) {
        evt = evt || window.event;
        if ("buttons" in evt) {
            return evt.buttons == 1;
        }
        var button = evt.which || evt.button;
        return button == 1;
    }

    viewModeOn() {
        this._planViewFacade.singleModeOn();
        this._planViewFacade.viewModeOn();

        this.undisableAllButtons();
        this.disableButton("viewMode");

        PlanViewRouter.getInstance().navigateToMainInfo();
    }

    buildModeOn() {
        this._planViewFacade.singleModeOn();
        this._planViewFacade.buildModeOn();

        this.undisableAllButtons();
        this.disableButton("buildingMode");

        PlanViewRouter.getInstance().navigateToBuilding();
    }

    objectsModeOn() {

        this._planViewFacade.objectsModeOn();

        this.undisableAllButtons();
        this.disableButton("objectsMode");

        PlanViewRouter.getInstance().navigateToObjectsBar();
    }

    private defineSelectedWallpapers(wallpapersTexture: string) {
        for (var i = 0; i < this.wallpapers.length; i++) {
            var wallpaper = this.wallpapers[i];
            if (wallpaper.link == wallpapersTexture) {
                return wallpaper;
            }
        }
        return undefined;
    }

    private defineSelectedFloor(floorTexture: string) {
        for (var i = 0; i < this.floors.length; i++) {
            var floor = this.floors[i];
            if (floor.link == floorTexture) {
                return floor;
            }
        }
        return undefined;
    }

    foundationModeOn() {
        this.undisableAllButtons();
        (<HTMLInputElement>document.getElementById("foundationMode")).disabled = true;

        PlanViewRouter.getInstance().navigateToFoundation();
    }

    porchModeOn() {

        this._planViewFacade.porchModeOn();

        this.undisableAllButtons();
        this.disableButton("porchMode");

        PlanViewRouter.getInstance().navigateToPorch();
    }

    balconiesModeOn() {

        this._planViewFacade.balconyModeOn();

        this.undisableAllButtons();
        this.disableButton("balconiesMode");

        PlanViewRouter.getInstance().navigateToBalconies();
    }

    columnsModeOn() {
        this._planViewFacade.objectsModeOn();

        this.undisableAllButtons();
        this.disableButton("columnsMode");

        PlanViewRouter.getInstance().navigateToColumns();
    }

    stairsModeOn() {
        this._planViewFacade.objectsModeOn();

        this.undisableAllButtons();
        this.disableButton("stairsMode");

        PlanViewRouter.getInstance().navigateToStairs();
    }

    windowsModeOn() {
        this._planViewFacade.objectsModeOn();

        this.undisableAllButtons();
        this.disableButton("windowsMode");

        PlanViewRouter.getInstance().navigateToWindows();
    }

    doorsModeOn() {
        this._planViewFacade.objectsModeOn();

        this.undisableAllButtons();
        this.disableButton("doorsMode");

        PlanViewRouter.getInstance().navigateToDoors();
    }

    roofModeOn() {
        this._planViewFacade.roofModeOn();

        this.undisableAllButtons();
        this.disableButton("roofMode");

        PlanViewRouter.getInstance().navigateToRoofs();
    }

    railingsModeOn() {
        this._planViewFacade.railingModeOn();

        this.undisableAllButtons();
        this.disableButton("railingsMode");

        PlanViewRouter.getInstance().navigateToRailings();
    }

    private undisableAllButtons() {
        this.enableButton("viewMode");
        this.enableButton("objectsMode");
        this.enableButton("buildingMode");
        this.enableButton("porchMode");
        this.enableButton("balconiesMode");
        this.enableButton("columnsMode");
        this.enableButton("stairsMode");
        this.enableButton("windowsMode");
        this.enableButton("doorsMode");
        this.enableButton("roofMode");
        this.enableButton("railingsMode");
    }

    private enableButton(elementId:string) {
        (<HTMLInputElement>document.getElementById(elementId)).disabled = false;
        (<HTMLInputElement>document.getElementById(elementId)).className = "nav-link px-2 text-black";
        (<HTMLInputElement>document.getElementById("i-" + elementId)).style.color = "black";
    }

    private disableButton(elementId:string) {
        (<HTMLInputElement>document.getElementById(elementId)).disabled = true;
        (<HTMLInputElement>document.getElementById(elementId)).className = "nav-link px-2 text-secondary";
        (<HTMLInputElement>document.getElementById("i-" + elementId)).style.color = "#6C757D";
    }

  openSaveModal() {
    // (<HTMLInputElement>document.getElementById("modalSheet")).style.visibility = "visible";
    //
    // (<HTMLInputElement>document.getElementById("type-project-name")).style.visibility = "hidden";
    // (<HTMLInputElement>document.getElementById("save-project")).style.visibility = "visible";
    // JsonParser.saveToJSON();
    this.saveloadComponent.openSaveModal();
  }

  openLoadModal() {
    // (<HTMLInputElement>document.getElementById("modalSheet")).style.visibility = "visible";
    // JsonParser.loadFromJSON();
    this.saveloadComponent.openLoadModal();
  }
}

function onWindowResize() {
    PlanViewEnvironment.getInstance().onWindowResize();
}
