import * as THREE from 'three';
import {ClosedSpaceService} from "../walls/wallsUtils/closedSpaceService";
import {Drawings} from "../walls/wallsUtils/drawings";
import {PlanViewEnvironment} from "../context/planViewEnvironment";
import {PlanViewContext} from "../context/planViewContext";
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import {changeFoundationMaterial, changeOutsideMaterial} from "../store/selectors/rightpanel.selectors";
import {IAppState} from "../store/state/app.state";

export class FoundationFacade {

    private foundation: THREE.Mesh;
    _store: Store<IAppState>;

    constructor(store: Store<IAppState>) {
        this.foundation = null;
        this._store = store;

        var foundationMaterialObserver:Observable<string> = this._store.pipe(select(changeFoundationMaterial));
        foundationMaterialObserver.subscribe((foundationMaterial => {
            console.log("foundationMaterial = " + foundationMaterial);
            if (foundationMaterial != null && foundationMaterial != "") {
                PlanViewContext.getInstance().foundationTexture = foundationMaterial;
                this.foundation.material = Drawings.createMeshMaterial(new THREE.TextureLoader(), 0xF2EEE8, foundationMaterial);
            }
        }));

    }
    
}