import * as THREE from 'three';

export class Roof{
    constructor(public roof:THREE.Mesh,
                public type:string) {
    }
}