import * as THREE from 'three';
import {PlanViewContext} from "../context/planViewContext";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../store/state/app.state";
import {Drawings} from "../walls/wallsUtils/drawings";
import {PlanViewEnvironment} from "../context/planViewEnvironment";
import {PlanViewSettings} from "../configuration/planViewSettings";
import {Object3D} from "../objects3D/object3D";
import {Loader3DObjects} from "../objects3D/loader3DObjects";
import {Observable} from "rxjs";
import {
  changeRoofMaterial,
  rotateObjectClockwise,
  rotateObjectCounterClockwise,
  selectRoofTransformation
} from "../store/selectors/rightpanel.selectors";
import {ShowRoofInfo} from "../store/actions/rightpanel.actions";
import {RoofDto} from "../dtos/roof.dto";
import {PlanViewRouter} from "../context/planViewRouter";
import {Roof} from "./roof";
import {Materials} from "../walls/materials";
import {AbstractFacade} from "../abstractFacade";
import {Calculation} from "../utils/calculation";
import {HintController} from "../context/hintController";

export class RoofFacade extends AbstractFacade {

  private static readonly CONE = "cone";
  private static readonly TRIANGLE = "triangle";

  _store: Store<IAppState>;
  roofElements: Roof[];
  draggableRoof = null;
  draggableRoofInfo = null;
  selectedRoof = null;
  highlightedRoof = null;
  raycaster: THREE.Raycaster;
  previousScaleX: number;
  previousScaleZ: number;
  previousPositionX: number;
  previousPositionZ: number;
  private scalingPositionX: string;
  private scalingPositionZ: string;
  private roofVisibility: boolean = true;

  constructor(raycaster: THREE.Raycaster, store: Store<IAppState>) {
    super();
    this._store = store;
    this.roofElements = [];
    this.raycaster = raycaster;

    var roofMaterialObserver: Observable<string> = this._store.pipe(select(changeRoofMaterial));
    roofMaterialObserver.subscribe((roofMaterial => {
      if (roofMaterial != null && roofMaterial != "") {
        PlanViewContext.getInstance().roofTexture = roofMaterial;
        this.applyMaterialToAllRoofElements();
      }
    }));

    var roofTransformationObserver: Observable<string> = this._store.pipe(select(selectRoofTransformation));
    roofTransformationObserver.subscribe((transformation => {
      if (this.selectedRoof != null) {
        if (transformation == "") {
          PlanViewEnvironment.getInstance().deleteTransformationFromObject()
        } else {
          PlanViewEnvironment.getInstance().applyTransformationToObject(this.selectedRoof);
          PlanViewEnvironment.getInstance().setTransformationMode(transformation);
        }
        PlanViewEnvironment.getInstance().render();
      }
    }));

    var rotateClockwiseObserver: Observable<number> = this._store.pipe(select(rotateObjectClockwise));
    rotateClockwiseObserver.subscribe((rotation => {
      if (this.selectedRoof != null) {
        this.selectedRoof.rotation.y = rotation;
        PlanViewEnvironment.getInstance().render();
        this._store.dispatch(new ShowRoofInfo(new RoofDto(this.selectedRoof.rotation.y)));
      }
    }));

    var rotateCounterClockwiseObserver: Observable<number> = this._store.pipe(select(rotateObjectCounterClockwise));
    rotateCounterClockwiseObserver.subscribe((rotation => {
      if (this.selectedRoof != null) {
        this.selectedRoof.rotation.y = rotation;
        PlanViewEnvironment.getInstance().render();
        this._store.dispatch(new ShowRoofInfo(new RoofDto(this.selectedRoof.rotation.y)));
      }
    }));
  }


  protected isObjectValid(): boolean {
    return this.isRoofValid();
  }

  protected getObjects() {
    return [this.draggableRoof];
  }

  protected getHintText() {
    return "Площадь крыши = " + (this.draggableRoof.scale.x * this.draggableRoof.scale.z) / 10000 + " м";
  }

  public scaleRoof() {
    if (this.selectedRoof) {
      if (PlanViewEnvironment.getInstance().isScaling) {

        if (this.previousScaleX) {
          if (this.selectedRoof.scale.x >= 0.25) {
            var delta = this.selectedRoof.scale.x * 200 - this.previousScaleX;
            if (this.scalingPositionX == "right") {
              this.selectedRoof.position.x = this.previousPositionX + delta / 2;
            } else if (this.scalingPositionX == "left") {
              this.selectedRoof.position.x = this.previousPositionX - delta / 2;
            }
          }
        }

        if (this.previousScaleZ) {
          if (this.selectedRoof.scale.z >= 0.25) {
            var delta = this.selectedRoof.scale.z * 200 - this.previousScaleZ;
            if (this.scalingPositionZ == "bottom") {
              this.selectedRoof.position.z = this.previousPositionZ + delta / 2;
            } else if (this.scalingPositionZ == "top") {
              this.selectedRoof.position.z = this.previousPositionZ - delta / 2;
            }
          }
        }
      }
    }
  }

  public createRoof(roof: string) {
    this.cancelDraggableObject();
    var roofMesh = null;
    if (roof == "roof1") {
      roofMesh = this.createConeRoof();
      PlanViewEnvironment.getInstance().scene.add(roofMesh);
      let roofClass: Roof = new Roof(roofMesh, RoofFacade.CONE);
      this.roofElements.push(roofClass);
      PlanViewContext.getInstance().addRoof(roofMesh);

      this.draggableRoof = roofMesh;
    }
    if (roof == "roof2") {
      this.loadRoof(new Object3D("roof1", 1, 1, 1, 0, 0, "Roof", 298, 498, 0, []));
    }
  }

  public dragRoof(intersect) {
    this.scaleRoof();
    if (this.draggableRoof != null) {
      this.validateObject();
      this.draggableRoof.position.copy(intersect.point).add(intersect.face.normal);
      this.draggableRoof.position.divideScalar(50).floor().multiplyScalar(50).addScalar(0);
      this.draggableRoof.position.y = PlanViewContext.getInstance().yPosition;
    }
  }

  public putRoof() {
    // this.updateRoofParameters();
    this.rememberScalingPosition();
    if (this.draggableRoof != null) {
      this.markRoofAsSelected(this.roofElements[this.roofElements.length - 1].roof);
      PlanViewRouter.getInstance().navigateToRoofInfo();
      this.draggableRoof = null;
    }
  }

  public cancelDraggableObject() {
    this.deleteObject(this.draggableRoof);
  }

  private deleteObject(objectToDelete) {
    if (objectToDelete) {
      PlanViewEnvironment.getInstance().scene.remove(objectToDelete);
      PlanViewContext.getInstance().deleteModel3D(objectToDelete);
      this.draggableRoof = null;
      this.draggableRoofInfo = null;
      HintController.deleteAllWarnings();
      HintController.setHintText("");
      PlanViewEnvironment.getInstance().render();
    }
  }

  private updateRoofParameters() {
    if (this.selectedRoof) {
      console.log("previous scale = " + this.selectedRoof.scale.x * 200);
      this.previousScaleX = this.selectedRoof.scale.x * 200;
      this.previousPositionX = this.selectedRoof.position.x + 0;
      this.scalingPositionX = null;

      this.previousScaleZ = this.selectedRoof.scale.z * 200;
      this.previousPositionZ = this.selectedRoof.position.z + 0;
      this.scalingPositionZ = null;
    }
  }

  public highlightRoof() {
    this.highlightedRoof = Drawings.highlightObject(PlanViewContext.getInstance().getRoofs(), this.raycaster);
  }

  public changeRoofVisibility(visibility: boolean) {
    this.roofVisibility = visibility;
    this.showAndHideRoofsAccordingToLevel();
  }

  public selectRoof() {
    if (this.highlightedRoof != null) {
      this.markRoofAsSelected(this.highlightedRoof);
    } else {
      this.unselectRoof();
    }
  }

  public unselectRoof() {
    Drawings.unselectObjects();
    this.selectedRoof = null;
    this._store.dispatch(new ShowRoofInfo(null));
    PlanViewEnvironment.getInstance().deleteTransformationFromObject();

  }

  public deleteSelectedRoof() {
    PlanViewEnvironment.getInstance().scene.remove(this.selectedRoof);
    this.roofElements.splice(this.roofElements.indexOf(this.selectedRoof), 1);
    PlanViewContext.getInstance().deleteRoof(this.selectedRoof);
    PlanViewEnvironment.getInstance().deleteTransformationFromObject();
  }

  public showAndHideRoofsAccordingToLevel() {
    if (this.roofVisibility) {
      this.showAllRoofsOnAllLevels();
      this.hideRoofsUpperCurrentLevel();
    } else {
      this.hideAllRoofsOnAllLevels();
    }
  }

  public hideAllRoofsOnAllLevels() {
    for (var level = 1; level < 4; level++) {
      var objects: THREE.Mesh[] = PlanViewContext.getInstance().getRoofByLevel(level);
      console.log("objects = " + objects);
      for (let object of objects) {
        object.visible = false;
      }
    }
  }

  private showAllRoofsOnAllLevels() {
    for (var level = 1; level < 4; level++) {
      var objects: THREE.Mesh[] = PlanViewContext.getInstance().getRoofByLevel(level);
      console.log("objects = " + objects);
      for (let object of objects) {
        object.visible = true;
      }
    }
  }

  private hideRoofsUpperCurrentLevel() {
    for (var level = PlanViewContext.getInstance().currentLevel + 1; level < 4; level++) {
      var objects: THREE.Mesh[] = PlanViewContext.getInstance().getRoofByLevel(level);
      for (let object of objects) {
        object.visible = false;
      }
    }
  }

  private loadRoof(roof: Object3D) {
    new Loader3DObjects(PlanViewEnvironment.getInstance().scene, roof).model3D.subscribe((roofMesh =>{

        roofMesh.children[0].material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewSettings.URL_PREFIX + "assets/roofs/roof1.jpg", -Math.PI / 2, 6, 3);
        roofMesh.children[1].material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().outsideTexture, -Math.PI / 2, 6, 3);

        PlanViewEnvironment.getInstance().scene.add(roofMesh);

        let roofClass: Roof = new Roof(roofMesh, RoofFacade.TRIANGLE);
        this.roofElements.push(roofClass);

        PlanViewContext.getInstance().addRoof(roofMesh);

        this.draggableRoof = roofMesh;
        this.draggableRoofInfo = roof;

        PlanViewContext.getInstance().models3DInformation.set(this.draggableRoof, this.draggableRoofInfo);

      }
    ));

  }

  private createConeRoof() {
    const geometry = new THREE.ConeGeometry(140, 100, 4, 1, false, Math.PI / 4);
    geometry.translate(0, 50, 0);
    const material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewSettings.URL_PREFIX + "assets/roofs/roof1.jpg");
    return new THREE.Mesh(geometry, material);
  }

  private markRoofAsSelected(roof) {
    Drawings.selectObject(roof);
    this.selectedRoof = roof;
    this._store.dispatch(new ShowRoofInfo(new RoofDto(roof.rotation.y)));
  }

  private applyMaterialToAllRoofElements() {
    for (var roof of this.roofElements) {
      this.applyMaterialToRoof(roof);
    }
    PlanViewEnvironment.getInstance().render();
  }

  private applyMaterialToRoof(roof: Roof) {
    if (roof.type == RoofFacade.CONE) {
      roof.roof.material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().roofTexture, 0, roof.roof.scale.x, roof.roof.scale.z);
    } else if (roof.type == RoofFacade.TRIANGLE) {
      roof.roof.children[0].material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().roofTexture, -Math.PI / 2, roof.roof.scale.x / 10, roof.roof.scale.z / 10);
    }
  }

  stopDrawRoof() {
    this.updateRoofParameters();
  }

  private rememberScalingPosition() {
    if (this.previousPositionX < PlanViewEnvironment.getInstance().rollOverMesh.position.x) {
      this.scalingPositionX = "right";
    } else {
      this.scalingPositionX = "left";
    }

    if (this.previousPositionZ < PlanViewEnvironment.getInstance().rollOverMesh.position.z) {
      this.scalingPositionZ = "bottom";
    } else {
      this.scalingPositionZ = "top";
    }
  }

  private isRoofValid() {
    let isRoofDoesntIntersectOtherObjects = !Calculation.isAtLeastOneVertexOf3dModelIntersectBox(this.draggableRoof, PlanViewContext.getInstance().getBalconies()) && !Calculation.isObjectIntersectOtherWalls(this.draggableRoof);
    HintController.defineWarnings(!isRoofDoesntIntersectOtherObjects, HintController.OBJECT_INTERSECT_OTHER_OBJECT);
    return isRoofDoesntIntersectOtherObjects;
  }
}
