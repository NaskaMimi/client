import {PlanViewContext} from "../context/planViewContext";
import * as THREE from 'three';
import {select, Store} from "@ngrx/store";
import {IAppState} from "../store/state/app.state";
import {Observable} from "rxjs";
import {changeFoundationMaterial} from "../store/selectors/rightpanel.selectors";
import {Calculation} from "../utils/calculation";
import {HintController} from "../context/hintController";
import {Materials} from "../walls/materials";
import {ObjectsFacade} from "../objects3D/objectsFacade";
import {Object3D} from "../objects3D/object3D";
import {Loader3DObjects} from "../objects3D/loader3DObjects";
import {PlanViewEnvironment} from "../context/planViewEnvironment";
import {Drawings} from "../walls/wallsUtils/drawings";

export class StairFacade extends ObjectsFacade {

    constructor(raycaster2: THREE.Raycaster, store: Store<IAppState>) {
        super(raycaster2, store);

        var foundationMaterialObserver: Observable<string> = this._store.pipe(select(changeFoundationMaterial));
        foundationMaterialObserver.subscribe((foundationMaterial => {
            console.log("cfm");
            if (foundationMaterial != null && foundationMaterial != "") {
                for (var mesh of PlanViewContext.getInstance().getModels3D()) {
                    var objectInfo = PlanViewContext.getInstance().models3DInformation.get(mesh);
                    if(objectInfo.type == "Stair") {
                        console.log("changeFoundationMaterial = " + foundationMaterial);
                        mesh.children[1].material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, foundationMaterial);
                    }
                }
                PlanViewEnvironment.getInstance().render();
            }
        }));
    }

    protected isObjectValid(): boolean {
        return this.isStairValid();
    }

    protected getHintText() {
        return "";
    }

    public createObjects(window: Object3D) {
        this.cancelDraggableObject();
        new Loader3DObjects(PlanViewEnvironment.getInstance().scene, window).model3D.subscribe((model =>{
                this.draggableObject = model;
                this.draggableObjectInfo = window;

                model.children[1].material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().foundationTexture);
                PlanViewContext.getInstance().models3DInformation.set(this.draggableObject, this.draggableObjectInfo);
            }
        ));
    }

    public dragObject(intersect) {
        if (this.draggableObject != null) {
            this.draggableObject.position.copy(intersect.point).add(intersect.face.normal);

            this.draggableObject.position.divideScalar(50).floor().multiplyScalar(50).addScalar(0);
            this.draggableObject.position.y = -0.5;
            this.validateObject();
        }
    }

    private isStairValid(): boolean {
        var modelsArray = PlanViewContext.getInstance().getModels3D().map((x) => x);
        modelsArray.splice(modelsArray.indexOf(this.draggableObject), 1);

        var isIntersectOtherObjects = Calculation.isAtLeastOneVertexOf3dModelIntersectBox(this.draggableObject, modelsArray);
        HintController.defineWarnings(isIntersectOtherObjects, HintController.OBJECT_INTERSECT_OTHER_OBJECT);

        let isObjectIntersectPorches = Calculation.isAtLeastOneVertexOf3dModelIntersectBox(this.draggableObject, PlanViewContext.getInstance().getPorches());
        HintController.defineWarnings(isObjectIntersectPorches, HintController.OBJECT_INTERSECT_OTHER_OBJECT);
        return !isObjectIntersectPorches && !isIntersectOtherObjects;
    }
}
