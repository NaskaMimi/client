import * as THREE from 'three';
import {Room} from "../walls/elements/room";
import {WallsManager} from "../walls/wallsManager";
import {PlanViewSettings} from "../configuration/planViewSettings";
import {Object3D} from "../objects3D/object3D";

export class PlanViewContext {

  private static instance: PlanViewContext;

  private _objects: Map<number, THREE.Mesh[]>;
  private _walls: Map<number, THREE.Mesh[]>;
  private _floors: Map<number, THREE.Mesh[]>;
  private _balconies: Map<number, THREE.Mesh[]>;
  private _roofs: Map<number, THREE.Mesh[]>;
  private _porches: THREE.Mesh[];
  private _stairs: THREE.Mesh[];
  private _foundation: THREE.Mesh;
  private _wallsManager: WallsManager;
  private _rooms: Map<number, Room[]>;
  private _models3D: Map<number, Object[]>;
  private _models3DInformation: Map<Object, Object3D>;
  private _outsideTexture: string;
  private _foundationTexture: string;
  private _roofTexture: string;

  private _yPosition: number;
  private _currentLevel: number;
  private _levelsCount: number;

  private constructor() {
    this._objects = new Map();
    this._objects.set(1, []);
    this._objects.set(2, []);
    this._objects.set(3, []);
    this._walls = new Map();
    this._walls.set(1, [])
    this._walls.set(2, [])
    this._walls.set(3, [])
    this._floors = new Map();
    this._floors.set(1, [])
    this._floors.set(2, [])
    this._floors.set(3, [])
    this._balconies = new Map();
    this._balconies.set(1, [])
    this._balconies.set(2, [])
    this._balconies.set(3, [])
    this._roofs = new Map();
    this._roofs.set(1, [])
    this._roofs.set(2, [])
    this._roofs.set(3, [])
    this._rooms = new Map();
    this._rooms.set(1, [])
    this._rooms.set(2, [])
    this._rooms.set(3, [])
    this._models3D = new Map();
    this._models3D.set(1, []);
    this._models3D.set(2, []);
    this._models3D.set(3, []);
    this._models3DInformation = new Map<Object, Object3D>();
    this._porches = [];
    this._stairs = [];
    this._wallsManager = new WallsManager();
    this._outsideTexture = PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper4.jpg";
    this._foundationTexture = PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper4.jpg";
    this._roofTexture = PlanViewSettings.URL_PREFIX + "assets/roofs/roof1.jpg";

    this._yPosition = PlanViewSettings.FOUNDATION_HEIGHT;
    this._currentLevel = 1;
    this._levelsCount = 1;
  }

  public static getInstance(): PlanViewContext {
    if (!PlanViewContext.instance) {
      PlanViewContext.instance = new PlanViewContext();
    }

    return PlanViewContext.instance;
  }


  get yPosition(): number {
    return this._yPosition;
  }

  set yPosition(value: number) {
    this._yPosition = value;
  }

  get currentLevel(): number {
    return this._currentLevel;
  }

  set currentLevel(value: number) {
    this._currentLevel = value;
  }

  get levelsCount(): number {
    return this._levelsCount;
  }

  set levelsCount(value: number) {
    this._levelsCount = value;
  }

  get foundationTexture(): string {
    return this._foundationTexture;
  }

  set foundationTexture(value: string) {
    this._foundationTexture = value;
  }

  get roofTexture(): string {
    return this._roofTexture;
  }

  set roofTexture(value: string) {
    this._roofTexture = value;
  }

  get outsideTexture(): string {
    return this._outsideTexture;
  }

  set outsideTexture(value: string) {
    this._outsideTexture = value;
  }

  get wallsManager(): WallsManager {
    return this._wallsManager;
  }

  set wallsManager(value: WallsManager) {
    this._wallsManager = value;
  }

  get foundation(): THREE.Mesh {
    return this._foundation;
  }

  set foundation(value: THREE.Mesh) {
    this._foundation = value;
  }

  public getObjects(): THREE.Mesh[] {
    return this._objects.get(this._currentLevel);
  }

  public addGround(value: THREE.Mesh, level: number) {
    this._objects.get(level).push(value);
  }

  public addObject(value: THREE.Mesh) {
    this._objects.get(this._currentLevel).push(value);
  }

  public deleteObject(value: THREE.Mesh) {
    this._objects.get(this._currentLevel).splice(this._objects.get(this._currentLevel).indexOf(value), 1);
  }

  public getWallsByLevel(level: number): THREE.Mesh[] {
    return this._walls.get(level);
  }

  public getWalls(): THREE.Mesh[] {
    return this._walls.get(this._currentLevel);
  }

  public addWall(value: THREE.Mesh) {
    this._walls.get(this._currentLevel).push(value);
  }

  public addWallToLevel(value: THREE.Mesh, level:number) {
    this._walls.get(level).push(value);
  }

  public deleteWall(value: THREE.Mesh) {
    this._walls.get(this._currentLevel).splice(this._walls.get(this._currentLevel).indexOf(value), 1);
  }

  public getFloorsByLevel(level: number): THREE.Mesh[] {
    return this._floors.get(level);
  }

  public getFloors(): THREE.Mesh[] {
    return this._floors.get(this._currentLevel);
  }

  public addFloor(value: THREE.Mesh) {
    this._floors.get(this._currentLevel).push(value);
  }

  public deleteFloor(value: THREE.Mesh) {
    this._floors.get(this._currentLevel).splice(this._floors.get(this._currentLevel).indexOf(value), 1);
  }

  public getBalconyByLevel(level: number): THREE.Mesh[] {
    return this._balconies.get(level);
  }

  public getBalconies(): THREE.Mesh[] {
    return this._balconies.get(this._currentLevel);
  }

  public addBalcony(value: THREE.Mesh) {
    this._balconies.get(this._currentLevel).push(value);
  }

  public deleteBalcony(value: THREE.Mesh) {
    this._balconies.get(this._currentLevel).splice(this._balconies.get(this._currentLevel).indexOf(value), 1);
  }

  public getRoofByLevel(level: number): THREE.Mesh[] {
    return this._roofs.get(level);
  }

  public getRoofs(): THREE.Mesh[] {
    return this._roofs.get(this._currentLevel);
  }

  public addRoof(value: THREE.Mesh) {
    this._roofs.get(this._currentLevel).push(value);
  }

  public deleteRoof(value: THREE.Mesh) {
    this._roofs.get(this._currentLevel).splice(this._roofs.get(this._currentLevel).indexOf(value), 1);
  }

  public getPorches(): THREE.Mesh[] {
    return this._porches;
  }

  public addPorch(value: THREE.Mesh) {
    this._porches.push(value);
  }

  public deletePorch(value: THREE.Mesh) {
    this._porches.splice(this._porches.indexOf(value), 1);
  }

  public getStairs(): THREE.Mesh[] {
    return this._stairs;
  }

  public addStair(value: THREE.Mesh) {
    this._stairs.push(value);
  }

  public deleteStair(value: THREE.Mesh) {
    this._stairs.splice(this._stairs.indexOf(value), 1);
  }

  public getRoomsByLevel(level: number): Room[] {
    return this._rooms.get(level);
  }

  public getRooms(): Room[] {
    return this._rooms.get(this._currentLevel);
  }

  public addRoom(value: Room) {
    this._rooms.get(this._currentLevel).push(value);
  }

  public deleteRoom(value: THREE.Mesh) {
    this._rooms.get(this._currentLevel).splice(this._rooms.get(this._currentLevel).indexOf(value), 1);
  }

  public getModels3DByLevel(level: number): THREE.Mesh[] {
    return this._models3D.get(level);
  }

  public getModels3D(): THREE.Mesh[] {
    return this._models3D.get(this._currentLevel);
  }

  public addModel3D(value: THREE.Mesh) {
    this._models3D.get(this._currentLevel).push(value);
  }

  public deleteModel3D(value: THREE.Mesh) {
    this._models3D.get(this._currentLevel).splice(this._models3D.get(this._currentLevel).indexOf(value), 1);
  }


  get models3DInformation(): Map<Object, Object3D> {
    return this._models3DInformation;
  }
}
