import {Materials} from "../walls/materials";
import {PlanViewContext} from "./planViewContext";
import * as THREE from 'three';
import {PlanViewUIElements} from "./planViewUIElements";
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
// import { TransformControls } from 'three/examples/jsm/controls/TransformControls.js';
import {TransformControls} from './TransformControls.js';
import {PlanViewSettings} from "../configuration/planViewSettings";
import {EffectComposer} from 'three/examples/jsm/postprocessing/EffectComposer.js';
import {RenderPass} from 'three/examples/jsm/postprocessing/RenderPass.js';
import {ShaderPass} from 'three/examples/jsm/postprocessing/ShaderPass.js';
import {OutlinePass} from 'three/examples/jsm/postprocessing/OutlinePass.js';
import {FXAAShader} from 'three/examples/jsm/shaders/FXAAShader.js';
import {Drawings} from "../walls/wallsUtils/drawings";

export class PlanViewEnvironment {

  private static instance: PlanViewEnvironment;

  private _controls;
  private _transformControls;
  private _camera;
  private _scene;
  private _renderer;
  private _rollOverMesh;

  private _composer;
  private _gridHelper;
  private _effectFXAA;
  private _outlinePass;
  private _selectionOutlinePass;
  private _enableOutlinePass;
  private _disableOutlinePass;
  private _plane0;
  private _plane1;
  private _plane2;
  private _plane3;

  private constructor() {
    this.createEnvironment();
  }

  public static getInstance(): PlanViewEnvironment {
    if (!PlanViewEnvironment.instance) {
      PlanViewEnvironment.instance = new PlanViewEnvironment();
    }

    return PlanViewEnvironment.instance;
  }

  public render() {
    if(this._renderer && this._scene && this._camera) {
      this._renderer.render(this._scene, this._camera);
      this._composer.render();
    }
  }

  get scene() {
    return this._scene;
  }

  get controls() {
    return this._controls;
  }

  get gridHelper() {
    return this._gridHelper;
  }

  get camera() {
    return this._camera;
  }

  get renderer() {
    return this._renderer;
  }

  get rollOverMesh() {
    return this._rollOverMesh;
  }

  get outlinePass() {
    return this._outlinePass;
  }

  get selectionOutlinePass() {
    return this._selectionOutlinePass;
  }

  get enableOutlinePass() {
    return this._enableOutlinePass;
  }

  get disableOutlinePass() {
    return this._disableOutlinePass;
  }

  getPlane() {
    if (PlanViewContext.getInstance().yPosition == PlanViewSettings.LEVEL0_POSITION) {
      return this._plane0;
    } else if (PlanViewContext.getInstance().yPosition == PlanViewSettings.LEVEL1_POSITION) {
      return this._plane1;
    } else if (PlanViewContext.getInstance().yPosition == PlanViewSettings.LEVEL2_POSITION) {
      return this._plane2;
    } else {
      return this._plane3;
    }
  }

  public setTransformationMode(mode) {
    this._transformControls.setMode(mode);
    this._transformControls.showY = mode == "scale";
  }

  public onWindowResize() {

    let view = PlanViewUIElements.getInstance().view;

    let contentBlock = document.getElementById("content-block");
    contentBlock.style.width = "100%";
    contentBlock.style.height = "100%";

    let innerWidth = view.getBoundingClientRect().width * 0.8;
    let innerHeight = view.getBoundingClientRect().height;
    this._camera.aspect = innerWidth / innerHeight;
    this._camera.updateProjectionMatrix();

    this._renderer.setSize(innerWidth, innerHeight);
    this._composer.setSize(innerWidth, innerHeight);

    let rightPanel = document.getElementById("div2");
    rightPanel.style.width = String(view.getBoundingClientRect().width * 0.2);
    rightPanel.style.height = "-webkit-fill-available";

  }

  public applyTransformationToObject(mesh) {
    this._transformControls.attach(mesh);
  }

  public deleteTransformationFromObject() {
    this._transformControls.detach();
  }

  public isScaling(): boolean {
    return this._transformControls.enabled && this._transformControls.mode == 'scale';
  }

  private createEnvironment() {
    this.createCamera();
    this.createScene();
    this.createGrid();
    this.createFakeGrounds();
    this.createLight();
    this.createRenderer();
    this.createComposer();
    this.createControls();
    this.createAppender();
    this.createGround();
  }

  private createGround() {
    this.createGroundForLevel(1, true);
  }

  private createGroundForLevel(level: number, visibility: boolean) {
    const geometry = new THREE.PlaneGeometry(10000, 10000);
    geometry.rotateX(-Math.PI / 2);

    var ground_material: THREE.MeshPhongMaterial = Materials.createMeshMaterial(new THREE.TextureLoader(), 0x5EC144, 'assets/ground/ground2.jpg', 0, 50, 50);

    var plane = new THREE.Mesh(geometry, ground_material);
    if (visibility) {
      plane.castShadow = true;
      plane.receiveShadow = true;
      plane.position.y = -1;
      this._scene.add(plane);
    } else {
      plane.visible = false;
    }

    PlanViewContext.getInstance().addGround(plane, level);
  }

  private createFakeGrounds() {
    this._plane0 = this.createFakeGround(PlanViewSettings.LEVEL0_POSITION);
    this._plane1 = this.createFakeGround(PlanViewSettings.LEVEL1_POSITION);
    this._plane2 = this.createFakeGround(PlanViewSettings.LEVEL2_POSITION);
    this._plane3 = this.createFakeGround(PlanViewSettings.LEVEL3_POSITION);
  }

  private createFakeGround(yPosition: number) {
    const geometry = new THREE.PlaneGeometry(10000, 10000);
    geometry.rotateX(-Math.PI / 2);

    var plane = new THREE.Mesh(geometry, Materials.cubeMaterial);
    plane.position.y = yPosition;
    plane.visible = false;
    this._scene.add(plane);
    return plane;
  }

  private createScene() {
    this._scene = new THREE.Scene();
    this._scene.background = new THREE.Color(0xADDCF0);
    this._scene.fog = new THREE.FogExp2(0xADDCF0, 0.00025);

  }

  private createCamera() {
    let view = PlanViewUIElements.getInstance().view;

    let width = view ? view.getBoundingClientRect().width : 1000;
    let height = view ? view.getBoundingClientRect().height : 1000;

    this._camera = new THREE.PerspectiveCamera(45, width / height, 1, 10000);
    this._camera.position.set(500, 800, 1300);
    this._camera.lookAt(0, 0, 0);
  }

  private createGrid() {
    this._gridHelper = new THREE.GridHelper(10000, 200, 0xffffff, 0xffffff);
    this._gridHelper.position.y = PlanViewSettings.FOUNDATION_HEIGHT;
    this._scene.add(this._gridHelper);
  }

  private createLight() {
    const ambientLight = new THREE.AmbientLight(0x606060);
    this._scene.add(ambientLight);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
    directionalLight.position.set(500, 500, 500);
    directionalLight.target.position.set(1, 0.75, 0.5);
    directionalLight.castShadow = true;
    this._scene.add(directionalLight);

    //Set up shadow properties for the light
    directionalLight.shadow.mapSize.width = 512; // default
    directionalLight.shadow.mapSize.height = 512; // default
    directionalLight.shadow.camera.near = 0.5; // default
    directionalLight.shadow.camera.far = 500; // default

    const helper = new THREE.DirectionalLightHelper( directionalLight, 5 );
    this._scene.add( helper );

  }

  private createRenderer() {
    let content = PlanViewUIElements.getInstance().content;

    if (content) {
      this._renderer = new THREE.WebGLRenderer({antialias: true});
      this._renderer.setPixelRatio(content.devicePixelRatio);
      this._renderer.setSize(content.getBoundingClientRect().width, content.getBoundingClientRect().height);

      this._renderer.shadowMap.enabled = true;
      this._renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
      this._renderer.shadowMapSoft = true;

      this._renderer.shadowCameraNear = 3;
      this._renderer.shadowCameraFar = this._camera.far;
      this._renderer.shadowCameraFov = 50;

      this._renderer.shadowMapBias = 0.0039;
      this._renderer.shadowMapDarkness = 0.5;
      this._renderer.shadowMapWidth = 1024;
      this._renderer.shadowMapHeight = 1024;

      document.getElementById("content").appendChild(this._renderer.domElement);
    }
  }

  private createControls() {
    if(this._camera && this._renderer) {
      this._controls = new OrbitControls(this._camera, this._renderer.domElement);
      this._controls.target.set(0, 5, 0);
      this._controls.enablePan = false;
      this._controls.mouseButtons = {
        LEFT: "",
        MIDDLE: THREE.MOUSE.MIDDLE,
        RIGHT: THREE.MOUSE.LEFT
      };

      this._controls.update();

      this._transformControls = new TransformControls(this._camera, this._renderer.domElement);
      this._transformControls.scaleSnap = 0.25;
      this._transformControls.setMode('scale');
      this._transformControls.setTranslationSnap(50);

      this._transformControls.addEventListener('dragging-changed', function (event) {
        PlanViewEnvironment.getInstance().controls.enabled = !event.value;
      });
      this._scene.add(this._transformControls);
    }
  }

  private createAppender() {
    const rollOverGeo = new THREE.BoxGeometry(PlanViewSettings.APPENDER_SIZE, PlanViewSettings.WALL_HEIGHT, PlanViewSettings.APPENDER_SIZE);
    rollOverGeo.translate(0, PlanViewSettings.WALL_HEIGHT / 2, 0);
    this._rollOverMesh = new THREE.Mesh(rollOverGeo, Materials.rollOverMaterial);
    this._scene.add(this._rollOverMesh);

    this._rollOverMesh.visible = false;
  }

  private createComposer() {
    if (this._renderer) {
      this._composer = new EffectComposer(this._renderer);

      const renderPass = new RenderPass(this._scene, this._camera);
      this._composer.addPass(renderPass);

      this._outlinePass = new OutlinePass(new THREE.Vector2(window.innerWidth, window.innerHeight), this._scene, this._camera);
      this._composer.addPass(this._outlinePass);

      this._selectionOutlinePass = new OutlinePass(new THREE.Vector2(window.innerWidth, window.innerHeight), this._scene, this._camera);
      this._selectionOutlinePass.visibleEdgeColor.set('#0363E2');
      this._composer.addPass(this._selectionOutlinePass);

      this._enableOutlinePass = new OutlinePass(new THREE.Vector2(window.innerWidth, window.innerHeight), this._scene, this._camera);
      this._enableOutlinePass.visibleEdgeColor.set('#05E203');
      this._composer.addPass(this._enableOutlinePass);

      this._disableOutlinePass = new OutlinePass(new THREE.Vector2(window.innerWidth, window.innerHeight), this._scene, this._camera);
      this._disableOutlinePass.visibleEdgeColor.set('#E20D03');
      this._composer.addPass(this._disableOutlinePass);

      var effectFXAA = new ShaderPass(FXAAShader);
      effectFXAA.uniforms['resolution'].value.set(1 / window.innerWidth, 1 / window.innerHeight);
      this._composer.addPass(effectFXAA);
      this.onWindowResize();
    }
  }
}
