export class HintController{

    public static readonly WALL_INTERSECT_OTHER_WALLS = "Стена не должна пересекать другие стены";
    public static readonly WALL_IS_OUT_OF_PORCH = "Стена не должна выходить за пределы крыльца";
    public static readonly WALL_INTERSECT_BALCONY = "Стена не должна пересекать балконы";
    public static readonly WALL_INTERSECT_OBJECT = "Стена не должна пересекать другие объекты";

    public static readonly RAILING_IS_OUT_OF_PORCH = "Перила не должны выходить за пределы крыльца";
    public static readonly RAILING_IS_OUT_OF_BALCONY = "Перила не должны выходить за пределы балкона";
    public static readonly RAILING_INTERSECT_WALL = "Перила не должны пересекать стены";

    public static readonly PORCH_INTERSECT_OTHER_PORCH = "Крыльцо не должно пересекать другие объекты";

    public static readonly BALCONY_INTERSECT_OTHER_BALCONY = "Балкон не должен пересекать другие балконы";
    public static readonly BALCONY_INTERSECT_WALL = "Балкон не должен пересекать стены";

    public static readonly OBJECT_INTERSECT_INNER_WALL = "Объект не может располагаться внутри дома";
    public static readonly OBJECT_DOESNT_INTERSECT_WALL = "Объект должен располагаться на стене";
    public static readonly OBJECT_INTERSECT_OTHER_OBJECT = "Объект не должен пересекать другие объекты";
    public static readonly OBJECT_IS_OUT_OF_HOUSE = "Объект не должен выходить за пределы дома";

    private static warnings:string[] = [];

    public static showHint(){
        (<HTMLInputElement>document.getElementById("hint")).hidden = false;
    }

    public static hideHint(){
        (<HTMLInputElement>document.getElementById("hint")).hidden = true;
    }

    public static setHintText(text){
        if(HintController.warnings.length == 0) {
            (<HTMLInputElement>document.getElementById("hint-text")).innerText = text;
            (<HTMLInputElement>document.getElementById("hint-text")).style.color = "white";
        } else {
            (<HTMLInputElement>document.getElementById("hint-text")).innerText = HintController.warnings[0];
            (<HTMLInputElement>document.getElementById("hint-text")).style.color = "red";
        }
    }

    public static defineWarnings(isInvalid:boolean, text:string){
        if(isInvalid){
            HintController.addWarning(text);
        } else {
            HintController.deleteWarning(text);
        }
    }

    public static deleteAllWarnings(){
        HintController.warnings = [];
    }

    private static addWarning(text){
        let index = HintController.warnings.indexOf(text);
        if(index < 0) {
            HintController.warnings.push(text);
        }
    }

    private static deleteWarning(text){
        let index = HintController.warnings.indexOf(text);
        if(index >= 0) {
            HintController.warnings.splice(index, 1);
        }
    }
}