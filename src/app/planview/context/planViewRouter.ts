import {Router} from "@angular/router";

export class PlanViewRouter {

    private static instance: PlanViewRouter;

    private _router: Router;
    private _currentURL: string;
    private _previousURL: string;

    private constructor() {

    }

    public static getInstance(): PlanViewRouter {
        if (!PlanViewRouter.instance) {
            PlanViewRouter.instance = new PlanViewRouter();
        }

        return PlanViewRouter.instance;
    }

    public set router(value: Router) {
        this._router = value;
    }

    public back(){
        this._router.navigate([this._previousURL]);
    }

    public navigateToWallTextures(){
        this.rememberUrl('/planview/textures');
        this._router.navigate(['/planview/textures']);
    }

    public navigateToFloorTextures(){
        this.rememberUrl('/planview/floor-textures');
        this._router.navigate(['/planview/floor-textures']);
    }

    public navigateToBuilding(){
        this.rememberUrl('/planview/building');
        this._router.navigate(['/planview/building']);
    }

    public navigateToObjectsBar(){
        this.rememberUrl('/planview/objectsbar');
        this._router.navigate(['/planview/objectsbar']);
    }

    public navigateToPorch(){
        this.rememberUrl('/planview/porch');
        this._router.navigate(['/planview/porch']);
    }

    public navigateToBalconies(){
        this.rememberUrl('/planview/balconies');
        this._router.navigate(['/planview/balconies']);
    }

    public navigateToColumns(){
        this.rememberUrl('/planview/columns');
        this._router.navigate(['/planview/columns']);
    }

    public navigateToStairs(){
        this.rememberUrl('/planview/stairs');
        this._router.navigate(['/planview/stairs']);
    }

    public navigateToWindows(){
        this.rememberUrl('/planview/windows');
        this._router.navigate(['/planview/windows']);
    }

    public navigateToDoors(){
        this.rememberUrl('/planview/doors');
        this._router.navigate(['/planview/doors']);
    }

    public navigateToRoofs(){
        this.rememberUrl('/planview/roof');
        this._router.navigate(['/planview/roof']);
    }

    public navigateToRailings(){
        this.rememberUrl('/planview/railings');
        this._router.navigate(['/planview/railings']);
    }

    public navigateToFoundation(){
        this.rememberUrl('/planview/foundation');
        this._router.navigate(['/planview/foundation']);
    }

    public navigateToWallInfo(){
        this.rememberUrl('/planview/wallinfo');
        this._router.navigate(['/planview/wallinfo']);
    }

    public navigateToRoomInfo(){
        this.rememberUrl('/planview/roominfo');
        this._router.navigate(['/planview/roominfo']);
    }

    public navigateToObjectInfo(){
        this.rememberUrl('/planview/objectinfo');
        this._router.navigate(['/planview/objectinfo']);
    }

    public navigateToBalconyInfo(){
        this.rememberUrl('/planview/balconyinfo');
        this._router.navigate(['/planview/balconyinfo']);
    }

    public navigateToPorchInfo(){
        this.rememberUrl('/planview/porchinfo');
        this._router.navigate(['/planview/porchinfo']);
    }

    public navigateToMainInfo(){
        this.rememberUrl('/planview/maininfo');
        this._router.navigate(['/planview/maininfo']);
    }

    public navigateToRoofInfo(){
        this.rememberUrl('/planview/roofinfo');
        this._router.navigate(['/planview/roofinfo']);
    }

    private rememberUrl(url: string){
        this._previousURL = "" + this._currentURL;
        this._currentURL = url;
    }
}