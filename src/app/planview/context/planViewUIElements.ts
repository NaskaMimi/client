export class PlanViewUIElements {

    private static instance: PlanViewUIElements;

    private _view;
    private _content;

    private constructor() {

    }

    public static getInstance(): PlanViewUIElements {
        if (!PlanViewUIElements.instance) {
            PlanViewUIElements.instance = new PlanViewUIElements();
        }

        return PlanViewUIElements.instance;
    }


    get view() {
        return this._view;
    }

    set view(value) {
        this._view = value;
    }

    get content() {
        return this._content;
    }

    set content(value) {
        this._content = value;
    }
}