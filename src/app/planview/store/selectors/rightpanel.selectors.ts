import {createSelector} from "@ngrx/store";
import {IRightpanelState} from "../state/rightpanel.state";
import {IAppState} from "../state/app.state";

const selectRightpanel = (state: IAppState) => state.rightpanel;

export const selectRoom = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.selectedRoom
)

export const selectWall = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.selectedWall
)

export const selectObject = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.selectedObject
)

export const selectRoof = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.selectedRoof
)

export const selectBalcony = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.selectedBalcony
)

export const selectPorch = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.selectedPorch
)

export const changeOutsideMaterial = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.outsideMaterial
)

export const changeRoofMaterial = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.roofMaterial
)

export const changeFoundationMaterial = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.foundationMaterial
)

export const changeWallpapers = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.wallpapers
)

export const changeFloor = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.floor
)

export const changeBalconyFloor = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.balconyFloor
)

export const changePorchFloor = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.porchFloor
)

export const changeRoomName = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.roomName
)

export const moveObject = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.isDraggable
)

export const rotateObjectClockwise = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.rotation
)

export const rotateObjectCounterClockwise = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.rotation
)

export const selectRoofTransformation = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.roofTransformation
)

export const selectWallTextureType = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.wallTextureType
)

export const selectFloorTextureType = createSelector(
    selectRightpanel,
    (state: IRightpanelState) => state.floorTextureType
)