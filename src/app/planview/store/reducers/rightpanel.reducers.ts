import {initialRightpanelState, IRightpanelState} from "../state/rightpanel.state";
import {ChangeRoofMaterial, ERightpanelActions, RightpanelActions} from "../actions/rightpanel.actions";

export const rightpanelReducers = (
    state = initialRightpanelState,
    action: RightpanelActions
): IRightpanelState => {
    switch (action.type) {
        case ERightpanelActions.ShowRoomInfo: {
            return {
                ...state,
                selectedRoom: action.payload
            };
        }

        case ERightpanelActions.ShowWallInfo: {
            return {
                ...state,
                selectedWall: action.payload
            };
        }

        case ERightpanelActions.ShowObjectInfo: {
            return {
                ...state,
                selectedObject: action.payload
            };
        }

        case ERightpanelActions.ShowRoofInfo: {
            return {
                ...state,
                selectedRoof: action.payload
            };
        }

        case ERightpanelActions.ShowBalconyInfo: {
            return {
                ...state,
                selectedBalcony: action.payload
            };
        }

        case ERightpanelActions.ShowPorchInfo: {
            return {
                ...state,
                selectedPorch: action.payload
            };
        }

        case ERightpanelActions.ChangeRoofMaterial: {
            return {
                ...state,
                roofMaterial: action.payload
            };
        }

        case ERightpanelActions.ChangeOutsideMaterial: {
            return {
                ...state,
                outsideMaterial: action.payload
            };
        }

        case ERightpanelActions.ChangeFoundationMaterial: {
            return {
                ...state,
                foundationMaterial: action.payload
            };
        }

        case ERightpanelActions.ChangeWallpapers: {
            return {
                ...state,
                wallpapers: action.payload
            };
        }

        case ERightpanelActions.ChangeFloor: {
            return {
                ...state,
                floor: action.payload
            };
        }

        case ERightpanelActions.ChangeBalconyFloor: {
            return {
                ...state,
                balconyFloor: action.payload
            };
        }

        case ERightpanelActions.ChangePorchFloor: {
            return {
                ...state,
                porchFloor: action.payload
            };
        }

        case ERightpanelActions.ChangeRoomName: {
            return {
                ...state,
                roomName: action.payload
            };
        }

        case ERightpanelActions.MoveObject: {
            return {
                ...state,
                isDraggable: action.payload
            };
        }
        case ERightpanelActions.RotateObjectClockwise: {
            return {
                ...state,
                rotation: action.payload
            };
        }
        case ERightpanelActions.RotateObjectCounterClockwise: {
            return {
                ...state,
                rotation: action.payload
            };
        }
        case ERightpanelActions.RoofTransformation: {
            return {
                ...state,
                roofTransformation: action.payload
            };
        }
        case ERightpanelActions.ChangeWallTextureType: {
            return {
                ...state,
                wallTextureType: action.payload
            };
        }
        case ERightpanelActions.ChangeFloorTextureType: {
            return {
                ...state,
                floorTextureType: action.payload
            };
        }
        default:
            return state;
    }

};