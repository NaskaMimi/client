import {Action} from "@ngrx/store";
import {Room} from "../../walls/elements/room";
import * as THREE from 'three';
import {RoomDto} from "../../dtos/room.dto";
import {WallDto} from "../../dtos/wall.dto";
import {Model3DDto} from "../../dtos/model3D.dto";
import {PorchDto} from "../../dtos/porch.dto";
import {BalconyDto} from "../../dtos/balcony.dto";
import {RoofDto} from "../../dtos/roof.dto";

export enum ERightpanelActions {
    ShowMainInfo = 'Show Main Info',
    ShowRoomInfo = 'Show Room Info',
    ShowWallInfo = 'Show Wall Info',
    ShowObjectInfo = 'Show Object Info',
    ShowRoofInfo = 'Show Roof Info',
    ShowBalconyInfo = 'Show Balcony Info',
    ShowPorchInfo = 'Show Porch Info',
    ChangeOutsideMaterial = 'Change Outside Material',
    ChangeRoofMaterial = 'Change Roof Material',
    ChangeFoundationMaterial = 'Change Foundation Material',
    ChangeWallpapers = 'Change Wallpapers',
    ChangeFloor = 'Change Floor',
    ChangeBalconyFloor = 'Change Balcony Floor',
    ChangePorchFloor = 'Change Porch Floor',
    ChangeRoomName = 'Change Room Name',
    MoveObject = 'Move Object',
    RotateObjectClockwise = 'Rotate Object Clockwise',
    RotateObjectCounterClockwise = 'Rotate Object Counter Clockwise',
    RoofTransformation = 'Roof Transformation',
    ChangeWallTextureType = 'Change Wall Texture Type',
    ChangeFloorTextureType = 'Change Floor Texture Type'
}

export class ShowMainInfo implements Action {
    public readonly type = ERightpanelActions.ShowMainInfo;
}

export class ShowRoomInfo implements Action {
    public readonly type = ERightpanelActions.ShowRoomInfo;

    constructor(public payload: RoomDto) {
    }
}

export class ShowWallInfo implements Action {
    public readonly type = ERightpanelActions.ShowWallInfo;

    constructor(public payload: WallDto) {
    }
}

export class ShowObjectInfo implements Action {
    public readonly type = ERightpanelActions.ShowObjectInfo;

    constructor(public payload: Model3DDto) {
    }
}

export class ShowRoofInfo implements Action {
    public readonly type = ERightpanelActions.ShowRoofInfo;

    constructor(public payload: RoofDto) {
    }
}

export class ShowBalconyInfo implements Action {
    public readonly type = ERightpanelActions.ShowBalconyInfo;

    constructor(public payload: BalconyDto) {
    }
}

export class ShowPorchInfo implements Action {
    public readonly type = ERightpanelActions.ShowPorchInfo;

    constructor(public payload: PorchDto) {
    }
}

export class ChangeOutsideMaterial implements Action {
    public readonly type = ERightpanelActions.ChangeOutsideMaterial;

    constructor(public payload: string) {
    }
}

export class ChangeRoofMaterial implements Action {
    public readonly type = ERightpanelActions.ChangeRoofMaterial;

    constructor(public payload: string) {
    }
}

export class ChangeFoundationMaterial implements Action {
    public readonly type = ERightpanelActions.ChangeFoundationMaterial;

    constructor(public payload: string) {
    }
}

export class ChangeWallpapers implements Action {
    public readonly type = ERightpanelActions.ChangeWallpapers;

    constructor(public payload: string) {
    }
}

export class ChangeFloor implements Action {
    public readonly type = ERightpanelActions.ChangeFloor;

    constructor(public payload: string) {
    }
}

export class ChangeBalconyFloor implements Action {
    public readonly type = ERightpanelActions.ChangeBalconyFloor;

    constructor(public payload: string) {
    }
}

export class ChangePorchFloor implements Action {
    public readonly type = ERightpanelActions.ChangePorchFloor;

    constructor(public payload: string) {
    }
}

export class ChangeRoomName implements Action {
    public readonly type = ERightpanelActions.ChangeRoomName;

    constructor(public payload: string) {
    }
}

export class MoveObject implements Action {
    public readonly type = ERightpanelActions.MoveObject;

    constructor(public payload: boolean) {
    }
}

export class RotateObjectClockwise implements Action {
    public readonly type = ERightpanelActions.RotateObjectClockwise;

    constructor(public payload: number) {
    }
}

export class RotateObjectCounterClockwise implements Action {
    public readonly type = ERightpanelActions.RotateObjectCounterClockwise;

    constructor(public payload: number) {
    }
}

export class RoofTransformation implements Action {
    public readonly type = ERightpanelActions.RoofTransformation;

    constructor(public payload: string) {
    }
}

export class ChangeWallTextureType implements Action {
    public readonly type = ERightpanelActions.ChangeWallTextureType;

    constructor(public payload: string) {
    }
}

export class ChangeFloorTextureType implements Action {
    public readonly type = ERightpanelActions.ChangeFloorTextureType;

    constructor(public payload: string) {
    }
}

export type RightpanelActions = ShowMainInfo | ShowRoomInfo | ShowWallInfo | ShowObjectInfo | ShowRoofInfo | ShowBalconyInfo| ShowPorchInfo | ChangeOutsideMaterial | ChangeRoofMaterial | ChangeFoundationMaterial | ChangeWallpapers | ChangeFloor | ChangeBalconyFloor | ChangePorchFloor | ChangeRoomName | MoveObject | RotateObjectClockwise | RotateObjectCounterClockwise | RoofTransformation | ChangeWallTextureType | ChangeFloorTextureType;