import {Room} from "../../walls/elements/room";
import * as THREE from 'three';
import {RoomDto} from "../../dtos/room.dto";
import {WallDto} from "../../dtos/wall.dto";
import {Model3DDto} from "../../dtos/model3D.dto";
import {PorchDto} from "../../dtos/porch.dto";
import {BalconyDto} from "../../dtos/balcony.dto";
import {RoofDto} from "../../dtos/roof.dto";

export interface IRightpanelState {
    elementToShow: string;
    selectedRoom:RoomDto;
    selectedWall:WallDto;
    selectedObject:Model3DDto;
    selectedRoof:RoofDto;
    selectedBalcony:BalconyDto;
    selectedPorch:PorchDto;
    roofMaterial:string;
    outsideMaterial:string;
    foundationMaterial:string;
    wallpapers:string;
    floor:string;
    balconyFloor:string;
    porchFloor:string;
    roomName:string;
    isDraggable:boolean;
    rotation:number;
    roofTransformation:string;
    wallTextureType:string;
    floorTextureType:string;
}

export const initialRightpanelState:IRightpanelState = {
    elementToShow: "main-info",
    selectedRoom: null,
    selectedWall:null,
    selectedObject:null,
    selectedRoof:null,
    selectedBalcony:null,
    selectedPorch:null,
    roofMaterial:"",
    outsideMaterial:"",
    foundationMaterial:"",
    wallpapers:"",
    floor:"",
    balconyFloor:"",
    porchFloor:"",
    roomName:"",
    isDraggable:false,
    rotation:0,
    roofTransformation:"",
    wallTextureType:"",
    floorTextureType:""

}