import {RouterReducerState} from "@ngrx/router-store";
import {initialRightpanelState, IRightpanelState} from "./rightpanel.state";
import {Store} from "@ngrx/store";

export interface IAppState {
    router?: RouterReducerState;
    rightpanel: IRightpanelState;
}

export const initialAppState:IAppState = {
    rightpanel: initialRightpanelState
}

export function getInitialState(): IAppState {
    return initialAppState;
}
