export class ObjectToSave {
  constructor(public name: string,
              public positionX: number,
              public positionY: number,
              public width: number,
              public height: number,
              public rotation: number,
              public selectedIndex:number
  ) {
  }
}
