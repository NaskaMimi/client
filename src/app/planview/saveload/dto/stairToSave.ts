export class StairToSave{
  constructor(public positionX: number,
              public positionY: number,
              public width: number,
              public height: number,
              public stairTexture: string
  ) {
  }
}
