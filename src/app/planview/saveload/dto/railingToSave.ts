export class RailingToSave{
  constructor(public positionX: number,
              public positionY: number,
              public width: number,
              public height: number,
              public railingTexture: string
  ) {
  }
}
