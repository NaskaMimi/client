import {WallForSave} from "./wallForSave";
import {PorchToSave} from "./porchToSave";
import {BalconyToSave} from "./balconyToSave";
import {RoofToSave} from "./roofToSave";
import {ObjectToSave} from "./objectToSave";
import {RailingToSave} from "./railingToSave";
import {StairToSave} from "./stairToSave";

export class SceneToSave{
  constructor(public outsideTexture: string,
              public foundationTexture: string,
              public roofTexture: string,
              public porches: PorchToSave[],
              public balconies2:BalconyToSave[],
              public balconies3:BalconyToSave[],
              public roofs1:RoofToSave[],
              public roofs2:RoofToSave[],
              public roofs3:RoofToSave[],
              public objects1:ObjectToSave[],
              public objects2:ObjectToSave[],
              public objects3:ObjectToSave[],
              public railings1:RailingToSave[],
              public railings2:RailingToSave[],
              public railings3:RailingToSave[],
              public stairs:ObjectToSave[],
              public walls1:WallForSave[],
              public walls2:WallForSave[],
              public walls3:WallForSave[]) {
  }
}
