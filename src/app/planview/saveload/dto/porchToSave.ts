export class PorchToSave {
  constructor(public positionX: number,
              public positionY: number,
              public width: number,
              public height: number,
              public floorTexture: string
  ) {
  }
}
