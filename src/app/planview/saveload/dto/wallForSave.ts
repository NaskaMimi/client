export class WallForSave{
  constructor(public vertex1X:number,
              public vertex1Y:number,
              public vertex2X:number,
              public vertex2Y:number,
              public wallAxis:String,
              public orientation:string,
              public paintedWallsNumbers:number[],
              public mixedMaterials:string[]) {
  }
}
