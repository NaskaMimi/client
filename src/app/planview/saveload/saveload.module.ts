import {NgModule} from "@angular/core";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {SaveloadService} from "./service/saveload.service";
import {SaveloadComponent} from "./saveload.component";

@NgModule({
  imports: [HttpClientModule, RouterModule],
  declarations: [SaveloadComponent],
  exports: [
    SaveloadComponent
  ],
  providers: [
    SaveloadService
  ]
})
export class SaveloadModule {
}
