import * as THREE from 'three';
import {PlanViewContext} from "../../context/planViewContext";
import {Wall} from "../../walls/elements/wall";
import {WallForSave} from "../dto/wallForSave";
import {SceneToSave} from "../dto/sceneToSave";
import {Materials} from "../../walls/materials";
import {Drawings} from "../../walls/wallsUtils/drawings";
import {PlanViewEnvironment} from "../../context/planViewEnvironment";
import {PorchToSave} from "../dto/porchToSave";
import {PlanViewSettings} from "../../configuration/planViewSettings";
import {StairToSave} from "../dto/stairToSave";
import {ObjectToSave} from "../dto/objectToSave";
import {RoofToSave} from "../dto/roofToSave";
import {BalconyToSave} from "../dto/balconyToSave";
import {RailingToSave} from "../dto/railingToSave";
import {Loader3DObjects} from "../../objects3D/loader3DObjects";
import {Objects} from "../../objects3D/objects";

export class JsonParser {
  constructor() {

  }

  public static saveToJSON():string {
    var walls1 = this.extractWalls(1);
    var walls2 = this.extractWalls(2);
    var walls3 = this.extractWalls(3);

    var porches = this.extractBoxes(PlanViewContext.getInstance().getPorches());

    var stairs: ObjectToSave[] = this.extractObjects(PlanViewContext.getInstance().getStairs());

    var railings1: RailingToSave[] = [];
    var railings2: RailingToSave[] = [];
    var railings3: RailingToSave[] = [];

    var objects1: ObjectToSave[] = this.extractObjects(PlanViewContext.getInstance().getModels3DByLevel(1));
    var objects2: ObjectToSave[] = this.extractObjects(PlanViewContext.getInstance().getModels3DByLevel(2));
    var objects3: ObjectToSave[] = this.extractObjects(PlanViewContext.getInstance().getModels3DByLevel(3));

    var roofs1: RoofToSave[] = [];
    var roofs2: RoofToSave[] = [];
    var roofs3: RoofToSave[] = [];

    var balcony2: BalconyToSave[] = this.extractBoxes(PlanViewContext.getInstance().getBalconyByLevel(2));
    var balcony3: BalconyToSave[] = this.extractBoxes(PlanViewContext.getInstance().getBalconyByLevel(3));

    var scene: SceneToSave = new SceneToSave(PlanViewContext.getInstance().outsideTexture,
      PlanViewContext.getInstance().foundationTexture,
      PlanViewContext.getInstance().roofTexture, porches, balcony2, balcony3, roofs1, roofs2, roofs3,
      objects1, objects2, objects3, railings1, railings2, railings3, stairs,
      walls1, walls2, walls3);

    var json = JSON.stringify(scene);
    console.log(json);
    return json;
  }

  public static loadFromJSON() {
    var json = this.getJson();
    const scene: SceneToSave = JSON.parse(json);

    PlanViewContext.getInstance().outsideTexture = scene.outsideTexture;
    PlanViewContext.getInstance().foundationTexture = scene.foundationTexture;
    PlanViewContext.getInstance().roofTexture = scene.roofTexture;
    this.loadWalls(scene);
    this.loadObjects(scene);
    this.loadStairs(scene);
    this.loadPorches(scene);
    this.loadBalconies(scene);

    PlanViewEnvironment.getInstance().render();
  }

  private static extractWalls(level: number) {
    let walls: THREE.Mesh[] = PlanViewContext.getInstance().getWallsByLevel(level);
    var wallForSaves: WallForSave[] = [];
    for (var wall of walls) {
      var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall);
      var wallForSave: WallForSave = new WallForSave(
        wallClass.vertex1.x, wallClass.vertex1.y, wallClass.vertex2.x, wallClass.vertex2.y,
        wallClass.wallAxis, wallClass.orientation, wallClass.paintedWallsNumbers, []);
      wallForSaves.push(wallForSave);
    }
    return wallForSaves;
  }

  private static extractObjects(objects: THREE.Mesh[]): ObjectToSave[] {
    var objectsToSave: ObjectToSave[] = [];
    for (var object of objects) {
      var objectInfo = PlanViewContext.getInstance().models3DInformation.get(object);
      var objectToSave: ObjectToSave = new ObjectToSave(object.name, object.position.x, object.position.z, 1, 1, object.rotation.y, objectInfo.selectedIndex);
      objectsToSave.push(objectToSave);
    }
    return objectsToSave;
  }

  private static extractBoxes(boxes: THREE.Mesh[]) {
    // let porches: THREE.Mesh[] = PlanViewContext.getInstance().getPorches();
    var boxesToSave: PorchToSave[] = [];
    for (var box of boxes) {
      var boxToSave: PorchToSave = new PorchToSave(box.position.x, box.position.z, box.scale.x, box.scale.z, "");
      boxesToSave.push(boxToSave);
    }
    return boxesToSave;
  }

  private static loadWalls(scene: SceneToSave) {
    this.drawWallsForLevel(scene.walls1, PlanViewSettings.LEVEL1_POSITION, 1);
    this.drawWallsForLevel(scene.walls2, PlanViewSettings.LEVEL2_POSITION, 2);
    this.drawWallsForLevel(scene.walls3, PlanViewSettings.LEVEL3_POSITION, 3);
  }

  private static loadObjects(scene: SceneToSave) {
    this.drawObjectsForLevel(scene.objects1, PlanViewSettings.LEVEL1_POSITION, 1);
    this.drawObjectsForLevel(scene.objects2, PlanViewSettings.LEVEL2_POSITION, 2);
    this.drawObjectsForLevel(scene.objects3, PlanViewSettings.LEVEL3_POSITION, 3);
  }

  private static loadStairs(scene: SceneToSave) {
    this.drawObjectsForLevel(scene.stairs, PlanViewSettings.LEVEL0_POSITION, 0);
  }

  private static drawWallsForLevel(walls: WallForSave[], yPosition: number, level: number) {
    PlanViewContext.getInstance().yPosition = yPosition;
    PlanViewContext.getInstance().currentLevel = level;

    for (var wall of walls) {
      this.createWall(wall, yPosition);
    }

    let drawedWalls = PlanViewContext.getInstance().getWallsByLevel(level);
    for (var drawedWall of drawedWalls) {
      var wallArray = [];
      PlanViewContext.getInstance().wallsManager.findClosedSpace(drawedWall, wallArray, drawedWall, drawedWalls, new Map(), new Map());
    }
  }

  private static drawObjectsForLevel(objects: ObjectToSave[], yPosition: number, level: number) {
    PlanViewContext.getInstance().yPosition = yPosition;
    PlanViewContext.getInstance().currentLevel = level;

    for (var object of objects) {
      this.createObject(object, yPosition);
    }

  }

  private static loadPorches(scene: SceneToSave) {
    for (var porch of scene.porches) {
      this.createPorch(porch);
    }
  }

  private static loadBalconies(scene: SceneToSave) {
    this.drawBalconiesForLevel(scene.balconies2, PlanViewSettings.LEVEL2_POSITION, 2);
    this.drawBalconiesForLevel(scene.balconies3, PlanViewSettings.LEVEL3_POSITION, 3);
  }

  private static drawBalconiesForLevel(balconies: BalconyToSave[], yPosition: number, level: number) {
    PlanViewContext.getInstance().yPosition = yPosition;
    PlanViewContext.getInstance().currentLevel = level;

    for (var balcony of balconies) {
      this.createBalcony(balcony, yPosition);
    }
  }

  private static createObject(objectToSave: ObjectToSave, yPosition: number) {
    var objectInfo = Objects.findObjectByName(objectToSave.name);
    new Loader3DObjects(PlanViewEnvironment.getInstance().scene, objectInfo).model3D.subscribe((model => {
        model.rotation.y = objectToSave.rotation;

        model.position.x = objectToSave.positionX;
        model.position.z = objectToSave.positionY;
        model.position.y = objectInfo.yPosition + yPosition;

        if(objectInfo.type == "Stair"){
          model.children[1].material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().foundationTexture);
        } else {
          if (objectInfo.textures.length > 0) {
            var textures = objectInfo.textures[objectToSave.selectedIndex].textures;
            for (var i = 0; i < textures.length; i++) {
              model.children[i].material = textures[i];
            }
          }
        }
        PlanViewContext.getInstance().models3DInformation.set(model, objectInfo);
        PlanViewEnvironment.getInstance().render();
      }
    ));
  }

  private static createWall(wallForSave: WallForSave, yPosition: number) {
    var material = Materials.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewContext.getInstance().outsideTexture);
    var wall = Drawings.drawWallFromJSON("wall" + Math.floor(Math.random() * 100), PlanViewContext.getInstance().getWalls(), new THREE.Vector3(wallForSave.vertex1X, yPosition, wallForSave.vertex1Y), new THREE.Vector3(wallForSave.vertex2X, yPosition, wallForSave.vertex2Y),
      material, wallForSave.wallAxis, wallForSave.orientation);
    this.addWallToArrays(wall, wallForSave.wallAxis);
    PlanViewContext.getInstance().wallsManager.addWall(
      new Wall(wall, new THREE.Vector2(wallForSave.vertex1X, wallForSave.vertex1Y), new THREE.Vector2(wallForSave.vertex2X, wallForSave.vertex2Y),
        wallForSave.wallAxis, wallForSave.orientation, wallForSave.paintedWallsNumbers, null));
  }

  private static createPorch(porchToSave: PorchToSave) {
    var mixedMaterials = Materials.createMixedMaterial(PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg");

    var porchGeometry = new THREE.BoxGeometry(PlanViewSettings.WALL_INITIAL_SIZE, PlanViewSettings.FOUNDATION_HEIGHT, PlanViewSettings.WALL_INITIAL_SIZE);
    porchGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.FOUNDATION_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);

    var porch = new THREE.Mesh(porchGeometry, mixedMaterials);
    porch.castShadow = true;
    porch.receiveShadow = true;
    porch.position.x = porchToSave.positionX;
    //What's the.. ?
    // porch.position.y = -0.5;
    porch.position.z = porchToSave.positionY;
    porch.position.divideScalar(50).floor().multiplyScalar(50).addScalar(0);

    porch.scale.x = porchToSave.width;
    porch.scale.z = porchToSave.height;
    PlanViewEnvironment.getInstance().scene.add(porch);
    PlanViewContext.getInstance().addPorch(porch);
  }

  private static createBalcony(balconyToSave: BalconyToSave, yPosition:number) {
    var mixedMaterials = Materials.createMixedMaterial(PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg");

    var balconyGeometry = new THREE.BoxGeometry(PlanViewSettings.WALL_INITIAL_SIZE, 30, PlanViewSettings.WALL_INITIAL_SIZE);
    balconyGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, 0, PlanViewSettings.WALL_INITIAL_SIZE / 2);

    var balcony = new THREE.Mesh(balconyGeometry, mixedMaterials);
    balcony.castShadow = true;
    balcony.receiveShadow = true;
    // balcony.position.copy(intersect.point);
    balcony.position.x = balconyToSave.positionX;
    balcony.position.z = balconyToSave.positionY;
    balcony.position.divideScalar(50).floor().multiplyScalar(50).addScalar(0);
    // balcony.position.y = PlanViewContext.getInstance().yPosition;
    balcony.position.y = yPosition;

    balcony.scale.x = balconyToSave.width;
    balcony.scale.z = balconyToSave.height;

    // this.currentBalcony = balcony;
    PlanViewEnvironment.getInstance().scene.add(balcony);
    PlanViewContext.getInstance().addBalcony(balcony);
    // this.balconyTextures.set(balcony, PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg");
  }

  private static addWallToArrays(wall, axis) {
    PlanViewEnvironment.getInstance().scene.add(wall);
    PlanViewContext.getInstance().addObject(wall);
    PlanViewContext.getInstance().addWall(wall);
  }


  private static getJson() {
    return '{"outsideTexture":"../assets/wallpapers/wallpaper4.jpg","foundationTexture":"../assets/wallpapers/wallpaper4.jpg","roofTexture":"../assets/roofs/roof1.jpg","porches":[{"positionX":-350,"positionY":-50,"width":600,"height":550,"floorTexture":""}],"balconies2":[],"balconies3":[],"roofs1":[],"roofs2":[],"roofs3":[],"objects1":[{"name":"sofa3","positionX":-50,"positionY":300,"width":1,"height":1,"rotation":0,"selectedIndex":11}],"objects2":[],"objects3":[],"railings1":[],"railings2":[],"railings3":[],"stairs":[],"walls1":[],"walls2":[],"walls3":[]}';

  }

}
