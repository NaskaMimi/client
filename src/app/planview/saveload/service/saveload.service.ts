import {environment} from "../../../../environments/environment";
import {RegistrationModel} from "../../../authorization/authservice/auth.dto";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {ProjectDto} from "../dto/project.dto";

@Injectable()
export class SaveloadService{

  constructor(public http: HttpClient) {
  }

  save(name: string, payload: string) {
    return this.http.post(`${environment.backendUrl}/api/v1/project`,
      new ProjectDto(name, payload))
      .subscribe(
        data => console.log(data),
        err => console.log(err)
      );
  }
}
