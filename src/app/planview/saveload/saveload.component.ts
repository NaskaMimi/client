import {Component} from "@angular/core";
import {SaveloadService} from "./service/saveload.service";
import {JsonParser} from "./json/jsonParser";

@Component({
  selector: 'save-load',
  templateUrl: './saveload.component.html'
})
export class SaveloadComponent {
  constructor(public saveloadService: SaveloadService) {
  }

  createNewProject(){
    (<HTMLInputElement>document.getElementById("type-project-name")).style.visibility = "visible";
    (<HTMLInputElement>document.getElementById("save-project")).style.visibility = "hidden";
  }

  closeModal(){
    (<HTMLInputElement>document.getElementById("modalSheet")).style.visibility = "hidden";
    (<HTMLInputElement>document.getElementById("type-project-name")).style.visibility = "hidden";
    (<HTMLInputElement>document.getElementById("save-project")).style.visibility = "hidden";
  }

  openSaveModal() {
    (<HTMLInputElement>document.getElementById("modalSheet")).style.visibility = "visible";

    (<HTMLInputElement>document.getElementById("type-project-name")).style.visibility = "hidden";
    (<HTMLInputElement>document.getElementById("save-project")).style.visibility = "visible";
    // JsonParser.saveToJSON();
  }

  openLoadModal() {
    (<HTMLInputElement>document.getElementById("modalSheet")).style.visibility = "visible";
    // JsonParser.loadFromJSON();
  }

  save(){
    var json:string = JsonParser.saveToJSON();
    this.saveloadService.save("new project", json);
    this.closeModal();
  }

  load(){
    JsonParser.loadFromJSON();
    this.closeModal();
  }

}
