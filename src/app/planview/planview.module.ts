import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {PlanviewComponent} from "./planview.component";
import {StoreModule} from "@ngrx/store";
import {appReducers} from "./store/reducers/app.reducers";
import {StoreRouterConnectingModule} from "@ngrx/router-store";
import {RightpanelModule} from "./rightpanel/rightpanel.module";
import {HeaderModule} from "../header/header.module";
import {SaveloadModule} from "./saveload/saveload.module";

@NgModule({
    imports: [
        HttpClientModule,
        StoreModule.forRoot(appReducers),
        StoreRouterConnectingModule.forRoot({stateKey: 'router'}),
        RightpanelModule,
        HeaderModule,
        SaveloadModule
    ],
    exports: [HttpClientModule],
    declarations: [PlanviewComponent],
    bootstrap: [PlanviewComponent]
})
export class PlanviewModule {}
