import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Calculation} from "./calculation";
import * as THREE from 'three';
import {PlanViewContext} from "../context/planViewContext";
import {Object3D} from "../objects3D/object3D";
import {PlanViewSettings} from "../configuration/planViewSettings";
import {Materials} from "../walls/materials";
import {anything, instance, mock, when} from "ts-mockito";
import {WallsManager} from "../walls/wallsManager";
import {Wall} from "../walls/elements/wall";
import {Room} from "../walls/elements/room";

let mockWallManager = mock(WallsManager);

describe('AppComponent', () => {
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule
            ],
            declarations: [
                // AppComponent
            ],
        }).compileComponents();
    });

  it('should calculate square', () => {
    var mesh: THREE.Mesh = new THREE.Mesh();
    mesh.scale.x = 100;
    mesh.scale.z = 100;
    var result = Calculation.calculateSquare(mesh);
    expect(result).toEqual(1);
  });

  // ___________ isObjectIntersectOtherWalls _________________________________
  it('should intersect horizontal wall', () => {
    var mesh = createObject(98, 98, 100, -40);
    createWall("x", new THREE.Vector2(0, 0), new THREE.Vector2(400, 0));

    var result = Calculation.isObjectIntersectOtherWalls(mesh);
    expect(result).toBeTrue();
  });

  it('should intersect vertical wall', () => {
    var mesh = createObject(98, 98, -40, 100);
    createWall("z", new THREE.Vector2(0, 0), new THREE.Vector2(0, 400));

    var result = Calculation.isObjectIntersectOtherWalls(mesh);
    expect(result).toBeTrue();
  });

  it('should not intersect wall from the bottom', () => {
    var mesh = createObject(98, 98, 100, -50);
    createWall("x", new THREE.Vector2(0, 0), new THREE.Vector2(400, 0));

    var result = Calculation.isObjectIntersectOtherWalls(mesh);
    expect(result).toBeFalse();
  });

  it('should not intersect wall from the top', () => {
    var mesh = createObject(98, 98, 100, 50);
    createWall("x", new THREE.Vector2(0, 0), new THREE.Vector2(400, 0));

    var result = Calculation.isObjectIntersectOtherWalls(mesh);
    expect(result).toBeFalse();
  });

  // it('should intersect wall from the left1', () => {
  //   var mesh = createObject(98, 98, -50, 100);
  //   createWall("z", new THREE.Vector2(0, 0), new THREE.Vector2(0, 400));
  //
  //   var result = Calculation.isObjectIntersectOtherWalls(mesh);
  //   expect(result).toBeTrue();
  // });
  //
  // it('should intersect wall from the right1', () => {
  //   var mesh = createObject(98, 98, 50, 100);
  //   createWall("z", new THREE.Vector2(0, 0), new THREE.Vector2(0, 400));
  //
  //   var result = Calculation.isObjectIntersectOtherWalls(mesh);
  //   expect(result).toBeTrue();
  // });


  // ___________ isWallIntersectOtherObjects _________________________________
  it('should intersect horizontal wall', () => {
    var mesh = createObject(98, 98, 100, -40);
    var wall = createWall("x", new THREE.Vector2(0, 0), new THREE.Vector2(400, 0));

    var result = Calculation.isWallIntersectOtherObjects(wall, [mesh]);
    expect(result).toBeTrue();
  });

  it('should intersect vertical wall', () => {
    var mesh = createObject(98, 98, -40, 100);
    var wall = createWall("z", new THREE.Vector2(0, 0), new THREE.Vector2(0, 400));

    var result = Calculation.isWallIntersectOtherObjects(wall, [mesh]);
    expect(result).toBeTrue();
  });

  it('should not intersect wall from the bottom', () => {
    var mesh = createObject(98, 98, 100, -50);
    var wall = createWall("x", new THREE.Vector2(0, 0), new THREE.Vector2(400, 0));

    var result = Calculation.isWallIntersectOtherObjects(wall, [mesh]);
    expect(result).toBeFalse();
  });

  it('should not intersect wall from the top', () => {
    var mesh = createObject(98, 98, 100, 50);
    var wall = createWall("x", new THREE.Vector2(0, 0), new THREE.Vector2(400, 0));

    var result = Calculation.isWallIntersectOtherObjects(wall, [mesh]);
    expect(result).toBeFalse();
  });

  it('should not intersect wall from the left', () => {
    var mesh = createObject(98, 98, -50, 100);
    var wall = createWall("z", new THREE.Vector2(0, 0), new THREE.Vector2(0, 400));

    var result = Calculation.isWallIntersectOtherObjects(wall, [mesh]);
    expect(result).toBeFalse();
  });

  it('should not intersect wall from the right', () => {
    var mesh = createObject(98, 98, 50, 100);
    var wall = createWall("z", new THREE.Vector2(0, 0), new THREE.Vector2(0, 400));

    var result = Calculation.isWallIntersectOtherObjects(wall, [mesh]);
    expect(result).toBeFalse();
  });


  // ___________ areWallCoordinatesMatchAnotherObjectCoordinates _________________________________

  it('horizontal wall should completely be on object top left', () => {
    var mesh = createBox(300, 300, 0, 0);
    let axis = "x";
    var wall = createWall(axis, new THREE.Vector2(0, 0), new THREE.Vector2(300, 0));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh]);
    expect(result).toBeTrue();
  });

  it('horizontal wall should completely be on object bottom right', () => {
    var mesh = createBox(-300, -300, 300, 300);
    let axis = "x";
    var wall = createWall(axis, new THREE.Vector2(0, 0), new THREE.Vector2(300, 0));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh]);
    expect(result).toBeTrue();
  });

  it('horizontal wall should completely be on object bottom left', () => {
    var mesh = createBox(300, -300, 0, 300);
    let axis = "x";
    var wall = createWall(axis, new THREE.Vector2(0, 0), new THREE.Vector2(300, 0));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh]);
    expect(result).toBeTrue();
  });

  it('horizontal wall should completely be on object top right', () => {
    var mesh = createBox(-300, 300, 300, 0);
    let axis = "x";
    var wall = createWall(axis, new THREE.Vector2(0, 0), new THREE.Vector2(300, 0));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh]);
    expect(result).toBeTrue();
  });

  it('vertical wall should completely be on object top left', () => {
    var mesh = createBox(300, 300, 0, 0);
    let axis = "z";
    var wall = createWall(axis, new THREE.Vector2(0, 0), new THREE.Vector2(0, 300));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh]);
    expect(result).toBeTrue();
  });

  it('vertical wall should completely be on object bottom right', () => {
    var mesh = createBox(-300, -300, 300, 300);
    let axis = "z";
    var wall = createWall(axis, new THREE.Vector2(0, 0), new THREE.Vector2(0, 300));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh]);
    expect(result).toBeTrue();
  });

  it('vertical wall should completely be on object bottom left', () => {
    var mesh = createBox(300, -300, 0, 300);
    let axis = "z";
    var wall = createWall(axis, new THREE.Vector2(0, 0), new THREE.Vector2(0, 300));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh]);
    expect(result).toBeTrue();
  });

  it('vertical wall should completely be on object top right', () => {
    var mesh = createBox(-300, 300, 300, 0);
    let axis = "z";
    var wall = createWall(axis, new THREE.Vector2(0, 0), new THREE.Vector2(0, 300));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh]);
    expect(result).toBeTrue();
  });

  it('horizontal wall should completely be on several objects top left positive coordinates', () => {
    var mesh1 = createBox(300, 300, 0, 0);
    var mesh2 = createBox(300, 300, 300, 0);
    let axis = "x";
    var wall = createWall(axis, new THREE.Vector2(0, 0), new THREE.Vector2(600, 0));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh1, mesh2]);
    expect(result).toBeTrue();
  });

  it('horizontal wall should completely be on several objects top left negative coordinates', () => {
    var mesh1 = createBox(300, 300, 0, 0);
    var mesh2 = createBox(300, 300, -300, 0);
    let axis = "x";
    var wall = createWall(axis, new THREE.Vector2(-300, 0), new THREE.Vector2(300, 0));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh1, mesh2]);
    expect(result).toBeTrue();
  });

  it('vertical wall should completely be on several objects top left positive coordinates', () => {
    var mesh1 = createBox(300, 300, 0, 0);
    var mesh2 = createBox(300, 300, 0, 300);
    let axis = "z";
    var wall = createWall(axis, new THREE.Vector2(0, 0), new THREE.Vector2(0, 600));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh1, mesh2]);
    expect(result).toBeTrue();
  });

  it('vertical wall should completely be on several objects top left negative coordinates', () => {
    var mesh1 = createBox(300, 300, 0, 0);
    var mesh2 = createBox(300, 300, 0, -300);
    let axis = "z";
    var wall = createWall(axis, new THREE.Vector2(0, -300), new THREE.Vector2(0, 300));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh1, mesh2]);
    expect(result).toBeTrue();
  });

  it('vertical wall should completely be on several objects top left negative coordinates 2', () => {
    var mesh1 = createBox(300, 350, -150, 100);
    var mesh2 = createBox(250, 150, -150, 600);
    let axis = "z";
    var wall = createWall(axis, new THREE.Vector2(-150, 100), new THREE.Vector2(-150, 600));

    var result = Calculation.areWallCoordinatesMatchBoxCoordinates(axis, wall, [mesh1, mesh2]);
    expect(result).toBeTrue();
  });

  it('vertical wall should intersect horizontal wall', () => {
    var wall = createWall("z", new THREE.Vector2(-250, 100), new THREE.Vector2(150, 100));

    var result = Calculation.isWallIntersectOtherWalls(new THREE.Vector2(150, 0), new THREE.Vector2(150, 300));
    expect(result).toBeTrue();
  });

  // ___________ isBoxIntersectFloor _________________________________

  //TODO make common method for all tests with isBoxIntersectFloor
  it('top left small box should not intersect floor from the right', () => {
    expect(isBoxIntersectFloor(createBox(300, 200, 150, 100))).toBeFalse();
  });

  it('top left small box should intersect floor from the right', () => {
    expect(isBoxIntersectFloor(createBox(300, 200, 100, 100))).toBeTrue();
  });

  it('top left small box should not intersect floor from the left', () => {
    expect(isBoxIntersectFloor(createBox(300, 200, -450, 100))).toBeFalse();
  });

  it('top left small box should intersect floor from the left', () => {
    expect(isBoxIntersectFloor(createBox(300, 200, -300, 100))).toBeTrue();
  });

  it('top left small box should not intersect floor from the bottom', () => {
    expect(isBoxIntersectFloor(createBox(200, 300, -100, 150))).toBeFalse();
  });

  it('top left small box should intersect floor from the bottom', () => {
    expect(isBoxIntersectFloor(createBox(200, 300, -100, 100))).toBeTrue();
  });

  it('top left small box should not intersect floor from the top', () => {
    expect(isBoxIntersectFloor(createBox(200, 300, -100, -450))).toBeFalse();
  });

  it('top left small box should intersect floor from the top', () => {
    expect(isBoxIntersectFloor(createBox(200, 300, -100, -400))).toBeTrue();
  });

  function isBoxIntersectFloor(mesh:THREE.Mesh){
    var wall1 = createWall("x", new THREE.Vector2(-150, 150), new THREE.Vector2(150, 150));
    var wall2 = createWall("z", new THREE.Vector2(150, 150), new THREE.Vector2(150, -150));
    var wall3 = createWall("x", new THREE.Vector2(150, -150), new THREE.Vector2(-150, -150));
    var wall4 = createWall("z", new THREE.Vector2(-150, -150), new THREE.Vector2(-150, 150));

    createRoom([wall1, wall2, wall3, wall4]);
    return Calculation.isBoxIntersectFloor(mesh, [wall1, wall2, wall3, wall4]);
  }

  function createObject(realScaleX:number, realScaleY:number, positionX:number, positionZ:number) {
    var mesh: THREE.Mesh = new THREE.Mesh();
    mesh.scale.x = 1;
    mesh.scale.z = 1;

    mesh.position.x = positionX;
    mesh.position.z = positionZ;

    var objectInfo: Object3D = new Object3D("model", 1, 1, 1, 0, 0, "Furniture", realScaleX, realScaleY, 0, []);
    PlanViewContext.getInstance().models3DInformation.set(mesh, objectInfo);
    return mesh;
  }

  function createRoom(walls:THREE.Mesh[]):Room {
    var room:Room = new Room("room", walls, null, "", "", "", null);
    when(mockWallManager.findRoomByFloor(anything())).thenReturn(room);
    return room;
  }

  function createWall(axis:string, vertex1:THREE.Vector2, vertex2:THREE.Vector2) {
    var wallGeometry = new THREE.BoxGeometry(PlanViewSettings.WALL_INITIAL_SIZE, PlanViewSettings.WALL_HEIGHT, PlanViewSettings.WALL_INITIAL_SIZE);
    if (axis == "x") {
      wallGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, PlanViewSettings.WALL_HEIGHT / 2, 0);
    } else if (axis == "z") {
      wallGeometry.translate(0, PlanViewSettings.WALL_HEIGHT / 2, PlanViewSettings.WALL_INITIAL_SIZE / 2);
    }
    var wall: THREE.Mesh = new THREE.Mesh(wallGeometry, Materials.cubeMaterial);
    PlanViewContext.getInstance().addWall(wall);

    var wallClass: Wall = new Wall(wall, vertex1, vertex2, axis, "", [], []);

    when(mockWallManager.findWallClassByWall(wall)).thenReturn(wallClass);
    PlanViewContext.getInstance().wallsManager = instance(mockWallManager);

    return wall;
  }

  function createBox(width: number, height: number, xPos: number, zPos: number):THREE.Mesh {
    var boxGeometry = new THREE.BoxGeometry(1, PlanViewSettings.FOUNDATION_HEIGHT, 1);
    var box = new THREE.Mesh(boxGeometry, Materials.cubeMaterial);
    box.scale.x = width;
    box.scale.z = height;
    box.position.x = xPos;
    box.position.z = zPos;

    return box;
  }

});
