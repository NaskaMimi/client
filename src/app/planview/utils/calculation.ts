import * as THREE from 'three';
import {Wall} from "../walls/elements/wall";
import {PlanViewContext} from "../context/planViewContext";
import {Room} from "../walls/elements/room";
import {WallsPoints} from "../walls/wallsUtils/wallsPoints";
import {LogHelper} from "../logHelper";

export class Calculation {
  public static calculateSquare(mesh: THREE.Mesh) {
    return mesh.scale.x * mesh.scale.z / 10000;
  }

  public static calculatePerimeter(mesh: THREE.Mesh) {
    return (mesh.scale.x + mesh.scale.z) * 2 / 100;
  }

  public static isObjectIntersectOtherWalls(movingObject: THREE.Mesh) {
    var movingObjectInfo = PlanViewContext.getInstance().models3DInformation.get(movingObject);
    var objectStartPoint: THREE.Vector2 = new THREE.Vector2(movingObject.position.x + movingObjectInfo.realScaleX / 2, movingObject.position.z + movingObjectInfo.realScaleY / 2);
    var objectEndPoint: THREE.Vector2 = new THREE.Vector2(movingObject.position.x - movingObjectInfo.realScaleX / 2, movingObject.position.z - movingObjectInfo.realScaleY / 2);

    for (var otherWall of PlanViewContext.getInstance().getWalls()) {
      var wall2Class: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(otherWall);

      var isCross = WallsPoints.intersect(objectStartPoint, objectEndPoint, wall2Class.vertex1, wall2Class.vertex2);
      if (isCross != false && isCross != true) {
        return true;
      }
    }
    return false;
  }

  public static isWallIntersectOtherObjects(wall: THREE.Mesh, collideMeshList: THREE.Mesh[]) {
    var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall);

    for (var object of collideMeshList) {

      var objectStartPoint: THREE.Vector2;
      var objectEndPoint: THREE.Vector2;
      var movingObjectInfo = PlanViewContext.getInstance().models3DInformation.get(object);
      if (movingObjectInfo) {
        objectStartPoint = new THREE.Vector2(object.position.x + movingObjectInfo.realScaleX / 2, object.position.z + movingObjectInfo.realScaleY / 2);
        objectEndPoint = new THREE.Vector2(object.position.x - movingObjectInfo.realScaleX / 2, object.position.z - movingObjectInfo.realScaleY / 2);
      } else {
        objectStartPoint = new THREE.Vector2(object.position.x, object.position.z);
        objectEndPoint = new THREE.Vector2(object.position.x + object.scale.x, object.position.z + object.scale.z);
      }
      if (Calculation.verticesAreEqual(wallClass.vertex1, objectStartPoint) ||
        Calculation.verticesAreEqual(wallClass.vertex1, objectEndPoint) ||
        Calculation.verticesAreEqual(wallClass.vertex2, objectStartPoint) ||
        Calculation.verticesAreEqual(wallClass.vertex2, objectEndPoint)) {
        continue;
      }
      var isCross = WallsPoints.intersect(objectStartPoint, objectEndPoint, wallClass.vertex1, wallClass.vertex2);
      if (isCross != false && isCross != true) {
        return true;
      }
    }
    return false;
  }

  public static areWallCoordinatesMatchBoxCoordinates(axis: String, wall: THREE.Mesh, objects) {
    var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall);
    return Calculation.isVertexInsideBox(objects, wallClass.vertex1, 50) && Calculation.isVertexInsideBox(objects, wallClass.vertex2, 50);
  }

  public static isWallIntersectOtherWalls(vertex1: THREE.Vector2, vertex2: THREE.Vector2) {
    for (var otherWall of PlanViewContext.getInstance().getWalls()) {
      var wall2Class: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(otherWall);
      var isCross = WallsPoints.intersect(vertex1, vertex2, wall2Class.vertex1, wall2Class.vertex2);
      if (isCross != false && isCross != true) {
        if (!Calculation.isObjectJustTouchTheWall(vertex1, vertex2, wall2Class)) {
          return true;
        }
      }
    }
    return false;
  }

  public static isObjectJustTouchTheWall(objectStartPoint: THREE.Vector2, objectEndPoint: THREE.Vector2, wall: Wall): boolean {
    var vertex1 = wall.vertex1;
    var vertex2 = wall.vertex2;

    return (objectStartPoint.x == vertex1.x && objectStartPoint.x == vertex2.x) ||
      (objectEndPoint.x == vertex1.x && objectEndPoint.x == vertex2.x) ||
      (objectStartPoint.y == vertex1.y && objectStartPoint.y == vertex2.y) ||
      (objectEndPoint.y == vertex1.y && objectEndPoint.y == vertex2.y);

  }

  public static findIntersectWallsByObject(draggableObject) {
    var objectStartPoint: THREE.Vector2;
    var objectEndPoint: THREE.Vector2;

    for (var wall of PlanViewContext.getInstance().getWalls()) {
      var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(wall);

      if (wallClass.wallAxis == "x") {
        objectStartPoint = new THREE.Vector2(draggableObject.position.x, draggableObject.position.z);
        objectEndPoint = new THREE.Vector2(draggableObject.position.x + draggableObject.scale.x, draggableObject.position.z);

        if (wallClass.paintedWallsNumbers[0] == 1) {
          draggableObject.rotation.y = 0; //0
        } else {
          draggableObject.rotation.y = Math.PI; //180
        }

        if (wallClass.vertex1.y == objectStartPoint.y) {

          if ((wallClass.vertex1.x <= objectStartPoint.x && wallClass.vertex2.x >= objectEndPoint.x) ||
            (wallClass.vertex2.x <= objectStartPoint.x && wallClass.vertex1.x >= objectEndPoint.x)) {
            return wall;
          }
        }
      } else {
        objectStartPoint = new THREE.Vector2(draggableObject.position.x, draggableObject.position.z);
        objectEndPoint = new THREE.Vector2(draggableObject.position.x + draggableObject.scale.x, draggableObject.position.z);

        if (wallClass.paintedWallsNumbers[0] == 5) {
          draggableObject.rotation.y = Math.PI / 2; //90
        } else {
          draggableObject.rotation.y = Math.PI * 1.5; //270
        }
        if (wallClass.vertex1.x == objectStartPoint.x) {

          if ((wallClass.vertex1.y <= objectStartPoint.y && wallClass.vertex2.y >= objectEndPoint.y) ||
            (wallClass.vertex2.y <= objectStartPoint.y && wallClass.vertex1.y >= objectEndPoint.y)) {
            return wall;
          }
        }
      }
    }
    return null;
  }

  public static isAllVerticesOf3dModelIntersectBox(movingObject: THREE.Mesh, collideMeshList: THREE.Mesh[]): boolean {
    var movingObjectInfo = PlanViewContext.getInstance().models3DInformation.get(movingObject);

    var objectVertex1: THREE.Vector2 = new THREE.Vector2(movingObject.position.x + movingObjectInfo.realScaleX / 2, movingObject.position.z + movingObjectInfo.realScaleY / 2);
    var objectVertex2: THREE.Vector2 = new THREE.Vector2(movingObject.position.x + movingObjectInfo.realScaleX / 2, movingObject.position.z - movingObjectInfo.realScaleY / 2);
    var objectVertex3: THREE.Vector2 = new THREE.Vector2(movingObject.position.x - movingObjectInfo.realScaleX / 2, movingObject.position.z + movingObjectInfo.realScaleY / 2);
    var objectVertex4: THREE.Vector2 = new THREE.Vector2(movingObject.position.x - movingObjectInfo.realScaleX / 2, movingObject.position.z - movingObjectInfo.realScaleY / 2);
    for (var anotherMesh of collideMeshList) {
      var anotherMeshVertex1: THREE.Vector2;
      var anotherMeshVertex2: THREE.Vector2;
      var anotherMeshVertex3: THREE.Vector2;
      var anotherMeshVertex4: THREE.Vector2;
      var anotherMeshInfo = PlanViewContext.getInstance().models3DInformation.get(anotherMesh);
      if (anotherMeshInfo) {
        anotherMeshVertex1 = new THREE.Vector2(anotherMesh.position.x + anotherMeshInfo.realScaleX / 2, anotherMesh.position.z + anotherMeshInfo.realScaleY / 2);
        anotherMeshVertex2 = new THREE.Vector2(anotherMesh.position.x + anotherMeshInfo.realScaleX / 2, anotherMesh.position.z - anotherMeshInfo.realScaleY / 2);
        anotherMeshVertex3 = new THREE.Vector2(anotherMesh.position.x - anotherMeshInfo.realScaleX / 2, anotherMesh.position.z - anotherMeshInfo.realScaleY / 2);
        anotherMeshVertex4 = new THREE.Vector2(anotherMesh.position.x - anotherMeshInfo.realScaleX / 2, anotherMesh.position.z + anotherMeshInfo.realScaleY / 2);
      } else {
        anotherMeshVertex1 = new THREE.Vector2(anotherMesh.position.x, anotherMesh.position.z);
        anotherMeshVertex2 = new THREE.Vector2(anotherMesh.position.x, anotherMesh.position.z + anotherMesh.scale.z);
        anotherMeshVertex3 = new THREE.Vector2(anotherMesh.position.x + anotherMesh.scale.x, anotherMesh.position.z + anotherMesh.scale.z);
        anotherMeshVertex4 = new THREE.Vector2(anotherMesh.position.x + anotherMesh.scale.x, anotherMesh.position.z);
      }
      var floorVertices: THREE.Vector2[] = [anotherMeshVertex1, anotherMeshVertex2, anotherMeshVertex3, anotherMeshVertex4];
      var isIntersect: boolean =
        Calculation.isPointInsidePolygon(objectVertex1, floorVertices) &&
        Calculation.isPointInsidePolygon(objectVertex2, floorVertices) &&
        Calculation.isPointInsidePolygon(objectVertex3, floorVertices) &&
        Calculation.isPointInsidePolygon(objectVertex4, floorVertices);
      if (isIntersect) {
        return true;
      }
    }
    return false;
  }

  public static isPointIntersectBox(point: THREE.Vector2, collideMeshList: THREE.Mesh[]): boolean {

    for (var anotherMesh of collideMeshList) {
      var anotherMeshVertex1: THREE.Vector2 = new THREE.Vector2(anotherMesh.position.x, anotherMesh.position.z);
      var anotherMeshVertex2: THREE.Vector2 = new THREE.Vector2(anotherMesh.position.x, anotherMesh.position.z + anotherMesh.scale.z);
      var anotherMeshVertex3: THREE.Vector2 = new THREE.Vector2(anotherMesh.position.x + anotherMesh.scale.x, anotherMesh.position.z + anotherMesh.scale.z);
      var anotherMeshVertex4: THREE.Vector2 = new THREE.Vector2(anotherMesh.position.x + anotherMesh.scale.x, anotherMesh.position.z);

      var floorVertices: THREE.Vector2[] = [anotherMeshVertex1, anotherMeshVertex2, anotherMeshVertex3, anotherMeshVertex4];
      var isIntersect: boolean = Calculation.isPointInsidePolygon(point, floorVertices);
      var edgePoints: THREE.Vector2[] = Calculation.collectEdgesPoints(anotherMesh);
      var isOnEdge: boolean = this.isArrayContainsVertex(edgePoints, point);

      if (isIntersect || isOnEdge) {
        return true;
      }
    }
    return false;
  }

  private static isVertexInsideBox(collideMeshList: THREE.Mesh[], objectVertex: THREE.Vector2, amendment: number): boolean {
    for (var mesh of collideMeshList) {
      var startXPosition;
      var startZPosition;
      var endXPosition;
      var endZPosition;

      if (mesh.position.x > mesh.position.x + mesh.scale.x) {
        startXPosition = mesh.position.x + mesh.scale.x;
        endXPosition = mesh.position.x;
      } else {
        startXPosition = mesh.position.x;
        endXPosition = mesh.position.x + mesh.scale.x;
      }

      if (mesh.position.z > mesh.position.z + mesh.scale.z) {
        startZPosition = mesh.position.z + mesh.scale.z;
        endZPosition = mesh.position.z;
      } else {
        startZPosition = mesh.position.z;
        endZPosition = mesh.position.z + mesh.scale.z;
      }

      for (var i = startXPosition; i < endXPosition + amendment; i += 50) {
        for (var j = startZPosition; j < endZPosition + amendment; j += 50) {
          var pointInsideMesh: THREE.Vector2 = new THREE.Vector2(i, j);
          if (objectVertex.x == pointInsideMesh.x && objectVertex.y == pointInsideMesh.y) {
            return true;
          }
        }
      }
    }
    return false;
  }

  private static isArrayContainsVertex(edgePoints: THREE.Vector2[], point: THREE.Vector2) {
    for (var edgePoint of edgePoints) {
      if (edgePoint.equals(point)) {
        return true;
      }
    }
    return false;
  }

  public static collectEdgesPoints(mesh: THREE.Mesh): THREE.Vector2[] {
    var points: THREE.Vector2[] = [];

    var originPoint = mesh.position.clone();

    if (originPoint.x < originPoint.x + mesh.scale.x && originPoint.z < originPoint.z + mesh.scale.z) {
      for (var i = originPoint.x; i < originPoint.x + mesh.scale.x + 50; i += 50) {
        for (var j = originPoint.z; j < originPoint.z + mesh.scale.z + 50; j += 50) {
          var vector: THREE.Vector2 = new THREE.Vector2(i, j);
          points.push(vector);
        }
      }
    }
    if (originPoint.x > originPoint.x + mesh.scale.x && originPoint.z < originPoint.z + mesh.scale.z) {
      for (var i = originPoint.x + mesh.scale.x; i < originPoint.x + 50; i += 50) {
        for (var j = originPoint.z; j < originPoint.z + mesh.scale.z + 50; j += 50) {
          var vector: THREE.Vector2 = new THREE.Vector2(i, j);
          points.push(vector);
        }
      }
    }
    if (originPoint.x < originPoint.x + mesh.scale.x && originPoint.z > originPoint.z + mesh.scale.z) {
      for (var i = originPoint.x; i < originPoint.x + mesh.scale.x + 50; i += 50) {
        for (var j = originPoint.z + mesh.scale.z; j < originPoint.z + 50; j += 50) {
          var vector: THREE.Vector2 = new THREE.Vector2(i, j);
          points.push(vector);
        }
      }
    }
    if (originPoint.x > originPoint.x + mesh.scale.x && originPoint.z > originPoint.z + mesh.scale.z) {
      for (var i = originPoint.x + mesh.scale.x; i < originPoint.x + 50; i += 50) {
        for (var j = originPoint.z + mesh.scale.z; j < originPoint.z + 50; j += 50) {
          var vector: THREE.Vector2 = new THREE.Vector2(i, j);
          points.push(vector);
        }
      }
    }

    return points;
  }

  public static isAtLeastOneVertexOf3dModelIntersectBox(movingObject: THREE.Mesh, collideMeshList: THREE.Mesh[]): boolean {
    var movingObjectInfo = PlanViewContext.getInstance().models3DInformation.get(movingObject);

    var objectVertex1: THREE.Vector2 = new THREE.Vector2(movingObject.position.x + movingObjectInfo.realScaleX / 2, movingObject.position.z + movingObjectInfo.realScaleY / 2);
    var objectVertex2: THREE.Vector2 = new THREE.Vector2(movingObject.position.x + movingObjectInfo.realScaleX / 2, movingObject.position.z - movingObjectInfo.realScaleY / 2);
    var objectVertex3: THREE.Vector2 = new THREE.Vector2(movingObject.position.x - movingObjectInfo.realScaleX / 2, movingObject.position.z + movingObjectInfo.realScaleY / 2);
    var objectVertex4: THREE.Vector2 = new THREE.Vector2(movingObject.position.x - movingObjectInfo.realScaleX / 2, movingObject.position.z - movingObjectInfo.realScaleY / 2);
    for (var anotherMesh of collideMeshList) {
      if (movingObjectInfo.type != "Roof" && movingObject.position.y != anotherMesh.position.y) {
        continue;
      }
      var anotherMeshVertex1: THREE.Vector2;
      var anotherMeshVertex2: THREE.Vector2;
      var anotherMeshVertex3: THREE.Vector2;
      var anotherMeshVertex4: THREE.Vector2;
      var anotherMeshInfo = PlanViewContext.getInstance().models3DInformation.get(anotherMesh);
      if (anotherMeshInfo) {
        anotherMeshVertex1 = new THREE.Vector2(anotherMesh.position.x + anotherMeshInfo.realScaleX / 2, anotherMesh.position.z + anotherMeshInfo.realScaleY / 2);
        anotherMeshVertex2 = new THREE.Vector2(anotherMesh.position.x + anotherMeshInfo.realScaleX / 2, anotherMesh.position.z - anotherMeshInfo.realScaleY / 2);
        anotherMeshVertex3 = new THREE.Vector2(anotherMesh.position.x - anotherMeshInfo.realScaleX / 2, anotherMesh.position.z - anotherMeshInfo.realScaleY / 2);
        anotherMeshVertex4 = new THREE.Vector2(anotherMesh.position.x - anotherMeshInfo.realScaleX / 2, anotherMesh.position.z + anotherMeshInfo.realScaleY / 2);
      } else {
        anotherMeshVertex1 = new THREE.Vector2(anotherMesh.position.x, anotherMesh.position.z);
        anotherMeshVertex2 = new THREE.Vector2(anotherMesh.position.x, anotherMesh.position.z + anotherMesh.scale.z);
        anotherMeshVertex3 = new THREE.Vector2(anotherMesh.position.x + anotherMesh.scale.x, anotherMesh.position.z + anotherMesh.scale.z);
        anotherMeshVertex4 = new THREE.Vector2(anotherMesh.position.x + anotherMesh.scale.x, anotherMesh.position.z);
      }
      var floorVertices: THREE.Vector2[] = [anotherMeshVertex1, anotherMeshVertex2, anotherMeshVertex3, anotherMeshVertex4];
      var isIntersect: boolean =
        Calculation.isPointInsidePolygon(objectVertex1, floorVertices) ||
        Calculation.isPointInsidePolygon(objectVertex2, floorVertices) ||
        Calculation.isPointInsidePolygon(objectVertex3, floorVertices) ||
        Calculation.isPointInsidePolygon(objectVertex4, floorVertices);
      if (isIntersect) {
        return true;
      }
    }
    return false;
  }

  public static isAtLeastOneVertexOfBoxIntersectOtherBox(movingObject: THREE.Mesh, collideMeshList: THREE.Mesh[]): boolean {

    var objectVertex1: THREE.Vector2 = new THREE.Vector2(movingObject.position.x, movingObject.position.z);
    var objectVertex2: THREE.Vector2 = new THREE.Vector2(movingObject.position.x + movingObject.scale.x, movingObject.position.z);
    var objectVertex3: THREE.Vector2 = new THREE.Vector2(movingObject.position.x + movingObject.scale.x, movingObject.position.z + movingObject.scale.z);
    var objectVertex4: THREE.Vector2 = new THREE.Vector2(movingObject.position.x, movingObject.position.z + movingObject.scale.z);
    for (var anotherMesh of collideMeshList) {
      var anotherMeshVertex1: THREE.Vector2 = new THREE.Vector2(anotherMesh.position.x, anotherMesh.position.z);
      var anotherMeshVertex2: THREE.Vector2 = new THREE.Vector2(anotherMesh.position.x, anotherMesh.position.z + anotherMesh.scale.z);
      var anotherMeshVertex3: THREE.Vector2 = new THREE.Vector2(anotherMesh.position.x + anotherMesh.scale.x, anotherMesh.position.z + anotherMesh.scale.z);
      var anotherMeshVertex4: THREE.Vector2 = new THREE.Vector2(anotherMesh.position.x + anotherMesh.scale.x, anotherMesh.position.z);

      var meshVertices: THREE.Vector2[] = [objectVertex1, objectVertex2, objectVertex3, objectVertex4];
      var floorVertices: THREE.Vector2[] = [anotherMeshVertex1, anotherMeshVertex2, anotherMeshVertex3, anotherMeshVertex4];
      var isObjectInsideOtherMesh: boolean =
        Calculation.isPointInsidePolygonExceptEdges(anotherMeshVertex1, meshVertices, movingObject) ||
        Calculation.isPointInsidePolygonExceptEdges(anotherMeshVertex2, meshVertices, movingObject) ||
        Calculation.isPointInsidePolygonExceptEdges(anotherMeshVertex3, meshVertices, movingObject) ||
        Calculation.isPointInsidePolygonExceptEdges(anotherMeshVertex4, meshVertices, movingObject);
      var isIntersect: boolean =
        Calculation.isPointInsidePolygonExceptEdges(objectVertex1, floorVertices, anotherMesh) ||
        Calculation.isPointInsidePolygonExceptEdges(objectVertex2, floorVertices, anotherMesh) ||
        Calculation.isPointInsidePolygonExceptEdges(objectVertex3, floorVertices, anotherMesh) ||
        Calculation.isPointInsidePolygonExceptEdges(objectVertex4, floorVertices, anotherMesh);
      if (isIntersect || isObjectInsideOtherMesh) {
        return true;
      }
    }
    return false;
  }

  public static isAllVerticesOf3dModelIntersectFloor(movingObject: THREE.Mesh, collideMeshList: THREE.Mesh[]): boolean {
    var movingObjectInfo = PlanViewContext.getInstance().models3DInformation.get(movingObject);
    var objectVertex1: THREE.Vector2 = new THREE.Vector2(movingObject.position.x + movingObjectInfo.realScaleX / 2, movingObject.position.z + movingObjectInfo.realScaleY / 2);
    var objectVertex2: THREE.Vector2 = new THREE.Vector2(movingObject.position.x - movingObjectInfo.realScaleX / 2, movingObject.position.z + movingObjectInfo.realScaleY / 2);
    var objectVertex3: THREE.Vector2 = new THREE.Vector2(movingObject.position.x + movingObjectInfo.realScaleX / 2, movingObject.position.z - movingObjectInfo.realScaleY / 2);
    var objectVertex4: THREE.Vector2 = new THREE.Vector2(movingObject.position.x - movingObjectInfo.realScaleX / 2, movingObject.position.z - movingObjectInfo.realScaleY / 2);
    //
    for (var anotherMesh of collideMeshList) {
      var room: Room = PlanViewContext.getInstance().wallsManager.findRoomByFloor(anotherMesh);
      var floorVertices: THREE.Vector2[] = WallsPoints.collectUnicPoints(room.walls);
      var isIntersect: boolean =
        Calculation.isPointInsidePolygon(objectVertex1, floorVertices) &&
        Calculation.isPointInsidePolygon(objectVertex2, floorVertices) &&
        Calculation.isPointInsidePolygon(objectVertex3, floorVertices) &&
        Calculation.isPointInsidePolygon(objectVertex4, floorVertices);
      if (isIntersect) {
        return true;
      }
    }
    return false;
  }

  public static isBoxIntersectFloor(movingObject: THREE.Mesh, collideMeshList: THREE.Mesh[]): boolean {

    var points: THREE.Vector2[] = Calculation.collectInnerPoints(movingObject);

    for (var anotherMesh of collideMeshList) {
      var room: Room = PlanViewContext.getInstance().wallsManager.findRoomByFloor(anotherMesh);
      var floorVertices: THREE.Vector2[] = WallsPoints.collectUnicPoints(room.walls);
      LogHelper.logVerteces(points);
      if(points.length > 0) {
        for (var point of points) {
          var isIntersect: boolean = Calculation.isPointInsidePolygon(point, floorVertices);
          if (isIntersect) {
            return true;
          }
        }
      }
    }
    return false;
  }

  private static isPointInsidePolygon(point: THREE.Vector2, polygonVertices: THREE.Vector2[]): boolean {
    var result: boolean = false;
    var size = polygonVertices.length;
    var j = size - 1;
    for (var i = 0; i < size; i++) {
      if ((polygonVertices[i].y < point.y && polygonVertices[j].y >= point.y ||
        polygonVertices[j].y < point.y && polygonVertices[i].y >= point.y) &&
        (polygonVertices[i].x + (point.y - polygonVertices[i].y) / (polygonVertices[j].y - polygonVertices[i].y) * (polygonVertices[j].x - polygonVertices[i].x) < point.x))
        result = !result;
      j = i;
    }
    return result;
  }

  private static isPointInsidePolygonExceptEdges(point: THREE.Vector2, polygonVertices: THREE.Vector2[], anotherMesh): boolean {
    var result: boolean = false;
    var size = polygonVertices.length;
    var j = size - 1;
    for (var i = 0; i < size; i++) {
      if ((polygonVertices[i].y < point.y && polygonVertices[j].y >= point.y ||
        polygonVertices[j].y < point.y && polygonVertices[i].y >= point.y) &&
        (polygonVertices[i].x + (point.y - polygonVertices[i].y) / (polygonVertices[j].y - polygonVertices[i].y) * (polygonVertices[j].x - polygonVertices[i].x) < point.x))
        result = !result;
      j = i;
    }
    return result && !Calculation.isVertexTouchMeshEdge(point, anotherMesh);
  }

  private static verticesAreEqual(vertex1: THREE.Vector2, vertex2: THREE.Vector2): boolean {
    return vertex1.x == vertex2.x && vertex1.y == vertex2.y;
  }

  //TODO bug here! need to check edges like (vertex.x == anotherMesh.position.x && vertex.z == anotherMesh.position.z)
  private static isVertexTouchMeshEdge(vertex, anotherMesh) {
    return vertex.x == anotherMesh.position.x ||
      vertex.x == anotherMesh.position.x + anotherMesh.scale.x ||
      vertex.y == anotherMesh.position.z ||
      vertex.y == anotherMesh.position.z + anotherMesh.scale.z;
  }

  private static collectInnerPoints(mesh: THREE.Mesh): THREE.Vector2[] {
    var points: THREE.Vector2[] = [];

    var originPoint = mesh.position.clone();

    if (originPoint.x < originPoint.x + mesh.scale.x && originPoint.z < originPoint.z + mesh.scale.z) {
      for (var i = originPoint.x + 25; i < originPoint.x + mesh.scale.x; i += 50) {
        for (var j = originPoint.z + 25; j < originPoint.z + mesh.scale.z; j += 50) {
          var vector: THREE.Vector2 = new THREE.Vector2(i, j);
          points.push(vector);
        }
      }
    }
    if (originPoint.x > originPoint.x + mesh.scale.x && originPoint.z < originPoint.z + mesh.scale.z) {
      for (var i = originPoint.x + mesh.scale.x + 25; i < originPoint.x; i += 50) {
        for (var j = originPoint.z + 25; j < originPoint.z + mesh.scale.z; j += 50) {
          var vector: THREE.Vector2 = new THREE.Vector2(i, j);
          points.push(vector);
        }
      }
    }
    if (originPoint.x < originPoint.x + mesh.scale.x && originPoint.z > originPoint.z + mesh.scale.z) {
      for (var i = originPoint.x + 25; i < originPoint.x + mesh.scale.x; i += 50) {
        for (var j = originPoint.z + mesh.scale.z + 25; j < originPoint.z; j += 50) {
          var vector: THREE.Vector2 = new THREE.Vector2(i, j);
          points.push(vector);
        }
      }
    }
    if (originPoint.x > originPoint.x + mesh.scale.x && originPoint.z > originPoint.z + mesh.scale.z) {
      for (var i = originPoint.x + mesh.scale.x + 25; i < originPoint.x; i += 50) {
        for (var j = originPoint.z + mesh.scale.z + 25; j < originPoint.z; j += 50) {
          var vector: THREE.Vector2 = new THREE.Vector2(i, j);
          points.push(vector);
        }
      }
    }

    return points;
  }
}
