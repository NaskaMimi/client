import {Drawings} from "./walls/wallsUtils/drawings";
import {HintController} from "./context/hintController";

export class AbstractFacade {
    isValid = false;

    protected isObjectValid():boolean{
        return true;
    }

    protected validateObject(){
        this.isValid = this.isObjectValid();
        if (this.isValid) {
            Drawings.markObjectsAsValid(this.getObjects());
        } else {
            Drawings.markObjectsAsInvalid(this.getObjects());
        }
        HintController.showHint();
        HintController.setHintText(this.getHintText());
    }

    protected getObjects(){
        return null;
    }

    protected getHintText(){
        return null;
    }
}