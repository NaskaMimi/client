import {Object3D} from "./object3D";
import {WallsPoints} from "../walls/wallsUtils/wallsPoints";
import {Wall} from "../walls/elements/wall";
import * as THREE from 'three';
import {Loader3DObjects} from "./loader3DObjects";
import {PlanViewEnvironment} from "../context/planViewEnvironment";
import {PlanViewContext} from "../context/planViewContext";
import {Drawings} from "../walls/wallsUtils/drawings";
import {MoveObject, ShowObjectInfo} from "../store/actions/rightpanel.actions";
import {PlanViewSettings} from "../configuration/planViewSettings";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../store/state/app.state";
import {Observable} from "rxjs";
import {moveObject, rotateObjectClockwise, rotateObjectCounterClockwise} from "../store/selectors/rightpanel.selectors";
import {Model3DDto} from "../dtos/model3D.dto";
import {AbstractFacade} from "../abstractFacade";
import {HintController} from "../context/hintController";
import {Calculation} from "../utils/calculation";

// var draggableObject = null;
// var draggableObjectInfo: Object3D = null;

// var objectsMap: Map<Object, Object3D>;

export class ObjectsFacade extends AbstractFacade {

    raycaster: THREE.Raycaster;
    highlightedObject = null;
    selectedObject = null;
    _store: Store<IAppState>;

    protected draggableObject = null;
    protected draggableObjectInfo: Object3D = null;


    constructor(raycaster2: THREE.Raycaster, store: Store<IAppState>) {
        super();
        this.raycaster = raycaster2;
        this._store = store;
        // objectsMap = new Map<Object, Object3D>();

        var moveObserver: Observable<boolean> = this._store.pipe(select(moveObject));
        moveObserver.subscribe((wallpaper => {
            if (this.selectedObject != null) {
                this.draggableObject = this.selectedObject.parent;
                this.draggableObjectInfo = PlanViewContext.getInstance().models3DInformation.get(this.draggableObject);
                this._store.dispatch(new MoveObject(false));
            }
        }));

        var rotateClockwiseObserver: Observable<number> = this._store.pipe(select(rotateObjectClockwise));
        rotateClockwiseObserver.subscribe((rotation => {
            if (this.selectedObject != null) {
                this.rotateObject(rotation, this.selectedObject);
                this._store.dispatch(new ShowObjectInfo(new Model3DDto(false, this.selectedObject.rotation.y)));
            }
        }));

        var rotateCounterClockwiseObserver: Observable<number> = this._store.pipe(select(rotateObjectCounterClockwise));
        rotateCounterClockwiseObserver.subscribe((rotation => {
            if (this.selectedObject != null) {
                this.rotateObject(rotation, this.selectedObject);
                this._store.dispatch(new ShowObjectInfo(new Model3DDto(false, this.selectedObject.rotation.y)));
            }
        }));

    }

    public dragObject(intersect) {
        if (this.draggableObject != null) {
            this.draggableObject.position.copy(intersect.point).add(intersect.face.normal);
            if (this.draggableObjectInfo.type == "Building") {
                // this.snapObject();
                this.rotateObjectByWallAxis();
            }

            this.draggableObject.position.divideScalar(50).floor().multiplyScalar(50).addScalar(0);
            this.draggableObject.position.y = this.draggableObjectInfo.yPosition + PlanViewContext.getInstance().yPosition;
            this.validateObject();
        }
    }

    public putObjectOnView() {
        if (this.draggableObject != null) {
            if (this.isValid) {
                this.draggableObject = null;
                this.draggableObjectInfo = null;
            }
        }
    }

    public createObjects(window: Object3D) {
        this.cancelDraggableObject();
        new Loader3DObjects(PlanViewEnvironment.getInstance().scene, window).model3D.subscribe((model =>{
                this.draggableObject = model;
                this.draggableObjectInfo = window;

                PlanViewContext.getInstance().models3DInformation.set(this.draggableObject, this.draggableObjectInfo);
            }
        ));
    }

    public rotateClockwise() {
        console.log("74674");
        if(this.draggableObject){
            this.rotateObject(this.draggableObject.rotation.y - (Math.PI / 2), this.draggableObject);
        }
    }

    public rotateCounterClockwise() {
        if(this.draggableObject){
            this.rotateObject(this.draggableObject.rotation.y + (Math.PI / 2), this.draggableObject);
        }
    }

    protected isObjectValid(): boolean {
        return this.is3DObjectValid();
    }

    protected getObjects() {
        return [this.draggableObject];
    }

    protected getHintText() {
        return "";
    }

    private snapObject() {

        this.draggableObject.position.divideScalar(25).floor().multiplyScalar(25).addScalar(25);

        var nearWallX;
        var nearWallZ;
        let walls = PlanViewContext.getInstance().getWalls();
        for (var i = 0; i < walls.length; i++) {
            var wall = walls[i];

            if (WallsPoints.getNumberByModule(wall.position.x - this.draggableObject.position.x) < 60) {
                nearWallX = wall;
            }
            if (WallsPoints.getNumberByModule(wall.position.z - this.draggableObject.position.z) < 60) {
                nearWallZ = wall;
            }
        }

        if (nearWallX != null) {
            this.draggableObject.position.x = nearWallX.position.x;
            this.draggableObject.position.x -= this.draggableObject.scale.z - PlanViewSettings.WALL_SIZE / 2 - 9;
        }
        if (nearWallZ != null) {
            this.draggableObject.position.z = nearWallZ.position.z;
            this.draggableObject.position.z -= this.draggableObject.scale.z - PlanViewSettings.WALL_SIZE / 2 - 9;
        }
    }

    private rotateObjectByWallAxis() {
        if (this.is3DObjectValid()) {
            const intersectsWalls = this.raycaster.intersectObjects(PlanViewContext.getInstance().getWalls());
            if (intersectsWalls.length > 0) {
                let intersectsWall: THREE.Mesh = intersectsWalls[0].object;

                var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(intersectsWall);

                if (wallClass != null) {
                    if (wallClass.wallAxis == "x") {
                        if (wallClass.paintedWallsNumbers[0] == 1) {
                            this.draggableObject.rotation.y = 0; //0
                        } else {
                            this.draggableObject.rotation.y = Math.PI; //180
                        }
                    } else {
                        if (wallClass.paintedWallsNumbers[0] == 5) {
                            this.draggableObject.rotation.y = Math.PI / 2; //90
                        } else {
                            this.draggableObject.rotation.y = Math.PI * 1.5; //270
                        }
                    }
                }
            }
        }
    }

    highlightObject() {
        this.highlightedObject = Drawings.highlightObject(PlanViewContext.getInstance().getModels3D(), this.raycaster, "3dModel");
    }

    public selectObject() {

        if (this.highlightedObject != null) {
            Drawings.selectObject(this.highlightedObject);
            this.selectedObject = this.highlightedObject;
            this._store.dispatch(new ShowObjectInfo(new Model3DDto(false, this.selectedObject.rotation.y)));
        } else {
            Drawings.unselectObjects();
            this.selectedObject = null;
            this._store.dispatch(new ShowObjectInfo(null));
        }

    }

    public cancelDraggableObject() {
        this.deleteObject(this.draggableObject);
    }

    public deleteSelectedObject() {
        this.deleteObject(this.selectedObject);
    }

    showAndHideObjectsAccordingToLevel() {
        this.showAllObjectsOnAllLevels();
        this.hideObjectssUpperCurrentLevel();
    }

    private deleteObject(objectToDelete) {
        if (objectToDelete) {
            PlanViewEnvironment.getInstance().scene.remove(objectToDelete);
            PlanViewContext.getInstance().deleteModel3D(objectToDelete);
            this.draggableObject = null;
            this.draggableObjectInfo = null;
            HintController.deleteAllWarnings();
            HintController.setHintText("");
            PlanViewEnvironment.getInstance().render();
        }
    }

    private showAllObjectsOnAllLevels() {
        for (var level = 1; level < 4; level++) {
            var objects: THREE.Mesh[] = PlanViewContext.getInstance().getModels3DByLevel(level);
            for (let object of objects) {
                object.visible = true;
            }
        }
    }

    private hideObjectssUpperCurrentLevel() {
        for (var level = PlanViewContext.getInstance().currentLevel + 1; level < 4; level++) {
            var objects: THREE.Mesh[] = PlanViewContext.getInstance().getModels3DByLevel(level);
            for (let object of objects) {
                object.visible = false;
            }
        }
    }

    private is3DObjectValid(): boolean {
        if (this.draggableObjectInfo.type == "Building") {

            let intersectWall: THREE.Mesh = Calculation.findIntersectWallsByObject(this.draggableObject);
            HintController.defineWarnings(!intersectWall, HintController.OBJECT_DOESNT_INTERSECT_WALL);
            if (intersectWall != null) {

                var wallClass: Wall = PlanViewContext.getInstance().wallsManager.findWallClassByWall(intersectWall);

                if (wallClass != null) {
                    var isIntersectInnerWalls = wallClass.paintedWallsNumbers.length <= 1;
                    HintController.defineWarnings(!isIntersectInnerWalls, HintController.OBJECT_INTERSECT_INNER_WALL);
                    return isIntersectInnerWalls;
                }
            } else {
                return false;
            }

        } else {
            var modelsArray = PlanViewContext.getInstance().getModels3D().map((x) => x);
            modelsArray.splice(modelsArray.indexOf(this.draggableObject), 1);

            var isIntersectOtherObjects = Calculation.isAtLeastOneVertexOf3dModelIntersectBox(this.draggableObject, modelsArray) || Calculation.isObjectIntersectOtherWalls(this.draggableObject);
            HintController.defineWarnings(isIntersectOtherObjects, HintController.OBJECT_INTERSECT_OTHER_OBJECT);

            var isObjectValid;
            if (PlanViewContext.getInstance().currentLevel == 1) {
                let isObjectIntersectPorches = Calculation.isAllVerticesOf3dModelIntersectBox(this.draggableObject, PlanViewContext.getInstance().getPorches());
                HintController.defineWarnings(!isObjectIntersectPorches, HintController.OBJECT_IS_OUT_OF_HOUSE);
                isObjectValid = isObjectIntersectPorches && !isIntersectOtherObjects;
            } else {
                let isObjectIntersectBalcony = Calculation.isAllVerticesOf3dModelIntersectBox(this.draggableObject, PlanViewContext.getInstance().getBalconies());
                let isObjectIntersectRoom = Calculation.isAllVerticesOf3dModelIntersectFloor(this.draggableObject, PlanViewContext.getInstance().getFloors());
                let isObjectIntersectInnerSpace: boolean = isObjectIntersectRoom || isObjectIntersectBalcony;
                HintController.defineWarnings(!isObjectIntersectInnerSpace, HintController.OBJECT_IS_OUT_OF_HOUSE);
                isObjectValid = isObjectIntersectInnerSpace && !isIntersectOtherObjects;
            }
            return isObjectValid;
        }
        return false;
    }

    private rotateObject(rotation: number, object) {
        console.log(rotation);
        object.rotation.y = rotation;
        var objectInfo = PlanViewContext.getInstance().models3DInformation.get(object);
        var newScaleX = objectInfo.realScaleY.valueOf();
        var newScaleY = objectInfo.realScaleX.valueOf();
        objectInfo.realScaleX = newScaleX;
        objectInfo.realScaleY = newScaleY;
        PlanViewEnvironment.getInstance().render();
    }
}
