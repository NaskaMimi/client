import {Object3D} from "./object3D";
import {PlanViewSettings} from "../configuration/planViewSettings";
import {Materials} from "../walls/materials";

export class Objects {

  public static wallpapers = [
    {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper1.jpg", name: "Бетон"},
    {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper2.jpg", name: "Краска"},
    {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper3.jpg", name: "Штукатурка"}
  ];

  public static roofs = [
    {link: PlanViewSettings.URL_PREFIX + "assets/roofs/roof1.jpg", name: "Черная"},
    {link: PlanViewSettings.URL_PREFIX + "assets/roofs/roof2.jpg", name: "Бурая"}
  ];

  public static floors = [
    {link: PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg", name: "Ламинат"},
    {link: PlanViewSettings.URL_PREFIX + "assets/floors/floor2.jpg", name: "Паркет"},
    {link: PlanViewSettings.URL_PREFIX + "assets/floors/floor3.jpg", name: "Плитка"}
  ];

  public static windowsArray: Object3D[] = [
    {
      name: "window1",
      scaleX: 5,
      scaleY: 5,
      scaleZ: 5,
      rotation: 0,
      yPosition: 90,
      type: "Building",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: []
    },
    {
      name: "window2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: Math.PI / 2,
      yPosition: 50,
      type: "Building",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.wood_textures
    }
  ];

  public static doorsArray: Object3D[] = [
    {
      name: "door1",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Building",
      realScaleX: 50,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.wood_textures
    },
    {
      name: "door2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Building",
      realScaleX: 50,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.wood_textures
    }
  ];

  public static outsideDoorsArray: Object3D[] = [
    {
      name: "window1",
      scaleX: 5,
      scaleY: 5,
      scaleZ: 5,
      rotation: 0,
      yPosition: 90,
      type: "Building",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: []
    },
    {
      name: "window_big",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: Math.PI / 2,
      yPosition: 20,
      type: "Building",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: []
    }
  ];

  public static stairsArray: Object3D[] = [
    {
      name: "stair1",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: Math.PI / 2,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.non_rotated_wood_textures
    },
    {
      name: "stair2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: Math.PI / 2,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.non_rotated_wood_textures
    }
  ];

  public static columnsArray: Object3D[] = [
    {
      name: "column1",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: Math.PI / 2,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.wood_textures
    },
    {
      name: "column2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: Math.PI / 2,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.non_rotated_wood_textures
    }
  ];

  public static outsideStairsArray: Object3D[] = [
    {
      name: "outsidestair1",
      scaleX: 50,
      scaleY: 76,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Stair",
      realScaleX: 98,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.outside_stair1_textures
    }
  ];
  public static bedsArray: Object3D[] = [
    {
      name: "bed1",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 198,
      selectedIndex: 0,
      textures: Materials.bed_triple_textures
    },
    {
      name: "bed2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: Math.PI / 2,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.bed_triple_textures
    },
    {
      name: "bed4",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: Math.PI / 2,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 198,
      selectedIndex: 0,
      textures: Materials.bed_triple_textures
    },
    {
      name: "bed3",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: Math.PI / 2,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 198,
      selectedIndex: 0,
      textures: Materials.bed_triple_textures
    }
  ];

  public static coffeetablesArray: Object3D[] = [
    {
      name: "little-table1",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 50,
      realScaleY: 50,
      selectedIndex: 0,
      textures: Materials.metall_textures
    },
    {
      name: "little-table2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 50,
      realScaleY: 50,
      selectedIndex: 0,
      textures: Materials.metall_glass_textures
    },
    {
      name: "little-table3",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    },
    {
      name: "little-table4",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.metall_textures
    },
    {
      name: "little-table5",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 98,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.metall_textures
    }
  ];

  public static sofasArray: Object3D[] = [
    {
      name: "sofa1",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    },
    {
      name: "sofa2",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 98,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    },
    {
      name: "sofa3",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 20,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.sofas_triple_textures
    },
    {
      name: "sofa4",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    },
    {
      name: "sofa5",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.sofas_triple_textures
    },
    {
      name: "sofa6",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    },
    {
      name: "sofa7",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 98,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    }

  ];

  public static tablesArray: Object3D[] = [
    {
      name: "table1",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.metall_glass_textures
    },
    {
      name: "table2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.wood_textures
    },
    {
      name: "table3",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 248,
      realScaleY: 198,
      selectedIndex: 0,
      textures: Materials.metall_glass_textures
    }
  ];

  public static chairsArray: Object3D[] = [
    {
      name: "chair1",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.metall_glass_textures
    },
    {
      name: "chair2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    },
    {
      name: "chair3",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    }
  ];

  public static bottomKitchensArray: Object3D[] = [
    {
      name: "kitchen5",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.top_kitchen_textures
    },
    {
      name: "kitchen7",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.bottom_kitchen_textures
    },
    {
      name: "kitchen8",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 98,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.bottom_kitchen_textures
    },
    {
      name: "kitchen9",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.bottom_kitchen_textures
    },
    {
      name: "kitchen10",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.bottom_kitchen_textures
    },
    {
      name: "kitchen11",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.bottom_kitchen_textures
    }
  ];

  public static topKitchensArray: Object3D[] = [
    {
      name: "kitchen12",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 150,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.top_kitchen_textures
    },
    {
      name: "kitchen13",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 150,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.top_kitchen_textures
    },
    {
      name: "kitchen14",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 150,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.top_kitchen_textures
    },
    {
      name: "kitchen15",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 150,
      type: "Furniture",
      realScaleX: 48,
      realScaleY: 48,
      selectedIndex: 0,
      textures: Materials.top_kitchen_textures
    }
  ];

  public static fireplacesArray: Object3D[] = [
    {
      name: "fireplace1",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 150,
      realScaleY: 50,
      selectedIndex: 0,
      textures: Materials.fireplace_textures
    },
    {
      name: "fireplace2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 150,
      realScaleY: 50,
      selectedIndex: 0,
      textures: Materials.fireplace2_textures
    },
    // {name: "fireplace3", scaleX: 100, scaleY: 100, scaleZ: 50, rotation: 0, yPosition: 0, type: "Furniture", realScaleX: 150, realScaleY: 50, selectedIndex: 0, textures: Materials.fireplace3_textures},
    {
      name: "fireplace4",
      scaleX: 100,
      scaleY: 70,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 148,
      realScaleY: 198,
      selectedIndex: 0,
      textures: Materials.fireplace4_textures
    }
  ];
  public static nightstandsArray: Object3D[] = [
    {
      name: "nightstand1",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: 0,
      yPosition: 20,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: []
    },
    {
      name: "nightstand2",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: Math.PI / 2,
      yPosition: 20,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: []
    },
    {
      name: "nightstand3",
      scaleX: 100,
      scaleY: 100,
      scaleZ: 100,
      rotation: Math.PI / 2,
      yPosition: 20,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 0,
      selectedIndex: 0,
      textures: []
    }
  ];
  public static tvsArray: Object3D[] = [];
  public static wardrobesArray: Object3D[] = [];
  public static desksArray: Object3D[] = [];
  public static plumbingsArray: Object3D[] = [
    {
      name: "bath",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 198,
      selectedIndex: 0,
      textures: []
    },
    {
      name: "bath2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 198,
      realScaleY: 148,
      selectedIndex: 0,
      textures: []
    }
  ];
  public static bathroomFurnitureArray: Object3D[] = [
    {
      name: "mirror1",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 98,
      realScaleY: 198,
      selectedIndex: 0,
      textures: Materials.mirror1_textures
    },
    {
      name: "mirror2",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 98,
      realScaleY: 198,
      selectedIndex: 0,
      textures: Materials.mirror2_textures
    },
    {
      name: "wardrobe1",
      scaleX: 50,
      scaleY: 50,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 98,
      realScaleY: 198,
      selectedIndex: 0,
      textures: Materials.wood_textures
    }
  ];
  public static decorsArray: Object3D[] = [
    {
      name: "curtains",
      scaleX: 100,
      scaleY: 90,
      scaleZ: 100,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 148,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.curtains_double_textures
    },
    {
      name: "decor1",
      scaleX: 50,
      scaleY: 39,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 148,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    },
    {
      name: "decor2",
      scaleX: 50,
      scaleY: 39,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 148,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    },
    {
      name: "decor3",
      scaleX: 50,
      scaleY: 60,
      scaleZ: 50,
      rotation: 0,
      yPosition: 0,
      type: "Furniture",
      realScaleX: 148,
      realScaleY: 0,
      selectedIndex: 0,
      textures: Materials.sofas_solid_textures
    }
  ];

  static railingsArray: Object3D[] = [
    {
      name: "railing1",
      scaleX: 30,
      scaleY: 20,
      scaleZ: 50,
      rotation: 0,
      yPosition: 20,
      type: "Furniture",
      realScaleX: 0,
      realScaleY: 50,
      selectedIndex: 0,
      textures: Materials.wood_textures
    }
  ];

  public static allObjects = [Objects.windowsArray,
    Objects.doorsArray, Objects.outsideDoorsArray, Objects.stairsArray, Objects.columnsArray,
    Objects.outsideStairsArray, Objects.bedsArray, Objects.coffeetablesArray, Objects.sofasArray,
    Objects.tablesArray, Objects.chairsArray, Objects.bottomKitchensArray, Objects.topKitchensArray,
    Objects.fireplacesArray, Objects.nightstandsArray, Objects.tvsArray, Objects.wardrobesArray,
    Objects.desksArray, Objects.plumbingsArray, Objects.bathroomFurnitureArray, Objects.decorsArray,
    Objects.railingsArray
  ];

  public static findObjectByName(name:string):Object3D{
    for(var array of Objects.allObjects){
      for(var object of array){
        if(object.name == name){
          return object;
        }
      }
    }
    return null;
  }
}
