import * as THREE from 'three';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader.js';
import { DDSLoader } from 'three/examples/jsm/loaders/DDSLoader.js';
import {Materials} from "../walls/materials";
import {Observable, of} from "rxjs";
import {Object3D} from "./object3D";
import {PlanViewContext} from "../context/planViewContext";
import {PlanViewSettings} from "../configuration/planViewSettings";
import {Drawings} from "../walls/wallsUtils/drawings";

const path_textures = PlanViewSettings.URL_PREFIX + 'assets/textures/';
const path_models = PlanViewSettings.URL_PREFIX + 'assets/models/';
const onProgress = function ( xhr ) {

    if ( xhr.lengthComputable ) {

        const percentComplete = xhr.loaded / xhr.total * 100;
        console.log( Math.round( percentComplete ) + '% downloaded' );

    }

};
const onError = function () { };

const manager = new THREE.LoadingManager();
var object3D = null;
var scene2;
var object2:Object3D;

export class Loader3DObjects{

    public model3D;

    constructor(public scene:THREE.Scene,
                public object:Object3D) {

        manager.addHandler( /\.dds$/i, new DDSLoader() );
        scene2 = scene;
        object2 = object;

        this.model3D = new Observable(this.sequenceSubscriber);

    }

    sequenceSubscriber(observer){
        new MTLLoader( manager )
            .setPath( path_textures )
            .load( object2.name + '.mtl', function ( materials ) {

                materials.preload();

                new OBJLoader( manager )
                    .setMaterials( materials )
                    .setPath( path_models )
                    .load( object2.name + '.obj', function ( object ) {

                        object.scale.set( object2.scaleX, object2.scaleY, object2.scaleZ );
                        object.rotation.y = object2.rotation;
                        object.name = object2.name;

                        if (object2.type == "Building") {
                            object.scale.z = 30 + PlanViewSettings.WALL_SIZE;
                        }

                        if (object2.textures.length > 0) {
                            var textures = object2.textures[object2.selectedIndex].textures;
                            for (var i = 0; i < textures.length; i++) {
                                // object.children[i].material = Drawings.createMeshMaterial(new THREE.TextureLoader(), 0xffffff, PlanViewSettings.URL_PREFIX + "assets/materials/" + textures[i] + ".jpg", -Math.PI / 2, 6, 3);
                                object.children[i].material = textures[i];
                                // object.children[i].material = Materials.glass_material;
                            }
                        }
                        scene2.add( object );
                        if(object2.type == "Stair"){
                          PlanViewContext.getInstance().addStair(object);
                        } else {
                          PlanViewContext.getInstance().addModel3D(object);
                        }
                        object3D = object;
                        observer.next(object);
                        observer.complete();

                    }, onProgress, onError );

            } );
    }

}
