import * as THREE from 'three';

export class TexturesElement{
    constructor(public color:string,
                public textures:THREE.Material[]){

    }
}