import {TexturesElement} from "./texturesElement";

export class Object3D{
    constructor(public name:string,
                public scaleX:number,
                public scaleY:number,
                public scaleZ:number,
                public rotation:number,
                public yPosition:number,
                public type:string,
                public realScaleX:number,
                public realScaleY:number,
                public selectedIndex:number,
                public textures:TexturesElement[]) {

    }
}