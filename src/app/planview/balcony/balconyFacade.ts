import {Drawings} from "../walls/wallsUtils/drawings";
import {PlanViewContext} from "../context/planViewContext";
import * as THREE from 'three';
import {PlanViewSettings} from "../configuration/planViewSettings";
import {PlanViewEnvironment} from "../context/planViewEnvironment";
import {ShowBalconyInfo} from "../store/actions/rightpanel.actions";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../store/state/app.state";
import {BalconyDto} from "../dtos/balcony.dto";
import {Observable} from "rxjs";
import {changeBalconyFloor} from "../store/selectors/rightpanel.selectors";
import {Calculation} from "../utils/calculation";
import {AbstractFacade} from "../abstractFacade";
import {HintController} from "../context/hintController";
import {Materials} from "../walls/materials";

export class BalconyFacade extends AbstractFacade {

    currentBalcony: THREE.Mesh;
    highlightedBalcony: THREE.Mesh;
    selectedBalcony: THREE.Mesh;
    raycaster: THREE.Raycaster;
    selectedBalconyTexture: string;
    balconyTextures: Map<THREE.Mesh, string>;
    startPoint: THREE.Vector3;
    isValid: boolean = false;

    _store: Store<IAppState>;

    constructor(raycaster: THREE.Raycaster, store: Store<IAppState>) {
        super();
        this.raycaster = raycaster;
        this._store = store;
        this.balconyTextures = new Map();

        var balconyObserver: Observable<string> = this._store.pipe(select(changeBalconyFloor));
        balconyObserver.subscribe((floor => {
            if (this.selectedBalcony != null) {
                this.selectedBalconyTexture = floor;
                this.selectedBalcony.material = Materials.createMixedMaterial(floor);

                this.balconyTextures.delete(this.selectedBalcony);
                this.balconyTextures.set(this.selectedBalcony, this.selectedBalconyTexture);
            }
        }));
    }

    public createBalcony(intersect) {
        this.startPoint = PlanViewEnvironment.getInstance().rollOverMesh.position.clone();

        var mixedMaterials = Materials.createMixedMaterial(PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg");

        var balconyGeometry = new THREE.BoxGeometry(PlanViewSettings.WALL_INITIAL_SIZE, 30, PlanViewSettings.WALL_INITIAL_SIZE);
        balconyGeometry.translate(PlanViewSettings.WALL_INITIAL_SIZE / 2, 0, PlanViewSettings.WALL_INITIAL_SIZE / 2);

        var balcony = new THREE.Mesh(balconyGeometry, mixedMaterials);
        balcony.castShadow = true;
        balcony.receiveShadow = true;
        balcony.position.copy(intersect.point);
        balcony.position.divideScalar(50).floor().multiplyScalar(50).addScalar(0);
        balcony.position.y = PlanViewContext.getInstance().yPosition;

        this.currentBalcony = balcony;
        PlanViewEnvironment.getInstance().scene.add(balcony);
        PlanViewContext.getInstance().addBalcony(balcony);
        this.balconyTextures.set(balcony, PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg");
    }

    public drawBalcony() {
        if (this.currentBalcony != null) {
            let rollOverMesh = PlanViewEnvironment.getInstance().rollOverMesh;
            this.currentBalcony.scale.x = rollOverMesh.position.x - this.currentBalcony.position.x;
            this.currentBalcony.scale.z = rollOverMesh.position.z - this.currentBalcony.position.z;

            this.validateObject();
        }
    }

    public stopDrawBalcony() {
        if (this.currentBalcony != null) {
            if (this.isValid) {
                this.currentBalcony.material = Materials.createMixedMaterial(PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg", this.currentBalcony.scale.x / 500, this.currentBalcony.scale.z / 500);
                Drawings.unmarkObjects();
            } else {
                this.selectedBalcony = this.currentBalcony;
                this.deleteSelectedBalcony();
            }
            this.currentBalcony = null;
        }
    }

    public showAndHideBalconiesAccordingToLevel() {
        this.showAllBalconiesOnAllLevels();
        this.hideBalconiesUpperCurrentLevel();
    }

    public highlightBalcony() {
        this.highlightedBalcony = Drawings.highlightObject(PlanViewContext.getInstance().getBalconies(), this.raycaster);
    }

    public selectBalcony() {
        this.selectedBalcony = this.highlightedBalcony;

        if (this.selectedBalcony != null) {
            Drawings.selectObject(this.selectedBalcony);
            this._store.dispatch(new ShowBalconyInfo(new BalconyDto(Calculation.calculateSquare(this.selectedBalcony), Calculation.calculatePerimeter(this.selectedBalcony), this.balconyTextures.get(this.selectedBalcony))));
        } else {
            Drawings.unselectObjects();
            this._store.dispatch(new ShowBalconyInfo(null));
        }

    }

    public deleteSelectedBalcony() {
        PlanViewEnvironment.getInstance().scene.remove(this.selectedBalcony);
        PlanViewContext.getInstance().deleteBalcony(this.selectedBalcony);

        this.highlightedBalcony = null;
        this.selectedBalcony = null;
        Drawings.unselectObjects();
        this._store.dispatch(new ShowBalconyInfo(null));
    }

    protected isObjectValid(): boolean {
        return this.isBalconyValid();
    }

    protected getObjects() {
        return [this.currentBalcony];
    }

    protected getHintText() {
        return "Площадь балкона = " + (this.currentBalcony.scale.x * this.currentBalcony.scale.z) / 10000 + " м";
    }

    private showAllBalconiesOnAllLevels() {
        for (var level = 2; level < 4; level++) {
            var objects: THREE.Mesh[] = PlanViewContext.getInstance().getBalconyByLevel(level);
            console.log("objects = " + objects);
            for (let object of objects) {
                object.visible = true;
            }
        }
    }

    private hideBalconiesUpperCurrentLevel() {
        for (var level = PlanViewContext.getInstance().currentLevel + 1; level < 4; level++) {
            var objects: THREE.Mesh[] = PlanViewContext.getInstance().getBalconyByLevel(level);
            for (let object of objects) {
                object.visible = false;
            }
        }
    }

    private isBalconyValid(): boolean {
        var balconyArray = PlanViewContext.getInstance().getBalconies().map((x) => x);
        balconyArray.splice(balconyArray.indexOf(this.currentBalcony), 1);

        let isBalconyIntersectWalls = !Calculation.isBoxIntersectFloor(this.currentBalcony, PlanViewContext.getInstance().getFloors());
        let isBalconyIntersectsOtherBalconies = !Calculation.isAtLeastOneVertexOfBoxIntersectOtherBox(this.currentBalcony, balconyArray);

        HintController.defineWarnings(!isBalconyIntersectWalls, HintController.BALCONY_INTERSECT_WALL);
        HintController.defineWarnings(!isBalconyIntersectsOtherBalconies, HintController.BALCONY_INTERSECT_OTHER_BALCONY);

        return isBalconyIntersectWalls && isBalconyIntersectsOtherBalconies;
    }
}
