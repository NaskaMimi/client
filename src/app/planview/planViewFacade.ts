import {Object3D} from "./objects3D/object3D";
import * as THREE from 'three';
import {ObjectsFacade} from "./objects3D/objectsFacade";
import {WallFacade} from "./walls/wallFacade";
import {PlanViewContext} from "./context/planViewContext";
import {PlanViewEnvironment} from "./context/planViewEnvironment";
import {Store} from "@ngrx/store";
import {IAppState} from "./store/state/app.state";
import {ChangeRoomName} from "./store/actions/rightpanel.actions";
import {PlanViewSettings} from "./configuration/planViewSettings";
import {RoofFacade} from "./roof/roofFacade";
import {Injectable} from "@angular/core";
import {PorchFacade} from "./porch/porchFacade";
import {BalconyFacade} from "./balcony/balconyFacade";
import {RailingFacade} from "./railing/railingFacade";
import {HintController} from "./context/hintController";
import {PlanViewRouter} from "./context/planViewRouter";
import {StairFacade} from "./stair/stairFacade";


var raycaster = new THREE.Raycaster();
var pointer = new THREE.Vector2();

var viewModeOn: boolean = true;

var needDraw = false;

@Injectable({
    providedIn: 'root',
})

export class PlanViewFacade {

    private ROOM_MODE: string = "roomModeOn";
    private MODELS3D_MODE: string = "models3DModeOn";
    private SINGLE_WALL_MODE: string = "singleWallModeOn";
    private PORCH_MODE: string = "porchModeOn";
    private ROOF_MODE: string = "roofModeOn";
    private BALCONY_MODE: string = "balconyModeOn";
    private RAILING_MODE: string = "railingModeOn";

    objectsFacade: ObjectsFacade;
    stairsFacade: StairFacade;
    wallFacade: WallFacade;
    roofFacade: RoofFacade;
    porchFacade: PorchFacade;
    balconyFacade: BalconyFacade;
    railingFacade: RailingFacade;
    store: Store<IAppState>;

    currentPoint = null;

    currentMode: string = this.SINGLE_WALL_MODE;
    buildingModeOn: boolean = false;

    constructor(private _store: Store<IAppState>) {

        this.store = _store;
        this.objectsFacade = new ObjectsFacade(raycaster, this.store);
        this.stairsFacade = new StairFacade(raycaster, this.store);
        this.wallFacade = new WallFacade(raycaster, pointer, this.store);
        this.roofFacade = new RoofFacade(raycaster, this.store);
        this.porchFacade = new PorchFacade(raycaster, this.store);
        this.balconyFacade = new BalconyFacade(raycaster, this.store);
        this.railingFacade = new RailingFacade();
    }

    public onPointerMove(event) {

        let canvasBounds = PlanViewEnvironment.getInstance().renderer.getContext().canvas.getBoundingClientRect();
        var mouseX = ((event.clientX - canvasBounds.left) / (canvasBounds.right - canvasBounds.left)) * 2 - 1;
        var mouseY = -((event.clientY - canvasBounds.top) / (canvasBounds.bottom - canvasBounds.top)) * 2 + 1;

        pointer.set(mouseX, mouseY);

        raycaster.setFromCamera(pointer, PlanViewEnvironment.getInstance().camera);
        const intersects = raycaster.intersectObjects([PlanViewEnvironment.getInstance().getPlane()]);
        const intersect = intersects[0];

        if (intersect) {
            this.currentPoint = intersect.point;
            PlanViewEnvironment.getInstance().rollOverMesh.position.x = intersect.point.x;
            PlanViewEnvironment.getInstance().rollOverMesh.position.z = intersect.point.z;
            PlanViewEnvironment.getInstance().rollOverMesh.position.divideScalar(50).floor().multiplyScalar(50).addScalar(0);
            PlanViewEnvironment.getInstance().rollOverMesh.position.y = PlanViewContext.getInstance().yPosition;
            this.objectsFacade.dragObject(intersect);
            this.stairsFacade.dragObject(intersect);
            this.roofFacade.dragRoof(intersect);

            if (this.currentMode == this.RAILING_MODE) {
                this.railingFacade.dragRailing(intersect);
            }

            this.highlightObject();
            this.drawWallIfNeed();

            PlanViewEnvironment.getInstance().render();
        }

    }

    public onPointerDown(event, callback) {

        this.objectsFacade.putObjectOnView();
        this.stairsFacade.putObjectOnView();
        this.roofFacade.putRoof();

        var startPoint: THREE.Vector3 = new THREE.Vector3(PlanViewEnvironment.getInstance().rollOverMesh.position.x, PlanViewEnvironment.getInstance().rollOverMesh.position.y, PlanViewEnvironment.getInstance().rollOverMesh.position.z);
        this.railingFacade.startDrawRailing(startPoint);
        // this.wallFacade.walls = PlanViewContext.getInstance().getWalls();

        if (this.isBuildingModeOn()) {
            this.startDrawWall(event);
        } else {
            if (viewModeOn) {
                this.selectObject();
                callback();
            }
        }
    }

    public onPointerUp() {
        this.endDrawWall();
        this.railingFacade.stopDrawRailing();
        this.balconyFacade.stopDrawBalcony();
        this.porchFacade.stopDrawPorch();
        this.roofFacade.stopDrawRoof();
        HintController.deleteAllWarnings();

        PlanViewEnvironment.getInstance().render();
    }

    private startDrawWall(event) {
        needDraw = true;

        let canvasBounds = PlanViewEnvironment.getInstance().renderer.getContext().canvas.getBoundingClientRect();
        var mouseX = ((event.clientX - canvasBounds.left) / (canvasBounds.right - canvasBounds.left)) * 2 - 1;
        var mouseY = -((event.clientY - canvasBounds.top) / (canvasBounds.bottom - canvasBounds.top)) * 2 + 1;

        pointer.set(mouseX, mouseY);

        raycaster.setFromCamera(pointer, PlanViewEnvironment.getInstance().camera);
        const intersects = raycaster.intersectObjects([PlanViewEnvironment.getInstance().getPlane()]);

        if (intersects.length > 0) {

            const intersect = intersects[0];
            if (this.currentMode == this.SINGLE_WALL_MODE) {
                this.wallFacade.createSingleWall(intersect);
            }
            if (this.currentMode == this.ROOM_MODE) {
                this.wallFacade.createRoom(intersect);
            }
            if (this.currentMode == this.PORCH_MODE) {
                this.porchFacade.createPorch(intersect);
            }
            if (this.currentMode == this.BALCONY_MODE) {
                this.balconyFacade.createBalcony(intersect);
            }

            PlanViewEnvironment.getInstance().render();

        }
    }

    public viewModeOn() {
        this.roofFacade.showAndHideRoofsAccordingToLevel();
        PlanViewEnvironment.getInstance().rollOverMesh.visible = false;
        viewModeOn = true;
        this.buildingModeOn = false;

        this.roofFacade.unselectRoof();

        PlanViewEnvironment.getInstance().gridHelper.visible = false;
        PlanViewEnvironment.getInstance().render();

    }

    public buildModeOn() {
        this.buildingModeOn = true;

        this.roofFacade.showAndHideRoofsAccordingToLevel();
        this.roofFacade.unselectRoof();

        this.defineYPosition(PlanViewContext.getInstance().currentLevel);
        PlanViewEnvironment.getInstance().rollOverMesh.visible = true;
        PlanViewEnvironment.getInstance().rollOverMesh.position.y = PlanViewContext.getInstance().yPosition;
        viewModeOn = false;

        PlanViewEnvironment.getInstance().gridHelper.visible = true;
        PlanViewEnvironment.getInstance().gridHelper.position.y = PlanViewContext.getInstance().yPosition;
        PlanViewEnvironment.getInstance().render();
    }

    public objectsModeOn() {

        this.roofFacade.unselectRoof();
        this.roofFacade.showAndHideRoofsAccordingToLevel();

        this.defineYPosition(PlanViewContext.getInstance().currentLevel);
        PlanViewEnvironment.getInstance().rollOverMesh.position.y = PlanViewContext.getInstance().yPosition;
        PlanViewEnvironment.getInstance().rollOverMesh.visible = false;
        viewModeOn = false;
        this.buildingModeOn = false;

        PlanViewEnvironment.getInstance().render();
    }

    public porchModeOn() {
        this.buildModeOn();

        this.currentMode = this.PORCH_MODE;

        PlanViewContext.getInstance().yPosition = PlanViewSettings.LEVEL0_POSITION;
        PlanViewEnvironment.getInstance().gridHelper.position.y = PlanViewSettings.LEVEL0_POSITION;
        PlanViewEnvironment.getInstance().rollOverMesh.position.y = PlanViewSettings.LEVEL0_POSITION;
    }

    public porchForViewModeOn() {
        this.currentMode = this.PORCH_MODE;
        this.viewModeOn();
    }

    public roofModeOn() {
        this.buildModeOn();
        this.roofFacade.showAndHideRoofsAccordingToLevel();
        this.currentMode = this.ROOF_MODE;

        this.defineYPosition(PlanViewContext.getInstance().currentLevel + 1);
        PlanViewEnvironment.getInstance().gridHelper.position.y = PlanViewContext.getInstance().yPosition;
        PlanViewEnvironment.getInstance().rollOverMesh.visible = false;
        PlanViewEnvironment.getInstance().render();
    }

    public roofForViewModeOn() {
        this.currentMode = this.ROOF_MODE;
        this.viewModeOn();
    }


    balconyModeOn() {
        this.buildModeOn();
        this.currentMode = this.BALCONY_MODE;
    }

    railingModeOn() {
        this.buildModeOn();
        this.currentMode = this.RAILING_MODE;
    }

    balconyForViewModeOn() {
        this.currentMode = this.BALCONY_MODE;
        this.viewModeOn();
    }

    public singleModeOn() {
        this.currentMode = this.SINGLE_WALL_MODE;
    }

    public roomModeOn() {
        this.currentMode = this.ROOM_MODE;
    }

    public models3DModeOn() {
        this.currentMode = this.MODELS3D_MODE;
    }

    public changeWallsHeightView(viewHeight: number) {
        let walls1 = PlanViewContext.getInstance().getWalls();
        for (var i = 0; i < walls1.length; i++) {
            var wall: THREE.Mesh = walls1[i];
            wall.scale.y = viewHeight;
        }

        PlanViewEnvironment.getInstance().render();
    }

    public createWindow(window: Object3D, type:string = "furniture") {
        if(type == "furniture") {
            this.objectsFacade.createObjects(window);
        } else if(type == "stairs") {
            this.stairsFacade.createObjects(window);
        } else if(type == "railings") {
            // this.stairsFacade.createObjects(window);
            this.drawRailing(window);
        }

        PlanViewEnvironment.getInstance().render();
    }

    public drawRailing(railing: Object3D) {
        if (this.currentMode == this.RAILING_MODE) {
            this.railingFacade.setRailingForDraw(railing);
            PlanViewEnvironment.getInstance().render();
        }
    }

    private endDrawWall() {
        needDraw = false;

        if (this.isBuildingModeOn()) {
            this.wallFacade.endWallsDraw();
        }
    }

    private highlightObject() {
        if (viewModeOn) {
            if (this.currentMode == this.SINGLE_WALL_MODE) {
                this.wallFacade.highlightWall();
            }

            if (this.currentMode == this.ROOM_MODE) {
                this.wallFacade.highlightRoom();
            }

            if (this.currentMode == this.MODELS3D_MODE) {
                this.objectsFacade.highlightObject();
            }

            if (this.currentMode == this.BALCONY_MODE) {
                this.balconyFacade.highlightBalcony();
            }

            if (this.currentMode == this.ROOF_MODE) {
                this.roofFacade.highlightRoof();
            }

            if (this.currentMode == this.PORCH_MODE) {
                this.porchFacade.highlightPorch();
            }
        }
    }

    private selectObject() {
        if (viewModeOn) {
            if (this.currentMode == this.SINGLE_WALL_MODE) {
                this.wallFacade.selectWall();
            }

            if (this.currentMode == this.ROOM_MODE) {
                this.wallFacade.selectRoom();
            }

            if (this.currentMode == this.MODELS3D_MODE) {
                this.objectsFacade.selectObject();
            }

            if (this.currentMode == this.BALCONY_MODE) {
                this.balconyFacade.selectBalcony();
            }

            if (this.currentMode == this.PORCH_MODE) {
                this.porchFacade.selectPorch();
            }

            if (this.currentMode == this.ROOF_MODE) {
                this.roofFacade.selectRoof();
            }
        }
    }

    private drawWallIfNeed() {
        if (needDraw) {
            PlanViewEnvironment.getInstance().rollOverMesh.visible = false;
            if (this.currentMode == this.SINGLE_WALL_MODE) {
                this.wallFacade.drawSingleWall();
            }
            if (this.currentMode == this.ROOM_MODE) {
                this.wallFacade.drawRoom();
            }
            if (this.currentMode == this.PORCH_MODE) {
                this.porchFacade.drawPorch();
            }
            if (this.currentMode == this.BALCONY_MODE) {
                this.balconyFacade.drawBalcony();
            }
        } else if (this.isBuildingModeOn() && this.currentMode != this.ROOF_MODE) {
            PlanViewEnvironment.getInstance().rollOverMesh.visible = true;
        }
    }

    private isBuildingModeOn() {
        return this.buildingModeOn;
    }

    changeRoomName(newName) {
        this._store.dispatch(new ChangeRoomName(newName));
    }

    switchToLevel(number: number) {
        // this.roofFacade.hideRoof();
        // this.wallFacade.walls = PlanViewContext.getInstance().getWalls();
        PlanViewContext.getInstance().currentLevel = number;
        this.defineYPosition(number);
        PlanViewEnvironment.getInstance().gridHelper.position.y = PlanViewContext.getInstance().yPosition;
        PlanViewEnvironment.getInstance().rollOverMesh.position.y = PlanViewContext.getInstance().yPosition;
        // PlanViewEnvironment.getInstance().camera.position.y = PlanViewContext.getInstance().yPosition + 700;
        this.wallFacade.showAndHideWallsAccordingToLevel();
        this.objectsFacade.showAndHideObjectsAccordingToLevel();
        this.balconyFacade.showAndHideBalconiesAccordingToLevel();
        this.roofFacade.showAndHideRoofsAccordingToLevel();
        PlanViewEnvironment.getInstance().render();
    }

    private defineYPosition(number: number) {
        if (number == 1) {
            PlanViewContext.getInstance().yPosition = PlanViewSettings.LEVEL1_POSITION;
        } else if (number == 2) {
            PlanViewContext.getInstance().yPosition = PlanViewSettings.LEVEL2_POSITION;
        } else if (number == 3) {
            PlanViewContext.getInstance().yPosition = PlanViewSettings.LEVEL3_POSITION;
        } else if (number == 4) {
            PlanViewContext.getInstance().yPosition = PlanViewSettings.LEVEL4_POSITION;
        }
    }

    showRoof() {
        // this.roofFacade.showRoof();
        PlanViewEnvironment.getInstance().render();
    }

    addLevel() {
        PlanViewContext.getInstance().levelsCount = PlanViewContext.getInstance().levelsCount + 1;
    }


    deleteWall() {
        this.wallFacade.deleteSelectedWall();
        PlanViewEnvironment.getInstance().render();
        PlanViewRouter.getInstance().navigateToMainInfo();
    }

    deleteRoom() {
        this.wallFacade.deleteSelectedRoom();
        PlanViewEnvironment.getInstance().render();
        PlanViewRouter.getInstance().navigateToMainInfo();
    }

    deleteBalcony() {
        this.balconyFacade.deleteSelectedBalcony();
        PlanViewEnvironment.getInstance().render();
        PlanViewRouter.getInstance().navigateToMainInfo();
    }

    deletePorch() {
        this.porchFacade.deleteSelectedPorch();
        PlanViewEnvironment.getInstance().render();
        PlanViewRouter.getInstance().navigateToMainInfo();
    }

    deleteRoof() {
        this.roofFacade.deleteSelectedRoof();
        PlanViewEnvironment.getInstance().render();
        PlanViewRouter.getInstance().navigateToMainInfo();
    }

    createRoof(roof: string) {
        this.roofFacade.createRoof(roof);
    }

    cancelAllDraggableObjects() {
        this.objectsFacade.cancelDraggableObject();
        this.roofFacade.cancelDraggableObject();
        this.stairsFacade.cancelDraggableObject();
    }

    deleteSelectedObject(){
        this.objectsFacade.deleteSelectedObject();
    }

    changeRoofVisibility(visibility: boolean) {
        this.roofFacade.changeRoofVisibility(visibility);
        PlanViewEnvironment.getInstance().render();
    }

    rotateClockwise() {
        this.objectsFacade.rotateClockwise();
    }

    rotateCounterClockwise() {
        this.objectsFacade.rotateCounterClockwise();
    }
}
