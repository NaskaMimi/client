import * as THREE from 'three';

export class LogHelper {
    static logVertex(currentWallVertex1: THREE.Vector2) {
        console.log("vertex = " + currentWallVertex1.x + " " + currentWallVertex1.y);
    }

    static logVertex3(currentWallVertex1: THREE.Vector2) {
        console.log("vertex = " + currentWallVertex1.x + " " + currentWallVertex1.y + " " + currentWallVertex1.z);
    }

    static logVerteces(verteces: THREE.Vector2[]) {
        for (var i = 0; i < verteces.length; i++) {
            LogHelper.logVertex(verteces[i]);
        }
    }

    static logVerteces3(verteces: THREE.Vector3[]) {
        for (var i = 0; i < verteces.length; i++) {
            LogHelper.logVertex3(verteces[i]);
        }
    }
}