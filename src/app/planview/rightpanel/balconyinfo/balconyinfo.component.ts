import {Component, OnInit} from "@angular/core";
import {WallDto} from "../../dtos/wall.dto";
import {PlanViewFacade} from "../../planViewFacade";
import {Objects} from "../../objects3D/objects";
import {ChangeBalconyFloor, ChangeFloor, ChangeFloorTextureType} from "../../store/actions/rightpanel.actions";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../../store/state/app.state";
import {Observable} from "rxjs";
import {RoomDto} from "../../dtos/room.dto";
import {changeBalconyFloor, selectBalcony, selectRoom} from "../../store/selectors/rightpanel.selectors";
import {BalconyDto} from "../../dtos/balcony.dto";
import {PlanViewRouter} from "../../context/planViewRouter";
import {Materials} from "../../walls/materials";

@Component({
    selector: 'balconyinfo',
    templateUrl: './balconyinfo.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class BalconyinfoComponent implements OnInit {

    floors = Objects.floors;
    selectedFloor:string = this.floors[0].link;

    highlightedBalcony:BalconyDto = null;

    constructor(private _store: Store<IAppState>, private _planViewFacade: PlanViewFacade) {
        var highlightedBalconyObserver:Observable<BalconyDto> = this._store.pipe(select(selectBalcony));
        highlightedBalconyObserver.subscribe((balcony => {
            this.highlightedBalcony = balcony;
            if(balcony != null) {
                this.selectedFloor = balcony.floorTexture;
            }
        }));

        var balconyTextureObserver: Observable<string> = this._store.pipe(select(changeBalconyFloor));
        balconyTextureObserver.subscribe((floor => {
            if (floor && floor != "") {
                this.selectedFloor = floor;
            }
        }));
    }

    ngOnInit() {

    }

    goToFloorTextures() {
        this._store.dispatch(new ChangeFloorTextureType("balcony"));
        PlanViewRouter.getInstance().navigateToFloorTextures();
    }

    deleteBalcony() {
        this._planViewFacade.deleteBalcony();
    }
}