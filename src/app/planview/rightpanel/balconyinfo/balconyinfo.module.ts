import {NgModule} from "@angular/core";
import {BalconyinfoComponent} from "./balconyinfo.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule],
    declarations: [BalconyinfoComponent],
    exports: [
        BalconyinfoComponent
    ]
})
export class BalconyinfoModule {}