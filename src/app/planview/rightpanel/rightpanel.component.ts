import {Component, OnInit} from "@angular/core";
import {PlanViewRouter} from "../context/planViewRouter";

@Component({
    selector: 'rightpanel',
    templateUrl: './rightpanel.component.html'

})
export class RightpanelComponent implements OnInit {
    ngOnInit() {
        PlanViewRouter.getInstance().navigateToMainInfo();
    }
}