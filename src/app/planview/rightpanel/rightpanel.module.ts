import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {RightpanelComponent} from "./rightpanel.component";
import {BuildingModule} from "./building/building.module";
import {MaininfoModule} from "./maininfo/maininfo.module";
import {RoominfoModule} from "./roominfo/roominfo.module";
import {WallinfoModule} from "./wallinfo/wallinfo.module";
import {ObjectinfoModule} from "./objectinfo/objectinfo.module";
import {ObjectsbarModule} from "./objectsbar/objectsbar.module";
import {RouterModule} from "@angular/router";
import {FoundationModule} from "./foundation/foundation.module";
import {WindowsModule} from "./windows/windows.module";
import {DoorsModule} from "./doors/doors.module";
import {BalconyinfoModule} from "./balconyinfo/balconyinfo.module";
import {PorchinfoModule} from "./porchinfo/porchinfo.module";
import {RailingsModule} from "./railings/railings.module";
import {BalconiesModule} from "./balconies/balconies.module";
import {PorchModule} from "./porch/porch.module";
import {RoofModule} from "./roof/roof.module";
import {RoofinfoModule} from "./roofinfo/roofinfo.module";
import {TexturesModule} from "./textures/textures.module";
import {FloorTexturesModule} from "./floortextures/floor.textures.module";
import {StairsModule} from "./stairs/stairs.module";
import {ColumnsModule} from "./columns/columns.module";

@NgModule({
    imports: [
        HttpClientModule,
        BuildingModule,
        WindowsModule,
        StairsModule,
        ColumnsModule,
        DoorsModule,
        FoundationModule,
        MaininfoModule,
        RoominfoModule,
        WallinfoModule,
        RailingsModule,
        RoofModule,
        RoofinfoModule,
        BalconyinfoModule,
        BalconiesModule,
        PorchinfoModule,
        PorchModule,
        ObjectinfoModule,
        ObjectsbarModule,
        TexturesModule,
        FloorTexturesModule,
        RouterModule],
    declarations: [RightpanelComponent],
    exports: [
        RightpanelComponent
    ]
})
export class RightpanelModule {
}