import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {TexturesComponent} from "./textures.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule, HttpClientModule],
    declarations: [TexturesComponent],
    exports: [
        TexturesComponent
    ],
    bootstrap: [TexturesComponent]
})
export class TexturesModule {}