import {Component} from "@angular/core";
import {PlanViewSettings} from "../../configuration/planViewSettings";
import {
    ChangeFoundationMaterial,
    ChangeOutsideMaterial,
    ChangeWallpapers
} from "../../store/actions/rightpanel.actions";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../../store/state/app.state";
import {Observable} from "rxjs";
import {selectWallTextureType} from "../../store/selectors/rightpanel.selectors";
import {PlanViewRouter} from "../../context/planViewRouter";

@Component({
    selector: 'textures',
    templateUrl: './textures.component.html',
    styleUrls: ['../../planview.component.css']

})
export class TexturesComponent {
    stones = [
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper15.jpg", name: "Камень"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper16.jpg", name: "Камень"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper17.jpg", name: "Камень"}
    ];

    bricks = [
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper4.jpg", name: "Бетон"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper18.jpg", name: "Краска"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper19.jpg", name: "Штукатурка"}
    ];

    wallpapers = [
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper20.jpg", name: "Обои"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper21.jpg", name: "Обои"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper22.jpg", name: "Обои"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper23.jpg", name: "Обои"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper24.jpg", name: "Обои"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper25.jpg", name: "Обои"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper26.jpg", name: "Обои"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper27.jpg", name: "Обои"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper28.jpg", name: "Обои"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper29.jpg", name: "Обои"}
    ];

    plasters = [
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper3.jpg", name: "Штукатурка"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper6.jpg", name: "Штукатурка"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper7.jpg", name: "Штукатурка"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper8.jpg", name: "Штукатурка"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper9.jpg", name: "Штукатурка"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper10.jpg", name: "Штукатурка"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper11.jpg", name: "Штукатурка"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper12.jpg", name: "Штукатурка"}
    ];

    paints = [
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper1.jpg", name: "Краска"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper2.jpg", name: "Краска"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper5.jpg", name: "Краска"}
    ];

    woods = [
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper13.jpg", name: "Дерево"},
        {link: PlanViewSettings.URL_PREFIX + "assets/wallpapers/wallpaper14.jpg", name: "Дерево"}
    ];

    wallTextureType;

    constructor(private _store: Store<IAppState>) {
        var wallTextureTypeObserver: Observable<string> = this._store.pipe(select(selectWallTextureType));
        wallTextureTypeObserver.subscribe((type => {
            this.wallTextureType = type;
        }));
    }

    applyTexture(texture) {
        if (this.wallTextureType == "outside") {
            this._store.dispatch(new ChangeOutsideMaterial(texture.link));
        }
        if (this.wallTextureType == "foundation") {
            this._store.dispatch(new ChangeFoundationMaterial(texture.link));
        }
        if (this.wallTextureType == "room") {
            this._store.dispatch(new ChangeWallpapers(texture.link));
        }
    }

    back() {
        PlanViewRouter.getInstance().back();
    }
}