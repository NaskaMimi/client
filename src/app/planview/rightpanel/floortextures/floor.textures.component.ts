import {Component, OnInit} from "@angular/core";
import {PlanViewSettings} from "../../configuration/planViewSettings";
import {
    ChangeBalconyFloor,
    ChangeFloor,
    ChangePorchFloor,
} from "../../store/actions/rightpanel.actions";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../../store/state/app.state";
import {Observable} from "rxjs";
import {selectFloorTextureType} from "../../store/selectors/rightpanel.selectors";
import {PlanViewRouter} from "../../context/planViewRouter";

@Component({
    selector: 'textures',
    templateUrl: './floor.textures.component.html',
    styleUrls: ['../../planview.component.css']

})
export class FloorTexturesComponent {

    stones = [
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor3.jpg", name: "Камень" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor4.jpg", name: "Камень" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor5.jpg", name: "Камень" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor6.jpg", name: "Камень" }
    ];

    bricks = [
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor7.jpg", name: "Плитка" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor8.jpg", name: "Плитка" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor9.jpg", name: "Плитка" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor10.jpg", name: "Плитка" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor11.jpg", name: "Плитка" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor12.jpg", name: "Плитка" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor13.jpg", name: "Плитка" }
    ];

    wallpapers = [
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor19.jpg", name: "Ковер" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor20.jpg", name: "Ковер" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor21.jpg", name: "Ковер" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor22.jpg", name: "Ковер" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor23.jpg", name: "Ковер" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor24.jpg", name: "Ковер" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor25.jpg", name: "Ковер" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor26.jpg", name: "Ковер" }
    ];

    plasters = [
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor27.jpg", name: "Наливной пол" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor28.jpg", name: "Наливной пол" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor29.jpg", name: "Наливной пол" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor30.jpg", name: "Наливной пол" }
    ];

    paints = [
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor1.jpg", name: "Ламинат" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor2.jpg", name: "Ламинат" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor14.jpg", name: "Ламинат" }
    ];

    woods = [
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor15.jpg", name: "Паркет" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor16.jpg", name: "Паркет" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor17.jpg", name: "Паркет" },
        { link: PlanViewSettings.URL_PREFIX + "assets/floors/floor18.jpg", name: "Паркет" }
    ];

    floorTextureType;

    constructor(private _store: Store<IAppState>) {
        var floorTextureTypeObserver: Observable<string> = this._store.pipe(select(selectFloorTextureType));
        floorTextureTypeObserver.subscribe((type => {
            this.floorTextureType = type;
        }));
    }

    applyTexture(texture) {
        if(this.floorTextureType == "porch") {
            this._store.dispatch(new ChangePorchFloor(texture.link));
        }
        if(this.floorTextureType == "balcony") {
            this._store.dispatch(new ChangeBalconyFloor(texture.link));
        }
        if(this.floorTextureType == "room") {
            this._store.dispatch(new ChangeFloor(texture.link));
        }
    }

    back() {
        PlanViewRouter.getInstance().back();
    }
}
