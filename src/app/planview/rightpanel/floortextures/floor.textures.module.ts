import {NgModule} from "@angular/core";
import {FloorTexturesComponent} from "./floor.textures.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule],
    declarations: [FloorTexturesComponent],
    exports: [
        FloorTexturesComponent
    ]
})
export class FloorTexturesModule {}