import {Component, OnInit} from "@angular/core";
import {PlanViewFacade} from "../../planViewFacade";
import {Objects} from "../../objects3D/objects";

@Component({
    selector: 'balconies',
    templateUrl: './balconies.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class BalconiesComponent implements OnInit {

    floors = Objects.floors;
    selectedFloor = this.floors[0];

    constructor(private _planViewFacade: PlanViewFacade) {

    }

    ngOnInit() {

    }

}