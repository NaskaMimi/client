import {NgModule} from "@angular/core";
import {BalconiesComponent} from "./balconies.component";

@NgModule({
    declarations: [BalconiesComponent],
    exports: [
        BalconiesComponent
    ]
})
export class BalconiesModule {}