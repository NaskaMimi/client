import {Component, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../../store/state/app.state";
import {ChangeRoofMaterial, ChangeWallTextureType} from "../../store/actions/rightpanel.actions";
import {PlanViewFacade} from "../../planViewFacade";
import {PlanViewContext} from "../../context/planViewContext";
import {Objects} from "../../objects3D/objects";
import {PlanViewRouter} from "../../context/planViewRouter";
import {Observable} from "rxjs";
import {changeFoundationMaterial} from "../../store/selectors/rightpanel.selectors";

@Component({
    selector: 'maininfo',
    templateUrl: './maininfo.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class MaininfoComponent implements OnInit {

    wallpapers = Objects.wallpapers;
    roofs = Objects.roofs;
    selectedOutsideMaterial = PlanViewContext.getInstance().outsideTexture;
    selectedRoofMaterial = this.roofs[0];
    selectedFoundationMaterial: string = this.wallpapers[0].link;

    constructor(private _store: Store<IAppState>, private _planViewFacade: PlanViewFacade) {
        var foundationMaterialObserver: Observable<string> = this._store.pipe(select(changeFoundationMaterial));
        foundationMaterialObserver.subscribe((foundationMaterial => {
            if (foundationMaterial != null && foundationMaterial != "") {
                this.selectedFoundationMaterial = foundationMaterial;
            }
        }));
    }

    ngOnInit() {
        if (PlanViewContext.getInstance().levelsCount == 1) {
            (<HTMLInputElement>document.getElementById("level2")).hidden = true;
            (<HTMLInputElement>document.getElementById("level3")).hidden = true;
        } else if (PlanViewContext.getInstance().levelsCount == 2) {
            (<HTMLInputElement>document.getElementById("level2")).hidden = false;
            (<HTMLInputElement>document.getElementById("level3")).hidden = true;
        } else if (PlanViewContext.getInstance().levelsCount == 3) {
            (<HTMLInputElement>document.getElementById("level2")).hidden = false;
            (<HTMLInputElement>document.getElementById("level3")).hidden = false;
        }
    }

    singleModeOn() {
        this._planViewFacade.singleModeOn();
    }

    roomModeOn() {
        this._planViewFacade.roomModeOn();
    }

    balconyForViewModeOn() {
        this._planViewFacade.balconyForViewModeOn();
    }

    porchForViewModeOn() {
        this._planViewFacade.porchForViewModeOn();
    }

    roofForViewModeOn() {
        this._planViewFacade.roofForViewModeOn();
    }

    models3DModeOn() {
        this._planViewFacade.models3DModeOn();
    }

    changeWallsHeightView(viewHeight: number) {
        this._planViewFacade.changeWallsHeightView(viewHeight);
    }

    changeRoofVisibility(visibility: boolean) {
        this._planViewFacade.changeRoofVisibility(visibility);
    }

    showRoof() {
        this.showLevel(PlanViewContext.getInstance().levelsCount);
        this._planViewFacade.showRoof();
    }

    addLevel() {
        if ((<HTMLInputElement>document.getElementById("level2")).hidden == true) {
            (<HTMLInputElement>document.getElementById("level2")).hidden = false;
        } else if ((<HTMLInputElement>document.getElementById("level3")).hidden == true) {
            (<HTMLInputElement>document.getElementById("level3")).hidden = false;
            (<HTMLInputElement>document.getElementById("addLevelButton")).hidden = true;
        }
        this._planViewFacade.addLevel();
        this.showLevel(PlanViewContext.getInstance().levelsCount);
    }

    showLevel(number: number) {
        this._planViewFacade.switchToLevel(number);
    }

    roofMaterialsChange(roof) {
        this._store.dispatch(new ChangeRoofMaterial(roof.link));
    }

    goToTextures(mode) {
        this._store.dispatch(new ChangeWallTextureType(mode));
        PlanViewRouter.getInstance().navigateToWallTextures();
    }

    private defineSelectedWallpapers(wallpapersTexture: string) {
        for (var i = 0; i < this.wallpapers.length; i++) {
            var wallpaper = this.wallpapers[i];
            if (wallpaper.link == wallpapersTexture) {
                return wallpaper;
            }
        }
        return undefined;
    }
}