import {NgModule} from "@angular/core";
import {MaininfoComponent} from "./maininfo.component";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [
        FormsModule, CommonModule
    ],
    declarations: [MaininfoComponent],
    exports: [
        MaininfoComponent
    ]
})
export class MaininfoModule {}