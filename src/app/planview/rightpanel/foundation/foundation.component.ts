import {Component} from "@angular/core";
import {PlanViewFacade} from "../../planViewFacade";
import {Objects} from "../../objects3D/objects";
import {ChangeFoundationMaterial} from "../../store/actions/rightpanel.actions";
import {Store} from "@ngrx/store";
import {IAppState} from "../../store/state/app.state";
import {PlanViewEnvironment} from "../../context/planViewEnvironment";
import {PlanViewContext} from "../../context/planViewContext";

@Component({
    selector: 'foundation',
    templateUrl: './foundation.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class FoundationComponent {
    wallpapers = Objects.wallpapers;
    selectedFoundationMaterial = this.wallpapers[0];

    constructor(private _store: Store<IAppState>, private _planViewFacade: PlanViewFacade) {
        this.selectedFoundationMaterial = this.defineSelectedWallpapers(PlanViewContext.getInstance().foundationTexture);
    }

    foundationMaterialsChange(wallpaper) {
        this._store.dispatch(new ChangeFoundationMaterial(wallpaper.link));
        PlanViewEnvironment.getInstance().render();
    }

    private defineSelectedWallpapers(wallpapersTexture: string) {
        for(var i = 0; i<this.wallpapers.length; i++){
            var wallpaper = this.wallpapers[i];
            if(wallpaper.link == wallpapersTexture){
                return wallpaper;
            }
        }
        return undefined;
    }
}