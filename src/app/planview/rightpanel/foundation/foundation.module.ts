import {NgModule} from "@angular/core";
import {FoundationComponent} from "./foundation.component";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [
        FormsModule, CommonModule
    ],
    declarations: [FoundationComponent],
    exports: [
        FoundationComponent
    ]
})
export class FoundationModule {}