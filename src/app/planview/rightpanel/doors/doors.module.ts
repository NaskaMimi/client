import {NgModule} from "@angular/core";
import {DoorsComponent} from "./doors.component";
import {ObjectModule} from "../objectsbar/object.module";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
    imports: [HttpClientModule, ObjectModule],
    declarations: [DoorsComponent],
    exports: [
        DoorsComponent
    ]
})
export class DoorsModule {}