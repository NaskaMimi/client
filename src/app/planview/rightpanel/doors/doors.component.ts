import {Component} from "@angular/core";
import {PlanViewFacade} from "../../planViewFacade";
import {Objects} from "../../objects3D/objects";
import {Object3D} from "../../objects3D/object3D";

@Component({
    selector: 'doors',
    templateUrl: './doors.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class DoorsComponent {
    doorsArray = Objects.doorsArray;
    outsideDoorsArray = Objects.outsideDoorsArray;

    constructor(private _planViewFacade: PlanViewFacade) {}

    createWindow(window: Object3D) {
        this._planViewFacade.createWindow(window);
    }
}