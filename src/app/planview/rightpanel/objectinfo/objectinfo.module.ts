import {NgModule} from "@angular/core";
import {ObjectinfoComponent} from "./objectinfo.component";

@NgModule({
    declarations: [ObjectinfoComponent],
    exports: [
        ObjectinfoComponent
    ]
})
export class ObjectinfoModule {}