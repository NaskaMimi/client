import {Component} from "@angular/core";
import {MoveObject, RotateObjectClockwise, RotateObjectCounterClockwise} from "../../store/actions/rightpanel.actions";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../../store/state/app.state";
import {Model3DDto} from "../../dtos/model3D.dto";
import {Observable} from "rxjs";
import {selectObject} from "../../store/selectors/rightpanel.selectors";
import {PlanViewFacade} from "../../planViewFacade";

@Component({
    selector: 'objectinfo',
    templateUrl: './objectinfo.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']
})
export class ObjectinfoComponent {
    highlightedObject:Model3DDto = null;

    constructor(private _store: Store<IAppState>, private _planViewFacade: PlanViewFacade) {
        var highlightedObjectObserver: Observable<Model3DDto> = this._store.pipe(select(selectObject));
        highlightedObjectObserver.subscribe((object => {
            this.highlightedObject = object;
        }));
    }

    moveObject() {
        this._store.dispatch(new MoveObject(true));
    }

    rotateObjectClockwise() {
        this._store.dispatch(new RotateObjectClockwise(this.highlightedObject.rotation - (Math.PI / 2)));
    }

    rotateObjectCounterClockwise() {
        this._store.dispatch(new RotateObjectCounterClockwise(this.highlightedObject.rotation + (Math.PI / 2)));
    }

    deleteObject() {
        this._planViewFacade.deleteSelectedObject();
    }
}