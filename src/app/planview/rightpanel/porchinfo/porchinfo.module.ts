import {NgModule} from "@angular/core";
import {PorchinfoComponent} from "./porchinfo.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule],
    declarations: [PorchinfoComponent],
    exports: [
        PorchinfoComponent
    ]
})
export class PorchinfoModule {}