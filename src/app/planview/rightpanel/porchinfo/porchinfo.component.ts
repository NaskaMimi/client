import {Component} from "@angular/core";
import {PlanViewFacade} from "../../planViewFacade";
import {Objects} from "../../objects3D/objects";
import {ChangeFloorTextureType} from "../../store/actions/rightpanel.actions";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../../store/state/app.state";
import {Observable} from "rxjs";
import {changePorchFloor, selectPorch} from "../../store/selectors/rightpanel.selectors";
import {PorchDto} from "../../dtos/porch.dto";
import {PlanViewRouter} from "../../context/planViewRouter";

@Component({
    selector: 'porchinfo',
    templateUrl: './porchinfo.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class PorchinfoComponent {

    floors = Objects.floors;
    selectedFloor: string = this.floors[0].link;

    highlightedPorch: PorchDto = null;

    constructor(private _store: Store<IAppState>, private _planViewFacade: PlanViewFacade) {
        var highlightedPorchObserver: Observable<PorchDto> = this._store.pipe(select(selectPorch));
        highlightedPorchObserver.subscribe((porch => {
            this.highlightedPorch = porch;
            if (porch != null) {
                console.log("porch.floorTexture = " + porch.floorTexture);
                this.selectedFloor = porch.floorTexture;
            }
        }));

        var porchTextureObserver: Observable<string> = this._store.pipe(select(changePorchFloor));
        porchTextureObserver.subscribe((floor => {
            if (floor && floor != "") {
                this.selectedFloor = floor;
            }
        }));
    }

    deletePorch() {
        this._planViewFacade.deletePorch();
    }

    goToFloorTextures() {
        this._store.dispatch(new ChangeFloorTextureType("porch"));
        PlanViewRouter.getInstance().navigateToFloorTextures();
    }
}