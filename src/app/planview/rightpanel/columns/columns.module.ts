import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {ColumnsComponent} from "./columns.component";
import {CommonModule} from "@angular/common";
import {ObjectModule} from "../objectsbar/object.module";

@NgModule({
    imports: [CommonModule, HttpClientModule, ObjectModule],
    declarations: [ColumnsComponent],
    exports: [
        ColumnsComponent
    ]
})
export class ColumnsModule {
}