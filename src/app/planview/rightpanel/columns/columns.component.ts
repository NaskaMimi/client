import {Component} from "@angular/core";
import {PlanViewFacade} from "../../planViewFacade";
import {Objects} from "../../objects3D/objects";
import {Object3D} from "../../objects3D/object3D";

@Component({
    selector: 'columns',
    templateUrl: './columns.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class ColumnsComponent {

    columnsArray = Objects.columnsArray;

    constructor(private _planViewFacade: PlanViewFacade) {}

    createWindow(window: Object3D) {
        this._planViewFacade.createWindow(window);
    }
}