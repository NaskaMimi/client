import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {StairsComponent} from "./stairs.component";
import {ObjectModule} from "../objectsbar/object.module";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule, HttpClientModule, ObjectModule],
    declarations: [StairsComponent],
    exports: [
        StairsComponent
    ]
})
export class StairsModule {}