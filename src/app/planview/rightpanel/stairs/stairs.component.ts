import {Component} from "@angular/core";
import {PlanViewFacade} from "../../planViewFacade";
import {Object3D} from "../../objects3D/object3D";
import {Objects} from "../../objects3D/objects";

@Component({
    selector: 'stairs',
    templateUrl: './stairs.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class StairsComponent {
    stairsArray = Objects.stairsArray;
    outsideStairsArray = Objects.outsideStairsArray;

    constructor(private _planViewFacade: PlanViewFacade) {}

    createWindow(window: Object3D) {
        this._planViewFacade.createWindow(window);
    }

    showScroll(windowScroll: string) {
        (<HTMLInputElement>document.getElementById("stair-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("outsideStairs-scroll")).hidden = true;


        (<HTMLInputElement>document.getElementById(windowScroll)).hidden = false;

    }
}