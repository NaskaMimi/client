import {NgModule} from "@angular/core";
import {PorchComponent} from "./porch.component";

@NgModule({
    declarations: [PorchComponent],
    exports: [
        PorchComponent
    ]
})
export class PorchModule {}