import {Component} from "@angular/core";
import {PlanViewFacade} from "../../planViewFacade";

@Component({
    selector: 'porch',
    templateUrl: './porch.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class PorchComponent {
    constructor(private _planViewFacade: PlanViewFacade) {}
}