import {Component} from "@angular/core";
import {PlanViewFacade} from "../../planViewFacade";

@Component({
    selector: 'building',
    templateUrl: './building.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class BuildingComponent {
    constructor(private _planViewFacade: PlanViewFacade) {}

    singleModeOn() {
        this._planViewFacade.singleModeOn();
    }

    roomModeOn() {
        this._planViewFacade.roomModeOn();
    }
}