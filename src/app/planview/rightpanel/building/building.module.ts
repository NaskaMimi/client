import {NgModule} from "@angular/core";
import {BuildingComponent} from "./building.component";

@NgModule({
    declarations: [BuildingComponent],
    exports: [
        BuildingComponent
    ]
})
export class BuildingModule {}