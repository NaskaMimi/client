import {NgModule} from "@angular/core";
import {WindowsComponent} from "./windows.component";
import {ObjectModule} from "../objectsbar/object.module";

@NgModule({
    imports: [ObjectModule],
    declarations: [WindowsComponent],
    exports: [
        WindowsComponent
    ]
})
export class WindowsModule {}