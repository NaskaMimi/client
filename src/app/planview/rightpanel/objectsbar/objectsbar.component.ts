import {Component} from "@angular/core";
import {Objects} from "../../objects3D/objects";
import {Object3D} from "../../objects3D/object3D";
import {PlanViewFacade} from "../../planViewFacade";
import {TexturesElement} from "../../objects3D/texturesElement";

@Component({
    selector: 'objectsbar',
    templateUrl: './objectsbar.component.html',
    styleUrls: ['objectsbar.component.css']
})
export class ObjectsbarComponent {

    bedsArray = Objects.bedsArray;
    sofasArray = Objects.sofasArray;
    coffeetablesArray = Objects.coffeetablesArray;
    tablesArray = Objects.tablesArray;
    chairsArray = Objects.chairsArray;
    bottomKitchensArray = Objects.bottomKitchensArray;
    topKitchensArray = Objects.topKitchensArray;
    fireplacesArray = Objects.fireplacesArray;
    nightstandsArray = Objects.nightstandsArray;
    tvsArray = Objects.tvsArray;
    wardrobesArray = Objects.wardrobesArray;
    desksArray = Objects.desksArray;
    plumbingsArray = Objects.plumbingsArray;
    bathroomFurnitureArray = Objects.bathroomFurnitureArray;
    decorsArray = Objects.decorsArray;

    constructor(private _planViewFacade: PlanViewFacade) {}

    showScroll(windowScroll: string) {
        (<HTMLInputElement>document.getElementById("fireplace-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("sofa-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("tv-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("bed-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("nightstand-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("wardrobe-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("table-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("kitchen-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("desk-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("plumbing-scroll")).hidden = true;
        (<HTMLInputElement>document.getElementById("decor-scroll")).hidden = true;

        (<HTMLInputElement>document.getElementById(windowScroll)).hidden = false;

    }

    createWindow(window: Object3D) {
        this._planViewFacade.createWindow(window);
    }

    changeTextures(sofa: Object3D, variant:TexturesElement) {
        sofa.selectedIndex = sofa.textures.indexOf(variant);
    }
}