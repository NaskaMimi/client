import {Component, Input, OnInit} from "@angular/core";
import {Object3D} from "../../objects3D/object3D";
import {PlanViewFacade} from "../../planViewFacade";
import {TexturesElement} from "../../objects3D/texturesElement";

@Component({
    selector: 'object-list',
    templateUrl: './object.component.html',
    styleUrls: ['object.component.css']
})
export class ObjectComponent implements OnInit {
    public static counter = 0;
    public static colorCounter = 0;

    @Input() listTitle: string = "";
    @Input() type: string = "furniture";
    @Input() titleVisible: boolean = true;
    @Input() objectsArray = [];
    public expandableId:string;
    public colorExpandableId:string;

    constructor(private _planViewFacade: PlanViewFacade) {}

    ngOnInit() {
        ObjectComponent.counter++;
        ObjectComponent.colorCounter++;
        this.expandableId = "title-button" + ObjectComponent.counter
        this.colorExpandableId = "color-button" + ObjectComponent.colorCounter
    }


    createWindow(window: Object3D, type:string = "furniture") {
        this._planViewFacade.createWindow(window, type);
    }

    changeTextures(sofa: Object3D, variant: TexturesElement) {
        sofa.selectedIndex = sofa.textures.indexOf(variant);
    }
}
