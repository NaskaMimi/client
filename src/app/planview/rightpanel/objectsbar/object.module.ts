import {NgModule} from "@angular/core";
import {ObjectComponent} from "./object.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule],
    declarations: [ObjectComponent],
    exports: [
        ObjectComponent
    ]
})
export class ObjectModule {}