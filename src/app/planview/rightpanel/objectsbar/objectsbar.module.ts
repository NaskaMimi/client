import {NgModule} from "@angular/core";
import {ObjectsbarComponent} from "./objectsbar.component";
import {ObjectModule} from "./object.module";

@NgModule({
    imports: [ObjectModule],
    declarations: [ObjectsbarComponent],
    exports: [
        ObjectsbarComponent
    ]
})
export class ObjectsbarModule {}