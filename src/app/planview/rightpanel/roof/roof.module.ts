import {NgModule} from "@angular/core";
import {RoofComponent} from "./roof.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule],
    declarations: [RoofComponent],
    exports: [
        RoofComponent
    ]
})
export class RoofModule {}