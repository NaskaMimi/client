import {Component} from "@angular/core";
import {PlanViewFacade} from "../../planViewFacade";

@Component({
    selector: 'roof',
    templateUrl: './roof.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class RoofComponent {
    roofsArray = ["roof1", "roof2"];

    constructor(private _planViewFacade: PlanViewFacade) {}

    createRoof(roof: string) {
        this._planViewFacade.createRoof(roof);
    }
}