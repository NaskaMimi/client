import {NgModule} from "@angular/core";
import {RoofinfoComponent} from "./roofinfo.component";

@NgModule({
    declarations: [RoofinfoComponent],
    exports: [
        RoofinfoComponent
    ]
})
export class RoofinfoModule {
}