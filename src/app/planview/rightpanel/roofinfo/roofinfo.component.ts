import {Component} from "@angular/core";
import {
    RoofTransformation,
    RotateObjectClockwise,
    RotateObjectCounterClockwise
} from "../../store/actions/rightpanel.actions";
import {select, Store} from "@ngrx/store";
import {IAppState} from "../../store/state/app.state";
import {Observable} from "rxjs";
import {selectRoof} from "../../store/selectors/rightpanel.selectors";
import {RoofDto} from "../../dtos/roof.dto";
import {PlanViewFacade} from "../../planViewFacade";

@Component({
    selector: 'roofinfo',
    templateUrl: './roofinfo.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class RoofinfoComponent {
    highlightedRoof:RoofDto = null;

    constructor(private _store: Store<IAppState>, private _planViewFacade: PlanViewFacade) {
        var highlightedRoofObserver: Observable<RoofDto> = this._store.pipe(select(selectRoof));
        highlightedRoofObserver.subscribe((object => {
            this.highlightedRoof = object;
        }));
    }

    moveObject() {
        this._store.dispatch(new RoofTransformation("translate"));
    }

    scaleObject() {
        this._store.dispatch(new RoofTransformation("scale"));
    }

    rotateObjectClockwise() {
        this._store.dispatch(new RotateObjectClockwise(this.highlightedRoof.rotation - (Math.PI / 2)));
    }

    rotateObjectCounterClockwise() {
        this._store.dispatch(new RotateObjectCounterClockwise(this.highlightedRoof.rotation + (Math.PI / 2)));
    }

    unselectObject() {
        this._store.dispatch(new RoofTransformation(""));
    }

    deleteRoof() {
        this._planViewFacade.deleteRoof();
    }
}