import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {WallinfoComponent} from "./wallinfo.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule, HttpClientModule],
    declarations: [WallinfoComponent],
    exports: [
        WallinfoComponent
    ]
})
export class WallinfoModule {
}