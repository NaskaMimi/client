import {Component} from "@angular/core";
import {WallDto} from "../../dtos/wall.dto";
import {PlanViewFacade} from "../../planViewFacade";
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import {selectWall} from "../../store/selectors/rightpanel.selectors";
import {IAppState} from "../../store/state/app.state";

@Component({
    selector: 'wallinfo',
    templateUrl: './wallinfo.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class WallinfoComponent {

    highlightedWall:WallDto = null;

    constructor(private _store: Store<IAppState>, private _planViewFacade: PlanViewFacade) {
        var highlightedWallObserver: Observable<WallDto> = this._store.pipe(select(selectWall));
        highlightedWallObserver.subscribe((wall => {
            this.highlightedWall = wall;
        }));
    }

    deleteWall() {
        this._planViewFacade.deleteWall();
    }
}