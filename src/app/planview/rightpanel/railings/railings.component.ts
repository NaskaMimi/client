import {Component} from "@angular/core";
import {PlanViewFacade} from "../../planViewFacade";
import {Objects} from "../../objects3D/objects";
import {Object3D} from "../../objects3D/object3D";

@Component({
    selector: 'railings',
    templateUrl: './railings.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class RailingsComponent {
    railingsArray = Objects.railingsArray;

    constructor(private _planViewFacade: PlanViewFacade) {}

    createRailing(railing: Object3D) {
        this._planViewFacade.drawRailing(railing);
    }
}