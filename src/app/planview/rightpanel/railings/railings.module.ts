import {NgModule} from "@angular/core";
import {RailingsComponent} from "./railings.component";
import {CommonModule} from "@angular/common";
import {ObjectModule} from "../objectsbar/object.module";

@NgModule({
    imports: [CommonModule, ObjectModule],
    declarations: [RailingsComponent],
    exports: [
        RailingsComponent
    ]
})
export class RailingsModule {}