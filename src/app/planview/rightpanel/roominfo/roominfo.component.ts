import {Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import {RoomDto} from "../../dtos/room.dto";
import {select, Store} from "@ngrx/store";
import {changeFloor, changeWallpapers, selectRoom} from "../../store/selectors/rightpanel.selectors";
import {IAppState} from "../../store/state/app.state";
import {
    ChangeFloor,
    ChangeFloorTextureType,
    ChangeWallTextureType
} from "../../store/actions/rightpanel.actions";
import {PlanViewFacade} from "../../planViewFacade";
import {PlanViewRouter} from "../../context/planViewRouter";

@Component({
    selector: 'roominfo',
    templateUrl: './roominfo.component.html',
    styleUrls: ['../../../app.component.css', '../../planview.component.css']

})
export class RoominfoComponent {

    highlightedRoom:RoomDto = null;
    selectedWallpaper = "";
    selectedFloor = "";

    constructor(private _store: Store<IAppState>, private _planViewFacade: PlanViewFacade) {
        var highlightedRoomObserver:Observable<RoomDto> = this._store.pipe(select(selectRoom));
        highlightedRoomObserver.subscribe((room => {
            this.highlightedRoom = room;
            if(room != null) {
                this.selectedWallpaper = room.wallpapersTexture;
                this.selectedFloor = room.floorTexture;
            }
        }));

        var wallpapersObserver: Observable<string> = this._store.pipe(select(changeWallpapers));
        wallpapersObserver.subscribe((wallpaper => {
            if (wallpaper && wallpaper != "") {
                this.selectedWallpaper = wallpaper;
            }
        }));

        var floorObserver: Observable<string> = this._store.pipe(select(changeFloor));
        floorObserver.subscribe((floor => {
            if (floor && floor != "") {
                this.selectedFloor = floor;
            }
        }));
    }

    changeRoomName() {
        var newName = (<HTMLInputElement>document.getElementById("roomName")).value;
        this._planViewFacade.changeRoomName(newName);
    }

    deleteRoom() {
        this._planViewFacade.deleteRoom();
    }

    floorChange(floor) {
        this._store.dispatch(new ChangeFloor(floor.link));
    }

    goToWallTextures() {
        this._store.dispatch(new ChangeWallTextureType("room"));
        PlanViewRouter.getInstance().navigateToWallTextures();
    }

    goToFloorTextures() {
        this._store.dispatch(new ChangeFloorTextureType("room"));
        PlanViewRouter.getInstance().navigateToFloorTextures();
    }
}