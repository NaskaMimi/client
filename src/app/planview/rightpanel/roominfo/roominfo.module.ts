import {NgModule} from "@angular/core";
import {RoominfoComponent} from "./roominfo.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule],
    declarations: [RoominfoComponent],
    exports: [
        RoominfoComponent
    ]
})
export class RoominfoModule {}