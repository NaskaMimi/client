export class PlanViewSettings{

    public static URL_PREFIX:string = "../";

    public static WALL_SIZE:number = 5;
    public static APPENDER_SIZE:number = 5;
    public static WALL_INITIAL_SIZE:number = 1;
    public static WALL_HEIGHT:number = 240;

    public static FOUNDATION_HEIGHT:number = 100;

    public static STEP:number = 50;
    public static HALF_STEP:number = PlanViewSettings.STEP / 2;

    public static LEVEL0_POSITION:number = 0;
    public static LEVEL1_POSITION:number = PlanViewSettings.FOUNDATION_HEIGHT;
    public static LEVEL2_POSITION:number = PlanViewSettings.FOUNDATION_HEIGHT + PlanViewSettings.WALL_HEIGHT;
    public static LEVEL3_POSITION:number = PlanViewSettings.FOUNDATION_HEIGHT + 2 * PlanViewSettings.WALL_HEIGHT;
    public static LEVEL4_POSITION:number = PlanViewSettings.FOUNDATION_HEIGHT + 3 * PlanViewSettings.WALL_HEIGHT;
}