import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "../authservice/auth.service";

@Component({
    selector: 'hello',
    templateUrl: './hello.component.html',
    styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {
    constructor(private router: Router,
                private authService: AuthService) {
    }

    signIngByGuest() {
        this.authService.authorize('guest', 'guest');
    }

    signOrRegister() {
        this.router.navigate(['/auth/sign']);
    }

    ngOnInit(): void {
        if (this.authService.isLogin()) {
            this.authService.moveToPlanView();
        }
    }
}