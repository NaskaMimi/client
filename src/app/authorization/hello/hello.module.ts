import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {HelloComponent} from "./hello.component";

@NgModule({
    imports: [HttpClientModule],
    declarations: [HelloComponent],
    exports: [
        HelloComponent
    ]
})
export class HelloModule { }
