import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RegistrationComponent} from "./registration.component";
import {NgBootstrapFormValidationModule} from "ng-bootstrap-form-validation";

@NgModule({
    imports: [FormsModule, ReactiveFormsModule,
        NgBootstrapFormValidationModule],
    declarations: [RegistrationComponent],
    exports: [
        RegistrationComponent
    ]
})
export class RegistrationModule {}