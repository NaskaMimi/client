import {Component, OnInit} from "@angular/core";
import {AuthService} from "../authservice/auth.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
    public registrationForm: FormGroup;

    constructor(public authService: AuthService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.registrationForm = this.formBuilder.group({
            Email: new FormControl('', [Validators.required, Validators.email]),
            Login: new FormControl('', [Validators.required, Validators.minLength(5)]),
        });
    }

    registrationHandler(login: string, email: string) {
        this.authService.registration(login, email)
            .subscribe((value => {
                alert('Письмо отправлено на почту')
            }))
    }
}