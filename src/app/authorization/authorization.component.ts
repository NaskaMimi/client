import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "./authservice/auth.service";
import {AuthRequest, AuthResponse} from "./authservice/auth.dto";

@Component({
    selector: 'auth-app',
    templateUrl: './authorization.component.html',
    styleUrls: ['./authorization.component.css']
})
export class AuthorizationComponent implements OnInit {
    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService) {
    }

    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe(params => {
            var jwtToken: string = params['jwtToken'];
            var expiredAt: number = Number(params['expiredAt']);
            var roles: string[] = params['role'];
            if (jwtToken) {
                this.authService.saveAuthDataAndNavigatePlanView(new AuthResponse(jwtToken,
                    expiredAt, roles))
            }
        });
    }
}