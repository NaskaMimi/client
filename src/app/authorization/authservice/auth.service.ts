import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {AuthRequest, AuthResponse, ChangePassword, RegistrationModel} from "./auth.dto";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import * as moment from "moment";
import {Observable} from "rxjs";

@Injectable()
export class AuthService {
    constructor(private http: HttpClient,
                private router: Router) {
    }

    authorize(login: string, password: string) {
        this.http.post(`${environment.backendUrl}/api/v1/auth/login`, new AuthRequest(login, password))
            .subscribe((resp: AuthResponse) => {
                this.saveAuthDataAndNavigatePlanView(resp)
            });
    }

    sendChangePassword(email: string, password: string): Observable<Object> {
        return this.http.put(`${environment.backendUrl}/api/v1/user/change-password`,
            new ChangePassword(email, password))
    }

    saveAuthDataAndNavigatePlanView(authResponse: AuthResponse) {
        localStorage.setItem('auth', JSON.stringify(authResponse));

        this.router.navigate(['/planview']);
    }

    getAuthResponse(): AuthResponse {
        const authString = localStorage.getItem("auth");
        return authString ? JSON.parse(authString) : null;
    }

    registration(login: string, email: string) {
        return this.http.post(`${environment.backendUrl}/api/v1/user/registration`,
            new RegistrationModel(login, email))
    }

    isLogin() {
        const auth: AuthResponse = JSON.parse(localStorage.getItem("auth"));
        return auth
            && auth.jwtToken
            && auth.expiredAt
            && moment().add(30, 'seconds').isAfter(auth.expiredAt);
    }

    isGuest() {
        let authResponse = this.getAuthResponse();
        return authResponse.roles.some(e => e === 'GUEST')
    }

    logout() {
        localStorage.removeItem('auth')
        this.router.navigate(['/auth/hello']);
    }

    moveToSign() {
        this.router.navigate(['/auth/sign']);
    }

    moveToPlanView() {
        this.router.navigate(['/planview'])
    }
}