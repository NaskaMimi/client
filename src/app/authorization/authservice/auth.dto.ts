export class AuthRequest {
    constructor(public login: string,
                public password: string) {
    }
}

export class AuthResponse {
    constructor(public jwtToken: string,
                public expiredAt: number,
                public roles: string[]) {
    }
}

export class ChangeEmail {
    constructor(public oldEmail: string,
                public newEmail: string) {
    }
}

export class ChangePassword {
    constructor(public email: string,
                public newPassword: string) {}
}

export class RegistrationModel {
    constructor(public login: string,
                public email: string) {}
}