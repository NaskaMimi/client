import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {AuthService} from "./auth.service";

@NgModule({
    imports: [HttpClientModule, RouterModule],
    providers: [AuthService],
})
export class AuthorizationServiceModule { }
