import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {AuthService} from "./authservice/auth.service";
import {AuthResponse} from "./authservice/auth.dto";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

    intercept(req: HttpRequest<any>,
              next: HttpHandler): Observable<HttpEvent<any>> {
        const authResponse: AuthResponse = this.authService.getAuthResponse()

        console.log("idToken = " + authResponse);
        if (authResponse) {
            const cloned = req.clone({
                headers: req.headers.set("Authorization",
                    "Bearer " + authResponse.jwtToken)
            });

            return next.handle(cloned);
        }
        else {
            return next.handle(req);
        }
    }
}
