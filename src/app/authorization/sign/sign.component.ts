import {Component, OnInit} from "@angular/core";
import {environment} from '../../../environments/environment';
import {AuthService} from "../authservice/auth.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'sign',
    templateUrl: './sign.component.html',
    styleUrls: ['./sign.component.css']
})
export class SignComponent implements OnInit{
    public googleOath2LoginUrl: string;
    public signForm: FormGroup;

    constructor(private authService: AuthService,
                private formBuilder: FormBuilder) {
        this.googleOath2LoginUrl = `${environment.backendUrl}/oauth2/authorization/google`
    }

    ngOnInit(): void {
        this.signForm = this.formBuilder.group({
            Login: new FormControl('', [Validators.required, Validators.minLength(5)]),
            Password: new FormControl('', [Validators.required, Validators.minLength(5)])
        });
    }

    public authorization(login: string, password: string) {
        this.authService.authorize(login, password)
    }
}