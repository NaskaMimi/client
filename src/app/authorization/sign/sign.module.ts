import {NgModule} from "@angular/core";
import {SignComponent} from "./sign.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgBootstrapFormValidationModule} from "ng-bootstrap-form-validation";

@NgModule({
    imports: [FormsModule, ReactiveFormsModule,
        NgBootstrapFormValidationModule],
    declarations: [SignComponent],
    exports: [
        SignComponent
    ]
})
export class SignModule {}