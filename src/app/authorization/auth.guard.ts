import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {AuthService} from "./authservice/auth.service";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router,
                private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const authResponse = this.authService.getAuthResponse()

        if (authResponse && this.authService.isLogin()) {
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/auth/hello']);
        return false;
    }
}