import {Component, OnInit} from "@angular/core";
import {AuthService} from "../authservice/auth.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
    public changePasswordForm: FormGroup;

    constructor(public authService: AuthService,
                private formBuilder: FormBuilder) {}

    public changePassword(email: string, password: string) {
        this.authService.sendChangePassword(email, password)
            .subscribe(() => {
                console.log("change password");
            })
    }

    public moveToSign() {
        this.authService.moveToSign();
    }

    ngOnInit(): void {
        this.changePasswordForm = this.formBuilder.group({
            Login: new FormControl('', [Validators.required, Validators.minLength(5)]),
            Password: new FormControl('', [Validators.required, Validators.minLength(5)])
        });
    }
}