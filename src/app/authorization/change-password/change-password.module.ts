import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {ChangePasswordComponent} from "./change-password.component";
import {NgBootstrapFormValidationModule} from "ng-bootstrap-form-validation";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [HttpClientModule, FormsModule, ReactiveFormsModule,
        NgBootstrapFormValidationModule],
    declarations: [ChangePasswordComponent],
    exports: [
        ChangePasswordComponent
    ]
})
export class ChangePasswordModule {}
