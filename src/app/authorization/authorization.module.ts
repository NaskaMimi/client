import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {AuthorizationComponent} from "./authorization.component";
import {RouterModule} from "@angular/router";
import {AuthService} from "./authservice/auth.service";
import {AuthorizationServiceModule} from "./authservice/auth.service.module";
import {RegistrationModule} from "./register/registration.module";
import {HelloModule} from "./hello/hello.module";
import {SignModule} from "./sign/sign.module";
import {ChangePasswordModule} from "./change-password/change-password.module";

@NgModule({
    imports: [HttpClientModule, RouterModule,
        HelloModule, RegistrationModule, SignModule, ChangePasswordModule,
        AuthorizationServiceModule],
    declarations: [AuthorizationComponent],
    exports: [
        AuthorizationComponent
    ],
    providers: [AuthService]
})
export class AuthorizationModule {}
